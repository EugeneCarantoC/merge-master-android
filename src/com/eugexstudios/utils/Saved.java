 package com.eugexstudios.utils;
 
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Random;

import android.os.Environment;

import com.eugexstudios.framework.FileIO;

public class Saved { 

		public static final String STUDIO_FOLDER   = "EugexStudios";
		public static final String GAME_FOLDER     = "MergeMaster";
		public static final String FILE_NAME       = "serialdata"; 
		public static final String EXTENSION       = ".txt"; 
	       
//			private static String generateID()
//			{
//				Random l_rand = new Random();
//				return l_rand.nextInt(1000)+l_rand.nextInt(500) + l_rand.nextInt(900) +"";
//			}
			
//   
//      public static void loadDatabase(FileIO files)
//      {  
//      	 BufferedReader in = null;
//      	 
//      	 try 
//      	 { 
//             in = new BufferedReader(new InputStreamReader(files.readFile(STUDIO_FOLDER+ "/" + GAME_FOLDER + "/" + FILE_NAME + EXTENSION))); 
////             m_testSave= Integer.parseInt(in.readLine());   
//         } 
//      	 catch (IOException e) 
//      	 {  
////      		SaveSerialData(files);
//         }
//      } 
	//   
	
	  static int m_numberOfSaved =0;
	  static String m_savedRaw[] = new String[1];
      public static void LoadPrevious(FileIO files)
      {  
      	 BufferedReader in = null;
      	 m_numberOfSaved = 0;
      	 try 
      	 { 
             in = new BufferedReader(new InputStreamReader(files.readFile(STUDIO_FOLDER+ "/" + GAME_FOLDER + "/" + FILE_NAME + EXTENSION))); 
             m_numberOfSaved= Integer.parseInt(in.readLine());  

             m_savedRaw = new String[m_numberOfSaved];
             
             for(int idx=0;idx<m_numberOfSaved;idx++)
             {
            	 m_savedRaw[idx]= in.readLine();  

             } 
         } 
      	 catch (IOException e) 
      	 {  

      		   m_numberOfSaved =0;
      		   m_savedRaw = new String[1];
      		SaveSerialData(files,"",false);
         }
      } 
      
      public static void SaveSerialData(FileIO files, String p_serial)
      { 
    	  SaveSerialData(files,p_serial,true);
      }
      
      public static void SaveSerialData(FileIO files, String p_serial, boolean p_loadPrevious)
      { 
    	  if(p_loadPrevious){
    		  LoadPrevious(files); 
    	  }
    	  
    	  BufferedWriter out = null; 
    	  File dir = new File(Environment.getExternalStorageDirectory(),STUDIO_FOLDER); 
 	 
    	  try
    	  { 
    		  dir.mkdir();  
    	  }
    	  catch(Exception es)
    	  {
    		  genSet.ShowToast(es.getMessage()); 
    	  }

    	  File dir_game = new File(Environment.getExternalStorageDirectory()+"/"+ STUDIO_FOLDER, GAME_FOLDER);
    	  try
    	  { 
    		  dir_game.mkdir(); 
    	  }
    	  catch(Exception es)
    	  {
    		  genSet.ShowToast(es.getMessage());
    	  }
    	  
    	  try 
  	      { 
	    	 out = new BufferedWriter(new OutputStreamWriter(files.writeFile(STUDIO_FOLDER+ "/" + GAME_FOLDER + "/" + FILE_NAME   + EXTENSION))); 

	    	 m_numberOfSaved++;
		    	 out.write(m_numberOfSaved+"");   
	  	    	 out.newLine();   

				 genSet.ShowToast("m_numberOfSaved: "+ m_numberOfSaved);
	  	    	for(int idx=0;idx<m_savedRaw.length;idx++)
	            { 
	  	    		out.write(m_savedRaw[idx]+"");   
	  	    		out.newLine();    
	            } 
	  	    	out.write(p_serial  );   
	    		out.newLine();    
	  	    	out.close();
  	      } 
  	      catch(IOException e) 
  	      {   
    		  genSet.ShowToast("write:" + e.getMessage());
  	      }  
  	      finally 
  	      {
  	    	 try 
  	    	 {
  	    		 if(out != null)
  	    			 out.close();
  	    	 }catch (IOException e)
  	    	 { 
  	    		 
  	    	 }
  	      }  
       }  


         
     } 