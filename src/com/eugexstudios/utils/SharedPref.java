package com.eugexstudios.utils;

public interface SharedPref {
	
	//SharedPreferences
	public String HAS_GAME_DATA          = "HAS_GAME_DATA";
	public String LAST_GAME_VERSION      = "LAST_GAME_VERSION";
	public String SOUND_ON			     = "SOUND_ON";
	public String MUSIC_ON			     = "MUSIC_ON";
	public String VIBRATE_ON 		     = "VIBRATE_ON";
	public String LANGUAGE_USED 	     = "LANGUAGE_USED";
	public String SHORTCUT_CREATED 	     = "SHORTCUT_CREATED";
	public String SERIAL_CREATED_CTR    = "SERIAL_CREATED_CTR";
	public String APP_OPENED_CTR	     = "APP_OPENED_CTR";  

//	public String LEVELS_REACHED_DATA    = "LEVELS_REACHED_DATA";
	public String LVL_REACHED_MODE_1    = "LVL_REACHED_MODE_1";
	public String LVL_REACHED_MODE_2    = "LVL_REACHED_MODE_2";
	public String LVL_REACHED_MODE_3    = "LVL_REACHED_MODE_3"; 

	public String UNDO_AVAILABLE    = "UNDO_AVAILABLE"; 
	public String QUICKMERGE_AVAILABLE    = "QUICKMERGE_AVAILABLE"; 
	public String COINS    = "COINS"; 
	

	public String BEST_MOVE_COUNT_A_    = "BEST_MOVE_COUNT_A_"; 
	public String BEST_MOVE_COUNT_B_    = "BEST_MOVE_COUNT_B_"; 
	public String BEST_MOVE_COUNT_C_    = "BEST_MOVE_COUNT_C_"; 
	
	public String INSTRUCTION_BASIC_DONE    = "INSTRUCTION_BASIC_DONE";
	 
}
