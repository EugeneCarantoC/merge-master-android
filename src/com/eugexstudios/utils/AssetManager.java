package com.eugexstudios.utils;
   
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Paint.Align;

import com.eugexstudios.framework.Game;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Image;
import com.eugexstudios.framework.Music;
import com.eugexstudios.framework.Screen;
import com.eugexstudios.framework.Sound;
import com.eugexstudios.framework.Graphics.ImageFormat;
import com.eugexstudios.mergemaster.GameScreen;

@SuppressLint("NewApi")
public class AssetManager extends Screen {

	private int m_loadingProgress;

	private final Rect m_logoCover = new Rect(0,CONFIG.SCREEN_MID_Y-120,CONFIG.SCREEN_WIDTH,CONFIG.SCREEN_MID_Y+60);
	public AssetManager(Game game) {
		super(game); 
		genSet.paint     = new Paint(); 
		genSet.game      = game;   
		m_animCtr = new float[ANIM_SIZE];
		for(int idx=0;idx<m_animCtr.length;idx++)
		{
			m_animCtr[idx] =0;
		}
		m_animCtr[1]=255;
		m_loadingProgress =0;
		m_animDone = false;
		
		 Comforta          = Typeface.createFromAsset(game.getGraphics().getAssetManager(), "Comfortaa-Bold.ttf" );  
		 splash_sound      = game.getAudio().createSound("71852__ludvique__digital-whoosh-soft.ogg");    
		 eugex_splash	       = game.getGraphics().newImage("eugex_splash.png", ImageFormat.ARGB8888);     

		 if(Debugger.has_no_splash && !Debugger.PROD_MODE)
 		 {  
			 m_animDone=true; 
 		 }
	}

	final int ANIM_SIZE=5;
	private float [] m_animCtr;
	private boolean m_animDone;
	
 
	
	public static Music GameMusic; 
	public static Typeface Comforta;  
	public static Image image ; 
	public static Image sprites ; 
	public static Image mountain ;   
	public static Image nums ;   
	public static Image levelbar_anim ;   
	public static Image tile_shine ;  
	public static Image bg ;  
	public static Image tile_shine_4x4 ;  
	public static Image tile_shine_5x5 ;  
	
	public static Image tiles ;  
	public static Image header_style_1 ;  
	public static Image levelbar ;  
	public static Image eugex_splash ;   
	public static Image promo ;   
	public static Image master ;   
	public static Image speech_bubble ;   
	
	public static Sound splash_sound; 
	

	public static Sound tile_let_go;
	public static Sound all_merged;
	public static Sound tile_merging;
	public static Sound ask_exit_popup;
	public static Sound ask_menu_popup;
	public static Sound ask_reset_popup;
	public static Sound restart;
	public static Sound tile_merged;
	public static Sound btn_1;
	public static Sound btn_2;
	public static Sound btn_3;
	public static Sound undo;
	public static Sound err_pop_up;
	public static Sound game_over_popup;
	public static Sound star_1;
	public static Sound star_3;
	public static Sound get_coin;
	
	@Override
	public void update(float deltaTime) {
		
 		Graphics g = game.getGraphics(); 
 		if(m_animDone==false) 
 		{
 			AnimUpdates(deltaTime);
 		}
 		else
 		{	
	 		switch(m_loadingProgress) 
	 		{ 
		 		case 1:  sprites           = g.newImage("sprites.png", ImageFormat.ARGB8888); break;
		 		case 2:  bg	               = g.newImage("bg.png", ImageFormat.ARGB8888);    break;
		 		case 3:  nums              = g.newImage("nums.png", ImageFormat.ARGB8888);    break;
		 		case 4:  levelbar	 	   = g.newImage("levelbar.png", ImageFormat.ARGB8888);  break;
		 		case 5:  mountain	       = g.newImage("mountain.png", ImageFormat.ARGB8888);       break; 
		 		case 6: get_coin           = game.getAudio().createSound("star_3.wav");  break;
		 		case 7: all_merged         = game.getAudio().createSound("all_merged.wav");  break; 
		 		case 8: speech_bubble             = g.newImage("speech_bubble.png", ImageFormat.ARGB8888);	  break; 
		 		case 14: sprites           = g.newImage("sprites.png", ImageFormat.ARGB8888);	 break; 
		 		case 15: tiles             = g.newImage("tiles.png", ImageFormat.ARGB8888);	 break; 
		 		case 16: header_style_1    = g.newImage("header_style_1.png", ImageFormat.ARGB8888);	 break; 
		 		case 17: tile_shine        = g.newImage("tile_shine.png", ImageFormat.ARGB8888);    break; 
		 		case 18: tile_shine_4x4    = g.newImage("tile_shine_4x4.png", ImageFormat.ARGB8888);    break; 
		 		case 19: levelbar_anim     = g.newImage("levelbar_anim.png", ImageFormat.ARGB8888);	 break; 
		 		case 20: tile_shine_5x5    = g.newImage("tile_shine_5x5.png", ImageFormat.ARGB8888);	 break; 
		 		case 21: promo             = g.newImage("promo.png", ImageFormat.ARGB8888);	  break; 
		 		case 22: tile_merging      = game.getAudio().createSound("tile_merging.wav");  break; 
		 		case 23: master             = g.newImage("master.png", ImageFormat.ARGB8888);	  break; 
		 		
		 		case 24: ask_exit_popup    = game.getAudio().createSound("ask_exit_popup.wav");  break; 
		 		case 25: ask_menu_popup    = game.getAudio().createSound("ask_menu_popup.wav");  break;
		 		case 26: ask_reset_popup   = game.getAudio().createSound("ask_reset_popup.wav");  break; 
		 		case 27: restart           = game.getAudio().createSound("restart.wav");  break; 
		 		case 28: tile_merged       = game.getAudio().createSound("tile_merged.wav");  break; 
		 		case 29: btn_1             = game.getAudio().createSound("btn_1.wav");  break; 
		 		case 30: btn_2             = game.getAudio().createSound("btn_2.wav");  break; 
		 		case 31: btn_3             = game.getAudio().createSound("btn_3.wav");  break;  
		 		case 32: undo              = game.getAudio().createSound("undo.wav");  break;  
		 		case 33: tile_merged       = game.getAudio().createSound("tile_merged.wav");  break; 
		 		case 34: tile_let_go       = game.getAudio().createSound("tile_let_go.wav");  break;
		 		case 35: game_over_popup   = game.getAudio().createSound("game_over_popup.wav");  break;
		 		case 36: err_pop_up        = game.getAudio().createSound("err_pop_up.wav");  break;
		 		case 37: star_1            = game.getAudio().createSound("star_1.wav");  break;
		 		case 38: star_3            = game.getAudio().createSound("star_3.wav");  break;
		 		
		 		case 40: game.setScreen(new GameScreen(game)); break;
	 		} 
		 	
	 		m_loadingProgress++; 
 		}
	}
	 

	@Override
	public void paint(float deltaTime) 
	{ 
		Graphics g = game.getGraphics();
		g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT), ColorManager.GAME_THEME_1);   
		
		g.drawImage(eugex_splash, 0,Constants.ScaleY(420));

//		g.drawRect(m_logoCover, Color.argb((int)(m_animCtr[1]), ColorManager.GAME_THEME_1_R,ColorManager.GAME_THEME_1_G,ColorManager.GAME_THEME_1_B)  );  
	}
	
 
	public void AnimUpdates(float p_deltaTime)
	{
		// 0 - Anim Step
		// 1 - White overlay entrance
		 
		if(m_animCtr[0]==0)
 		{  
			if(m_animCtr[2]<30)
			{
				m_animCtr[2] += 1*p_deltaTime; 
 			}
			else
			{
				AssetManager.splash_sound.play(1.0f); 
				m_animCtr[0]=1; 
			}
 		}
 		if(m_animCtr[0]==1)
 		{
 			if(m_animCtr[1]>0)
 			{
 				m_animCtr[1]-= 10*(p_deltaTime); 

 	 			if(m_animCtr[1]<=0)
 	 			{
 	 				m_animCtr[1]=0; 
 	 				m_animDone=true;
 	 			}
 			}
 			
 		} 
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {

	}
}