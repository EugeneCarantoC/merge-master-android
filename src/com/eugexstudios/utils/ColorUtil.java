package com.eugexstudios.utils;

import android.graphics.Color;

public class ColorUtil {

	public static int SetOpacity(float p_opacity, int p_red, int p_green, int p_blue)
	{
		return Color.argb((int)(p_opacity*255), p_red, p_green, p_blue);
	}
	public static int SetOpacity(float p_opacity, int p_color[])
	{
		return SetOpacity(p_opacity,  p_color[0]);
	}
	public static int SetOpacity(float p_opacity, int p_color)
	{ 
		return Color.argb((int)(p_opacity*255), Color.red(p_color), Color.green(p_color), Color.blue(p_color));
	}
	
	 
}
