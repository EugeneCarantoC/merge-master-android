package com.eugexstudios.utils;

import com.eugexstudios.framework.Sound;
import com.eugexstudios.framework.Input.TouchEvent;

import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Paint.Align;

public class utilx {

	public static Rect setRecx(int x1,int y1,int x2,int y2){
    	Rect r=new Rect(x1,y1,x2+x1,y2+y1);
    	return r;
    }
	
	public static boolean inBounds(TouchEvent event, int x, int y, int width,
			    int height) {
			        if (event.x > x && event.x < x + width - 1 && event.y > y
			        && event.y < y + height - 1)
			            return true;
			        else
			            return false;
			    }
	public static void playThis(Boolean isSoundOn,Sound sound){
    	if(isSoundOn)
    		sound.play(0.9f);
    }
    public static void playThis(Boolean isSoundOn,Sound sound,float volume){
    	if(isSoundOn)
    		sound.play(volume);	
    } 
	public void setTextProperties(Typeface typeface,int textSize,int color,Align align){ 
		genSet.paint.setTypeface(typeface);
		genSet.paint.setTextSize(textSize); 
		genSet.paint.setColor(color); 
		genSet.paint.setTextAlign(align);

		genSet.paint.setAntiAlias(true);
	}
     
 
}
