package com.eugexstudios.utils;

import android.graphics.Color;
import android.graphics.Rect; 
 




import com.eugexstudios.framework.ButtonData;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Image; 
import com.eugexstudios.framework.ImageData;
import com.eugexstudios.framework.Input.TouchEvent;

public abstract class Screen {
protected int buttonPressed;  
protected abstract void Paint(Graphics g);
protected abstract void Updates(float p_deltaTime);
protected abstract void TouchUpdates(TouchEvent g);
protected abstract void backButton();
//private enum state {
//	 PLACEHOLDER_1 
//	,PLACEHOLDER_2
//	,PLACEHOLDER_3
//	,PLACEHOLDER_4
//	,PLACEHOLDER_5
//	,PLACEHOLDER_6
//	}; 
//private state m_state;

//switch(m_state)
//{
//	case PLACEHOLDER_1: break;
//	case PLACEHOLDER_2: break;
//	case PLACEHOLDER_3: break;
//	case PLACEHOLDER_4: break;
//	case PLACEHOLDER_5: break;
//	case PLACEHOLDER_6: break;
//}


public void drawRoundRect(Graphics g,ImageData p_imgData, int p_color,  int p_color2,  int p_btnID)
{   
	g.drawRoundRect( p_imgData.x ,p_imgData.y, p_imgData.width, p_imgData.height, 20, 20, buttonPressed==p_btnID?p_color2:p_color);    
} 

public void drawRoundRect(Graphics g,ImageData p_imgData, int p_color,  int p_color2,  int p_btnID,float p_scaleX, float p_scaleY)
{   
	g.drawRoundRect( p_imgData.x ,p_imgData.y, (int)(p_imgData.width * p_scaleX), (int)(p_imgData.height*p_scaleY), 20, 20, buttonPressed==p_btnID?p_color2:p_color);    
} 
public void drawRoundRect(TouchEvent g, ImageData p_btnData, int p_color,  int p_color2,  int p_btnID,float p_scaleX, float p_scaleY)
{    
	if(inBounds(g, p_btnData.x ,p_btnData.y, (int)(p_btnData.width*p_scaleX), (int)(p_btnData.height*p_scaleY)))
		buttonPressed=p_btnID; 
  
} 
 

public void drawRoundRect(Graphics g,int p_addX, int p_addY, ImageData p_imgData, int p_color,  int p_color2,  int p_btnID)
{   
	g.drawRoundRect( p_imgData.x+p_addX ,p_imgData.y+p_addY, p_imgData.width, p_imgData.height, 20, 20, buttonPressed==p_btnID?p_color2:p_color);   
   
} 

public void drawRoundRect(Graphics g,int p_addX, int p_addY, ImageData p_imgData, int p_color,  int p_color2,  int p_btnID, float p_xscale, float p_yscale)
{   
	g.drawRoundRect( p_imgData.x+p_addX ,p_imgData.y+p_addY, (int)(p_imgData.width*p_xscale), (int)(p_imgData.height*p_yscale), 20, 20, buttonPressed==p_btnID?p_color2:p_color);   
   
} 

public void drawRoundRect(Graphics g,ButtonData p_btnData, int p_color,  int p_color2,  int p_btnID)
{   
	g.drawRoundRect( p_btnData.x ,p_btnData.y+p_btnData.height/2, p_btnData.width, p_btnData.height, 20, 20, buttonPressed==p_btnID?p_color2:p_color);   
//	g.drawRoundRect( p_x ,p_y, p_len, p_wid, 20, 20, buttonPressed==p_btnID?p_color2:p_color);   
  
} 
public void drawRoundRect(Graphics g,int p_addX, int p_addY, ButtonData p_btnData, int p_color,  int p_color2,  int p_btnID)
{   
	g.drawRoundRect(p_addX+ p_btnData.x ,p_addY+p_btnData.y+p_btnData.height/2, p_addX+p_btnData.width,p_addY+p_btnData.height, 20, 20, buttonPressed==p_btnID?p_color2:p_color);   
//	g.drawRoundRect( p_x ,p_y, p_len, p_wid, 20, 20, buttonPressed==p_btnID?p_color2:p_color);   
  
} 
public void drawRoundRect(TouchEvent g, ButtonData p_btnData, int p_color,  int p_color2,  int p_btnID, float p_xscale, float p_yscale)
{    
	if(inBounds(g, p_btnData.x ,p_btnData.y+p_btnData.height/2, (int)(p_btnData.width*p_xscale), (int)(p_btnData.height*p_yscale)))
		buttonPressed=p_btnID; 
  
} 
public void drawRoundRect(TouchEvent g ,int p_addX, int p_addY, ImageData p_imgData, int p_color,  int p_color2,  int p_btnID, float p_xscale, float p_yscale)
{    
	if(inBounds(g, p_addX+p_imgData.x , p_addY+p_imgData.y, (int)(p_imgData.width*p_xscale), (int)(p_imgData.height*p_yscale)))
		buttonPressed=p_btnID; 
  
} 
public void drawRoundRect(TouchEvent g, ButtonData p_btnData, int p_color,  int p_color2,  int p_btnID)
{    
	if(inBounds(g, p_btnData.x ,p_btnData.y+p_btnData.height/2, p_btnData.width, p_btnData.height))
		buttonPressed=p_btnID; 
  
} 
public void drawRoundRect(TouchEvent g, ImageData p_btnData, int p_color,  int p_color2,  int p_btnID)
{    
	if(inBounds(g, p_btnData.x ,p_btnData.y, p_btnData.width, p_btnData.height))
		buttonPressed=p_btnID; 
  
} 

public void drawRoundRect(TouchEvent g,int p_addX, int p_addY, ImageData p_imgData, int p_color,  int p_color2,  int p_btnID)
{   
	if(inBounds(g, p_imgData.x+p_addX ,p_imgData.y+p_addY, p_imgData.width, p_imgData.height))
		buttonPressed=p_btnID; 
   
} 
protected void drawRoundRect(Graphics g, int p_x, int p_y, int p_len, int p_wid, int p_color,  int p_color2,  int p_btnID)
{   
	g.drawRoundRect( p_x ,p_y, p_len, p_wid, 20, 20, buttonPressed==p_btnID?p_color2:p_color);   
}  

protected void drawRoundRect(Graphics g, Rect p_rect, int p_color,  int p_color2,  int p_btnID)
{   
	g.drawRoundRect( p_rect , 20, 20, buttonPressed==p_btnID?p_color2:p_color);   
 }  

protected void drawRoundRect(Graphics g, Rect p_rect, int p_color,    int p_btnID)
{   
	g.drawRoundRect( p_rect , 20, 20, p_color);   
 } 
protected void drawRoundRect(Graphics g, Rect p_rect, int p_color )
{   
	g.drawRoundRect( p_rect , 20, 20, p_color);   
 } 
protected void ShowToast(String p_toast)
{ 
	genSet.ShowToast(p_toast);	
}
protected void drawRoundRect(TouchEvent g, int p_x, int p_y, int p_len, int p_wid, int p_color,  int p_color2,  int p_btnID)
{    
	if(inBounds(g, p_x ,p_y, p_len, p_wid))
		buttonPressed=p_btnID; 
} 
protected void drawRoundRect(TouchEvent g,  Rect p_rect,  int p_color,  int p_color2,  int p_btnID)
{    
	if(inBounds(g, p_rect))
		buttonPressed=p_btnID; 
} 
  
protected boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
	 
	if (event.x > x && event.x < x + width - 1 && event.y > y && event.y < y + height - 1)
 
		return true; 
	else
		return false;
} 
protected boolean inBounds(TouchEvent event, Rect p_rect ) {
 
	if (event.x > p_rect.left && event.x < p_rect.right - 1 && event.y > p_rect.top && event.y <  p_rect.bottom - 1)
		return true;
	else
		return false;
} 
protected void paintButton(Graphics g, Image buttonImage, int xPos, int yPos, int buttonValue, int d1, int d2)
{
	g.drawImage( buttonImage, xPos - (buttonPressed == buttonValue ? d1 : 0), yPos+ (buttonPressed == buttonValue ? d2 : 0));
	 }

protected void paintRectButton(Graphics g, int p_posX,int p_posY, int p_wid, int p_height, int p_color, int buttonValue, int d1, int d2)
{
 
    g.drawRect(p_posX+5+ (buttonPressed == buttonValue ? d1 : 0),p_posY+10+ (buttonPressed == buttonValue ? d1 : 0),p_wid,p_height,Color.argb(60, 0, 0, 0));
    g.drawRect(p_posX+ (buttonPressed == buttonValue ? d1 : 0),p_posY+ (buttonPressed == buttonValue ? d1 : 0),p_wid,p_height, p_color);

}


protected void paintButton(Graphics g, Image buttonImage, int xPos, int yPos, int sourceX, int sourceY, int l, int w,
		int buttonValue, int d1, int d2) {
	g.drawImage( buttonImage, xPos - (buttonPressed == buttonValue ? d1 : 0), yPos
			+ (buttonPressed == buttonValue ? d2 : 0), sourceX, sourceY, l, w);
	 }
protected void paintButton(Graphics g, Image buttonImage, int xPos, int yPos, int xWid, int yWid, int sourceX,
		int sourceY, int srcWid, int srcHeight, int buttonValue, int d1, int d2) {
	g.drawScaledImage(buttonImage, xPos - (buttonPressed == buttonValue ? d1 : 0), yPos
			+ (buttonPressed == buttonValue ? d2 : 0), xWid - (buttonPressed == buttonValue ? d1 : 0), yWid
			+ (buttonPressed == buttonValue ? d2 : 0), sourceX, sourceY, srcWid, srcHeight,1);
} 
protected int[] paintButton(int g, Image buttonImage, int xPos, int yPos, int sourceX, int sourceY, int l, int w,
		int buttonValue, int d1, int d2) {
	  
	return new int[]{xPos,yPos,l,w,buttonValue};
	

}  
 
  
}
