package com.eugexstudios.utils;

import android.graphics.Color;

public class ColorManager {


	public final static int WINDOWBODY_COLOR         = Color.WHITE; 
	public final static int WINDOWHEADER_COLOR2         = Color.rgb(255,186,65); 
	public final static int THEME_1_C         = Color.rgb(255,217,83); 
	public final static int THEME_1_C2         = Color.rgb(255,192,45); 

	public final static int EMPTY_TILE_COLOR= Color.argb(12,5, 10, 10);
	public final static int GONE_OUT_OF_EXISTENCE= -1;

	public final static int MENU_CYAN         = Color.rgb(26,228,202); 
	public final static int MENU_WHITE         = Color.rgb(251,253,248);

	public final static int CYAN_2         = Color.rgb(26,228,202);
	public final static int BLUEGREEN_1         = Color.rgb(0,153,159); 
	public final static int VIOLET_1         = Color.rgb(255,150,255);
	public final static int VIOLET_2         = Color.rgb(187,74,240);
	public final static int LIGHT_ORANGE_1         = Color.rgb(255,206,165);
	public final static int ORANGE_2         = Color.rgb(255,131,121);
	public final static int GREEN_1         = Color.rgb(125,195,131);
	public final static int GREEN_2         = Color.rgb(106,156,120);
	public final static int YELLOW_2       = Color.rgb(255,241,188);
	public final static int MAROON_1       = Color.rgb(179,44,80);
	public final static int ORANGE_1       = Color.rgb(245,106,71); 
	public final static int MAROON_2       = Color.rgb(104,26,30); 
	public final static int VIOLET_3       = Color.rgb(101,88,138);  
	public final static int VIOLET_5       = Color.rgb(87,73,146);  
	public final static int GREEN_3        = Color.rgb(161,222,147);  
	public final static int RED_3          = Color.rgb(244,124,124);  
	public final static int YELLOW_3       = Color.rgb(247,244,139);  
	public final static int BLUE_3       = Color.rgb(112,161,215);  
	

	public final static int PURPLE_4         = Color.rgb(128,128,192);
	public final static int FUSHIA_1         = Color.rgb(255,111,183); 

	public final static int GAME_THEME_1_R       = 242;  
	public final static int GAME_THEME_1_G       = 61;  
	public final static int GAME_THEME_1_B       = 61;  
	public final static int GAME_THEME_1       = Color.rgb(GAME_THEME_1_R, GAME_THEME_1_G, GAME_THEME_1_B);
	
	public final static int[] COLORS = new int[]
	{

			 Color.rgb(212, 56, 56),
			 Color.rgb(225, 106, 48),
		     Color.rgb(255, 199, 47),
		     Color.rgb(117, 2019, 71),
		     Color.rgb(50, 174, 65)
			
			
	};
	
}
