package com.eugexstudios.utils;

public class Debugger {

	public final static boolean PROD_MODE 		     = false;   
	public static boolean show_bounds 			     = false;   	// WHEEL,  
	public static boolean show_touches 		   		 = false;    // GAMESCREEN
	public static boolean show_fps 		   			 = false;    // GAMESCREEN
	public static boolean has_no_splash 		     = true;     // GAMESCREEN 
	public static boolean has_modes 		         = false;     // GAMESCREEN 
	public static boolean has_creator 		         = false;     // GAMESCREEN 
	public static boolean play_immediate 		     = true;     // GAMESCREEN 
	
	public static boolean show_grid 		         = false;     // GAMESCREEN 

	public static boolean show_stats 		         = false;     // GAMESCREEN
	public static boolean eneble_creator 		         = true;    
	public static boolean has_level_mover 		         = false;    

	public static boolean aquires_level_prog 		 = true;    //This does not refer to Demo mode to be enabled
	
}
