package com.eugexstudios.utils;
  
   
import com.eugexstudios.framework.ImageData;

import android.graphics.Point;
import android.graphics.Rect;
  
public class Constants { 

	public static enum game_mode {mode_1, mode_2, mode_3};
	
	public static void SetScreenConfig(int p_wid, int p_height)
	{ 
		Point resolution = new Point(p_wid, p_height);

		 m_reso = reso_mode.default_size;
	     CONFIG.SCREEN_WIDTH   = p_wid;
	     CONFIG.SCREEN_HEIGHT  = p_height;
	 	 
		 if(resolution.equals(new Point(720,1280)) // SONY XA
		  )  
		 {
			 m_reso = reso_mode.default_size;
		 }
		 else if(resolution.equals(new Point(1080,1920))) 
		 { 
			 m_reso = reso_mode.size_b; 
			 CONFIG.X_SCALE  = (float)1080/720;
			 CONFIG.Y_SCALE = (float)1920/1280;
		 } 
		 else if(resolution.equals(new Point(810,1440)))  
		 { 
			 m_reso = reso_mode.size_c; 
			 CONFIG.X_SCALE  = (float)810/720;
			 CONFIG.Y_SCALE = (float)1440/1280;
		 } 		 else if(resolution.equals(new Point( 1080,1920))) // SM J7, 
		 { 
			 m_reso = reso_mode.size_e; 
			 CONFIG.X_SCALE  = (float)1080/720;
			 CONFIG.Y_SCALE = (float)1920/1280;
		 } 
		 else  
		 {  
			 m_reso = reso_mode.default_size;
		 } 
		  
	     // RECALC all dependencies
	    CONFIG.SCREEN_MID = CONFIG.SCREEN_WIDTH/2; 
		SCREEN_RECT = new Rect(0,0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT); 

		PLAY_HEADER_RECT = new Rect(0,0,CONFIG.SCREEN_WIDTH, ScaleY(0.109375f));
		PLAY_FOOTER_RECT = new Rect(0,CONFIG.SCREEN_HEIGHT-ScaleY(0.078125f),CONFIG.SCREEN_WIDTH,CONFIG.SCREEN_HEIGHT); 
		TILES_MARGIN_X  = ScaleX(40);
		TILES_MARGIN_Y  = ScaleY(340);
		LEVEL_PLAYING    = Rect(CONFIG.SCREEN_MID-270/2, 20, 270, 55);  
		
		MENU_HEADER      = Rect(0, 0, 720, 600); 
		
		WINDOW_A         = Rect(60, 200, 600, 800); 
		WINDOW_HEADER_A  = Rect(60, 200, 600, 140); 
		
		WINDOW_B         = Rect(30, 120, 660, 1030); 
		WINDOW_HEADER_B  = Rect(30, 120, 660, 140);  

		WINDOW_C         = Rect(70, 160, 580, 930); 
		WINDOW_HEADER_C  = Rect(70, 160, 580, 140); 

		WINDOW_D         = Rect(40, 300, 640, 730); 
		WINDOW_HEADER_D  = Rect(40, 300, 640, 140);  
  
		ASK_WINDOW_B         = Rect(30, 400, 660, 320); 
		ASK_WINDOW_HEADER_B  = Rect(30, 400, 660, 90); 
		ASK_WINDOW_BTN_1     = Rect(68, 686, 293, 110); 
		ASK_WINDOW_BTN_2     = Rect(375,686, 293, 110); 
		ASK_WINDOW_BTN_3     = Rect(68, 686, 600, 110); 
		
		PLAYNOW_BTN  = Rect(110, 800, 500, 150); 
		SETTINGS_BTN = Rect(130+(140+20)*0, 800+150+20, 140, 140);   
		OFFER_BTN    = Rect(130+(140+20)*2, 800+150+20, 140, 140); 
		CREATOR_BTN  = Rect(140, 800+150+20+160, 440, 120); 
		 
		ABOUT_WINDOW        = Rect(72, 175, 575, 910);  
		ABOUT_WINDOW_HEADER = Rect(72, 175, 575, 120);  

		PUBLISHER_PAGE_BTN = Rect(145, 715, 430,90);  
		SHARE_APP_BTN      = Rect(145, 875, 430,90);  

		MODE_BTN_1      = Rect(60, 310+(250+25)*0, 600, 250);  
		MODE_BTN_2      = Rect(60, 310+(250+25)*1, 600, 250);  
		MODE_BTN_3      = Rect(60, 310+(250+25)*2, 600, 250);  
 

		SOUND_BTN       = Rect(140, 465, 440, 110); 
		MUSIC_BTN       = Rect(140, 465+140*1, 440, 115);  
		ABOUT_BTN       = Rect(140, 465+140*2, 440, 115);  
		RESET_DATA_BTN  = Rect(140, 465+140*3, 440, 115);  

		CLOSE_ABOUT_BTN = new Rect(
			      Constants.ABOUT_WINDOW.right-Constants.ScaleX(90) 
				, Constants.ABOUT_WINDOW.top+Constants.ScaleY(10)
				, Constants.ABOUT_WINDOW.right-Constants.ScaleX(90)+Constants.ScaleX(80)
				, Constants.ABOUT_WINDOW.top+Constants.ScaleY(10)+Constants.ScaleY(80)
				
				);  
		PLAY_BTN = new ImageData(ScaleX(0.3680555555555f),ScaleY(0.703125f), 772, 6, 212, 221);
		LOGO = new ImageData(ScaleX(0.1319444f), ScaleY(0.17734375f),0, 9, 551, 281);
		MENU_LOGO = new ImageData(ScaleX(16), ScaleY(220),5592, 301, 692, 324);

		GAMEOVER_HEADER_1  = new ImageData(ScaleX(0.0166666666666667f), ScaleY(0.17f),1897, 18, 702, 205);
		GAMEOVER_HEADER_2  = new ImageData(ScaleX(0.0166666666666667f), ScaleY(0.17f),1895, 236, 700, 152);
		
		

		RESTART_BTN_1 = new ImageData(CONFIG.SCREEN_MID-((int)(244/2)*CONFIG.X_SCALE), ScaleY(0.39f),2641, 18, 244, 278);
		RESTART_BTN_2 = new ImageData(CONFIG.SCREEN_MID-((int)(209/2)*CONFIG.X_SCALE), ScaleY(0.35f),2637, 312, 209, 237);
		RESTART_BTN_3 = new ImageData(CONFIG.SCREEN_MID-((int)(145/2)*CONFIG.X_SCALE), ScaleY(490),2915, 162, 145, 155);

//		HOME_BTN_IMG  = new ImageData(2915, 14, 125, 133);
//		SHARE_BTN_IMG = new ImageData(2918+130, 15, 122, 133);
		HOME_BTN_IMG  = new ImageData(2896, 329, 108, 114);
		SHARE_BTN_IMG = new ImageData(3011, 329, 108, 114);
		RESTART_BTN_IMG= new ImageData(2892, 465, 113, 114);

		TILE_STAR_1 = new ImageData(0,-2,3186, 6, 126, 120);
		TILE_STAR_2 = new ImageData(0,-2,3186, 6+150*1, 126, 120);
		TILE_STAR_3 = new ImageData(0,0,3186, 6+150*2, 126, 120);
		TILE_STAR_4 = new ImageData(0,0,3186, 6+150*3, 126, 120);
		
		LEVEL_SELECT_HOME_BTN = new ImageData(0,ScaleY(1280)-84,3495,482, 108, 84);
		 
		CLOUDS = new ImageData[] {
					 new ImageData(576, 50*0, 186, 50) 
					,new ImageData(576, 50*1, 186, 50) 
					,new ImageData(576, 50*2, 186, 50) 
					,new ImageData(576, 50*3, 186, 50)
		};
 
				
		 LEVEL_COMP_HEADER_1   = new ImageData(ScaleX(0.0069444444444444f), ScaleY(0.19609375f),3493, 11, 715, 227);  
		 LEVEL_COMP_HEADER_2   = new ImageData(ScaleX(0.0069444444444444f), ScaleY(0.19609375f),3507, 250, 702, 172); 
		  
		 NEXT_LEVEL_IMG= new ImageData(4247, 288, 200, 210);
		 LOCK_IMG= new ImageData(4555, 6, 54, 74);
		  
		 MENU_SETTINGS   = new ImageData(ScaleX(0.8638888888888889f), ScaleY(0.5f)+ScaleY(0.089843f)*0, 5311, 12+105*0, 114, 99);
		 MENU_SHARE      = new ImageData(ScaleX(0.8638888888888889f), ScaleY(0.5f)+ScaleY(0.089843f)*1, 5311, 12+105*1, 114, 99);
		 MENU_MOREGAMES  = new ImageData(ScaleX(0.8638888888888889f), ScaleY(0.5f)+ScaleY(0.089843f)*2, 5311, 12+105*2, 114, 99);
		 
		 
		 UNDO_BTN  = new ImageData(ScaleX(573), ScaleY(1067),5310,345,122,94);
		 UNDO_UNAVAIL_BTN  = new ImageData(ScaleX(573), ScaleY(1067),5310,445,122,94); 

		 QUICK_MERGE_BTN  = new ImageData(ScaleX(573-135), ScaleY(1067),5310,545,122,94);
		 QUICK_MERGE_UNAVAIL_BTN  = new ImageData(ScaleX(573-135), ScaleY(1067),5310,645,122,94);
		 
		 CTR_IMG  = new ImageData(ScaleX(662), ScaleY(1054),5450,351,42,47);
		 CTR_UNAVAIL_IMG  = new ImageData(ScaleX(662), ScaleY(1054),5450,451,42,47);
		  
		 COLORTWIRL_BTN  = new ImageData(ScaleX(0.125f), 0, 5479, 27, 554, 211);
		 LINESWINGER_BTN = new ImageData(ScaleX(0.125f), ScaleY(220), 6064, 26, 554, 211);
		 

 
		 WINDOW_HEAD_1   = new ImageData(6640, 240, 660, 115); 
		 WINDOW_HEAD_1b   = new ImageData(6640, 240+300, 660, 130);

		 WINDOW_BODY_1   = new ImageData(6640, 360, 660, 100);
		 WINDOW_BOTTOM_1 = new ImageData(6640, 472, 660, 50);
		 
		 
		 STAR_RESULT_1   = new ImageData(1147, 244, 155, 196);
		 STAR_RESULT_2   = new ImageData(1147, 244, 340, 196);
		 STAR_RESULT_3   = new ImageData(1147, 244, 485, 196);
		 STAR_RESULT_BACK   = new ImageData(1147, 244+200, 485, 196);
		 

		 LEVEL_BTN = new ImageData(1135,76,147,131);
		 LEVEL_CURRENT_BTN = new ImageData(1135+300,76,147,131);
		 
		 LEVEL_STAR = new ImageData[] {
		         new ImageData(1135, 6, 47, 57), // 1 Star
		         new ImageData(1135, 6, 103, 57), // 2 Star
				 new ImageData(1135, 6, 147, 57) // 3 Star
		         
		 }; // 1 Star

		 LEVEL_LOCK_BTN = new ImageData(1285,76,147,131);
		 RESTART_PLAY = new ImageData(Constants.ScaleX(603), Constants.ScaleY(12),3033,469,101,101);
		 RESTART_PLAY_PRESS = new ImageData(Constants.ScaleX(603), Constants.ScaleY(12),3033,469+115,101,101);
		 

		 WINDOW_1= new ImageData(23,315,673,709);
		 MORE_GAMES_HEADER     = new ImageData(23,315+710+125*0,673,125);
		 LEVEL_COMPLETE_HEADER = new ImageData(23,315+710+125*1,673,125);
		 GAME_OVER_HEADER = new ImageData(23,315+710+125*2,673,125);

		 WATCH_VID_BTN= new ImageData(1958,22,570,159);
		 

		 WINDOW_2 = new ImageData(6645,750,631,614);
		 NEED_TO_UNDO_HEADER= new ImageData(6645+740,750+150*0,650,150);
		 NEED_QMERGE_HEADER= new ImageData(6645+740,750+150*1,650,150);
		 
		 
		 BUY_UNDO_BTN= new ImageData(6034,750,568,152);
		 WATCH_FOR_UNDO_BTN= new ImageData(6034,937,568,152);
		 
		 BUY_QMERGE_BTN= new ImageData(6034,1137,568,152);
		 WATCH_FOR_QMERGE_BTN= new ImageData(6034,1337,568,152); 
		 

		 CLOSE_WINDOW_BTN = new ImageData(3642,485,64,65);

		 GAMEPLAY_COIN = new ImageData(3735,487,40,54);

		 PROMO_3 = new ImageData(23,10,0,0,200,147);
		 PROMO_4 = new ImageData(38,24,0,200*1,200,147);
		 PROMO_5 = new ImageData(50,32,0,398,200,147);

		 INSTR_NEXT = new ImageData(3493,381,189,76);
		 CANCEL_MERGE_BTN = new ImageData(3483,586,103,114);
		 
		switch(m_reso)
		{ 
			case size_b: break;
			case size_c:  break;
			default:  break;
		}
		
		
	}

	private static Rect Rect(int p_x, int p_y, int p_width, int p_height)
	{
		return new Rect(ScaleX(p_x),ScaleY(p_y),ScaleX(p_x+p_width),  ScaleY(p_y+p_height));
	}
	
	public static int ScaleX(int p_sizeIfDefault)
	{
		return (int) (((float)p_sizeIfDefault/720)*CONFIG.SCREEN_WIDTH);	
	} 

	public static int ScaleY(int p_sizeIfDefault)
	{
		return (int) (((float)p_sizeIfDefault/1280)*CONFIG.SCREEN_HEIGHT);	
	} 
	 
	public static int ScaleX(float p_perc)
	{
		return (int)(CONFIG.SCREEN_WIDTH*p_perc);	
	}
	public static int ScaleY(float p_perc)
	{
		return (int)(CONFIG.SCREEN_HEIGHT*p_perc);	
	}
	 
   public enum reso_mode
   {
   	 	 default_size
    	,size_b
    	,size_c
    	,size_d 
    	,size_e
   }
   public  static reso_mode m_reso;

	//STARTAPP
	public final static String NATIVE_AD_MENU     = "menu_native_ad_01"; 
	public final static String NATIVE_AD_EXIT     = "exit_native_ad_01"; 
	public final static String NATIVE_AD_GAMEOVER = "game_gameover_ad_02";  
	public final static String NATIVE_AD_GAMEOVER_2 = "game_gameover_ad_03";  
	public final static String VIDEO_AD_CONTINUE  = "game_video_ad_01"; 
	public final static String VIDEO_AD_LVLCOMP  = "video_ad_lvlcomp"; 
	public final static String REWARD_VIDEO_AD_SKIP_LVL = "video_ad_skip_lvl";  
	public final static String REWARD_VIDEO_AD_BUY_UNDO = "video_ad_buy_undo";  
	public final static String REWARD_VIDEO_AD_BUY_QMERGE = "video_ad_buy_quick_merge";  
	
	public final static int VIDEO_AD_CHANCE =3; // 33% to get a video ad on level complete 
	public final int WHEEL_SECTION_GUIDE_RAD = 50;	 
	public final int BANNER_AD_SPACE_HEIGHT=125;
	
	public final static int UNDO_PURCH_QTY = 3;
	public final static int UNDO_PURCH_COIN_COST = 10;
	 
	public final static int QMERGE_PURCH_COIN_COST = 50;
	 

	public static Rect SCREEN_RECT = new Rect(0,0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT);

	public static int TILES_MARGIN_X;
	public static int TILES_MARGIN_Y;
	
	public static int ADJACENT_LINE_SIZE =15;
	public static Rect PLAY_HEADER_RECT; 
	public static Rect PLAY_FOOTER_RECT;
	
	public static Rect LEVEL_PLAYING;  

	public static Rect MENU_HEADER;  

	public static Rect WINDOW_A;  
	public static Rect WINDOW_HEADER_A; 
	 
	public static Rect WINDOW_B;  
	public static Rect WINDOW_HEADER_B; 

	public static Rect WINDOW_C;  
	public static Rect WINDOW_HEADER_C; 
	
	public static Rect WINDOW_D;  
	public static Rect WINDOW_HEADER_D; 
	 
	public static Rect ASK_WINDOW_B;  
	public static Rect ASK_WINDOW_HEADER_B; 
	public static Rect ASK_WINDOW_BTN_1;   
	public static Rect ASK_WINDOW_BTN_2;   

	public static Rect ASK_WINDOW_BTN_3;   
	 
	public static Rect ABOUT_WINDOW;  
	public static Rect ABOUT_WINDOW_HEADER;
	public static Rect PUBLISHER_PAGE_BTN; 
	public static Rect SHARE_APP_BTN; 
	public static Rect CLOSE_ABOUT_BTN; 
	 
	
	public  static Rect PLAYNOW_BTN;  
	public  static Rect SETTINGS_BTN; 
	public  static Rect OFFER_BTN; 
	public  static Rect CREATOR_BTN; 
	
	public  static Rect MODE_BTN_1; 
	public  static Rect MODE_BTN_2; 
	public  static Rect MODE_BTN_3; 
 
	
	public  static Rect SOUND_BTN; 
	public  static Rect MUSIC_BTN; 
	public  static Rect ABOUT_BTN;   
	public  static Rect RESET_DATA_BTN;   
	
	public final static int SHOW_AD_AFTER_N_PLAY = 3;
	public final static int PASS_SPEED = 15;
//	public final static int PASS_SPEED = 100;
	

	public static ImageData  PLAY_BTN;
	public static ImageData  LOGO;
	public static ImageData  MENU_LOGO;
	public static ImageData [] CLOUDS; 
	public static ImageData  GAMEOVER_HEADER_1;
	public static ImageData  GAMEOVER_HEADER_2;
	public static ImageData  HOME_BTN_IMG;
	public static ImageData  SHARE_BTN_IMG;
	public static ImageData  RESTART_BTN_IMG;
	public static ImageData  RESTART_BTN_1;
	public static ImageData  RESTART_BTN_2;
	public static ImageData  RESTART_BTN_3;

	public static ImageData  TILE_STAR_1;
	public static ImageData  TILE_STAR_2;
	public static ImageData  TILE_STAR_3;
	public static ImageData  TILE_STAR_4;

	public static ImageData  PROMO_3;
	public static ImageData  PROMO_4;
	public static ImageData  PROMO_5;
	
	public static ImageData  NEXT_LEVEL_IMG;
	public static ImageData  LEVEL_COMP_HEADER_1;
	public static ImageData  LEVEL_COMP_HEADER_2;
	public static ImageData  LOCK_IMG; 

	public static ImageData  MENU_SETTINGS;
	public static ImageData  MENU_SHARE;

	public static ImageData  UNDO_BTN; 
	public static ImageData  UNDO_UNAVAIL_BTN;  
	public static ImageData  CTR_IMG;
	public static ImageData  CTR_UNAVAIL_IMG;
	

	public static ImageData  QUICK_MERGE_BTN; 
	public static ImageData  QUICK_MERGE_UNAVAIL_BTN; 
	
	public static ImageData  MENU_MOREGAMES; 
	public static ImageData  COLORTWIRL_BTN;
	public static ImageData  LINESWINGER_BTN; 

	public static ImageData  WINDOW_HEAD_1;
	public static ImageData  WINDOW_HEAD_1b;
	public static ImageData  WINDOW_BODY_1;
	public static ImageData  WINDOW_BOTTOM_1;
	

	public static ImageData  STAR_RESULT_3;
	public static ImageData  STAR_RESULT_2;
	public static ImageData  STAR_RESULT_1;
	public static ImageData  STAR_RESULT_BACK;

	public static ImageData  LEVEL_BTN;
	public static ImageData  LEVEL_CURRENT_BTN;
	public static ImageData  LEVEL_STAR[]; 

	public static ImageData  LEVEL_LOCK_BTN;
	public static ImageData  RESTART_PLAY;
	public static ImageData  RESTART_PLAY_PRESS;
	public static ImageData LEVEL_SELECT_HOME_BTN; 

	public static ImageData WINDOW_1; 
	public static ImageData MORE_GAMES_HEADER; 
	public static ImageData LEVEL_COMPLETE_HEADER; 
	public static ImageData GAME_OVER_HEADER; 
	
	public static ImageData WATCH_VID_BTN; 

	public static ImageData WINDOW_2; 
	public static ImageData NEED_TO_UNDO_HEADER; 
	public static ImageData NEED_QMERGE_HEADER; 

	public static ImageData BUY_UNDO_BTN;
	public static ImageData WATCH_FOR_UNDO_BTN; 
	public static ImageData CLOSE_WINDOW_BTN; 


	public static ImageData BUY_QMERGE_BTN;
	public static ImageData WATCH_FOR_QMERGE_BTN; 
	
	public static ImageData GAMEPLAY_COIN; 

	public static ImageData INSTR_NEXT; 
	public static ImageData CANCEL_MERGE_BTN; 
	
	
}
