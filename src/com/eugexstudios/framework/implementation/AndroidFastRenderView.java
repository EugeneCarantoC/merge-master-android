package com.eugexstudios.framework.implementation;

import java.lang.reflect.Field;

import com.eugexstudios.utils.genSet;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class AndroidFastRenderView extends SurfaceView implements Runnable {
	AndroidGame game;
	Bitmap framebuffer;
	Thread renderThread = null;
	SurfaceHolder holder;
	volatile boolean running = false;

	private void disableSurfaceViewLogging() {
		try {
			Field field = SurfaceView.class.getDeclaredField("DEBUG");
			field.setAccessible(true);
			field.set(null, false);
		} catch (Exception e) {
		}
	}

	public AndroidFastRenderView(AndroidGame game, Bitmap framebuffer) {
		super(game);
		this.game = game;
		this.framebuffer = framebuffer;
		this.holder = getHolder();
		disableSurfaceViewLogging();
	}

	public void resume() {
		running = true;
		renderThread = new Thread(this);
		renderThread.start();
	}

	private void ProcessThread() {
		
		new Thread(new Runnable() {

			long l_processStartTime = System.nanoTime();

			public void run() {
				while (running) {

					if (!holder.getSurface().isValid()) {
						continue;
					}
					
					l_processStartTime = System.nanoTime();
					try {
						game.getCurrentScreen().update(1);
					}catch(Exception e) 
					{}
					doFpsCheck(l_processStartTime);
				}
			}
		}).start();

	}
	int ctr=0;

	public void run() {
		ProcessThread();

		Rect dstRect = new Rect();
		long l_startTimeRender = System.nanoTime();
		m_fps = 0;

		while (running) {
			if (!holder.getSurface().isValid()) {
				continue;
			}

			l_startTimeRender = System.nanoTime();
			try {
				game.getCurrentScreen().paint(2.55f); // 1.67//1.87
			} catch (Exception e)
			{ 
				if(ctr>80)
				genSet.ShowToast(e.getMessage());
				else
					ctr++;
			}
			Canvas canvas = holder.lockCanvas();
			canvas.getClipBounds(dstRect);
			canvas.drawBitmap(framebuffer, null, dstRect, null);
			holder.unlockCanvasAndPost(canvas);

//			doFpsCheck(l_startTimeRender);
		}

	}

	public void pause() {
		running = false;
		while (true) {
			try {
				renderThread.join();
				break;
			} catch (InterruptedException e) {
				// retry
			}

		}
	}

	private static final long SECOND = 1000000000;
	private static final long TARGET_FPS = 60;
	private static final long FRAME_PERIOD = SECOND / TARGET_FPS;

	/**
	 * 
	 * @param startTime
	 * @return <code>true</code> if the interval between startTime and the time when
	 *         this method was called is smaller or equal to the given frame period.
	 * 
	 *         Will return <code>false</code> if the interval was longer.
	 */  
	public boolean doFpsCheck(long startTime) {

		long sleepTime = (FRAME_PERIOD - (System.nanoTime() - startTime))/1000000;  
		if (sleepTime > 0) {
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) { 
			}
			return true;
		} else {
			return false;
		}
	}
	
	int m_fps;

	public int getFPS() {
		return m_fps;
	}
}