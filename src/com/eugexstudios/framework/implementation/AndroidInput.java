package com.eugexstudios.framework.implementation;

import java.lang.reflect.Field;
import java.util.List;

import android.content.Context;  
import android.view.View;

import com.eugexstudios.framework.Input;
import com.eugexstudios.framework.implementation.AccelerometerHandler;

public class AndroidInput implements Input {
    TouchHandler touchHandler;
    AccelerometerHandler accelHandler;

    public AndroidInput(Context context, View view, float scaleX, float scaleY) {
        accelHandler = new AccelerometerHandler(context);
 
       touchHandler = new SingleTouchHandler(view, scaleX, scaleY);
       disableTouchHandlerLogging();
       
    }

    private void disableTouchHandlerLogging() {
        try {
            Field field = Input.class.getDeclaredField("DEBUG");
            field.setAccessible(true);
            field.set(null, false); 
        } catch (Exception e) { 
        }
    }
    @Override
    public boolean isTouchDown(int pointer) {
        return touchHandler.isTouchDown(pointer);
    }

    @Override
    public int getTouchX(int pointer) {
        return touchHandler.getTouchX(pointer);
    }

    @Override
    public int getTouchY(int pointer) {
        return touchHandler.getTouchY(pointer);
    }

    @Override
    public List<TouchEvent> getTouchEvents() {
        return touchHandler.getTouchEvents();
    }

    @Override
    public float getAccelX() {
        return AccelerometerHandler.getAccelX();
    }

    @Override
    public float getAccelY() {
        return AccelerometerHandler.getAccelY();
    }

    @Override
    public float getAccelZ() {
        return AccelerometerHandler.getAccelZ();
    }

}
