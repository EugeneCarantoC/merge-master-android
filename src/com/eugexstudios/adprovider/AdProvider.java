package com.eugexstudios.adprovider;

import android.graphics.Bitmap;

import com.eugexstudios.adprovider.StartApp.VideoAdType;
import com.eugexstudios.utils.genSet;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
  
public class AdProvider 
{ 
	
	public void StartAppShowInsti(AdDisplayListener p_adDisplayListener) 
	{
		genSet.game.StartAppShowInsti(p_adDisplayListener); 
	}

	public boolean isOfferWallReady() 
	{
		return genSet.game.StartAppIsOfferWallReady();
	}
	public boolean isInstiReady() 
	{
		return genSet.game.StartAppIsInstiReady();
	}
	public void ShowOfferWall(AdDisplayListener p_adDisplayListener) 
	{
		genSet.game.StartAppShowOfferWall(p_adDisplayListener);
	}

	public void RequestOfferWallAd() 
	{
		genSet.game.StartAppRequestOfferWallAd();
	}
	public void RequestInstiAd() 
	{
		genSet.game.StartAppRequestInstiAd();
	}
	
	public void RequestNativeAd(String p_adTag)
	{
		genSet.game.StartAppRequestNativeAd(p_adTag);
	}
	public void RequestVideoAd(String p_adTag, VideoAdType p_vidAdType) 
	{
		genSet.game.StartAppRequestVideoAd(p_adTag,p_vidAdType);
	}
	public boolean HasNativeAd()
	{
		return genSet.game.StartAppHasNativeAd();
	}
	public boolean isVideoAdAvailable()
	{
		return genSet.game.StartAppIsVidAdAvailable();
	}
	public Bitmap GetNativeAd2ndBitmap()
	{
		return genSet.game.StartAppGetNativeAd2ndBitmap();
	}	
	public void ShowVideoAds()
	{
		  genSet.game.StartAppShowVideoAds();
	}	
	
	public Bitmap GetNativeAdBitmap()
	{
		return genSet.game.StartAppGetNativeAdBitmap();
	}
	public String GetNativeAdTitle()
	{
		return genSet.game.StartAppGetNativeAdTitle();
	}
	public String GetNativeAdDesc()
	{
		return genSet.game.StartAppGetNativeAdDesc();
	}

	public void SendImpression()
	{
		genSet.game.StartAppSendImpression();
	}
	public void SendClick()
 	{ 
		genSet.game.StartAppSendClick(); 
 	} 
	public void ShowExitAds()
 	{ 
		genSet.game.StartAppExitAd(); 
 	} 
 	public void RestartSession()
 	{  
 	} 
 	 
	
}
