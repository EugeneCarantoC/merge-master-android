package com.eugexstudios.mergemaster.levels;

import android.graphics.Color;

import com.eugexstudios.mergemaster.Level;
import com.eugexstudios.mergemaster.Tile;
import com.eugexstudios.mergemaster.creator.LevelUtil;
import com.eugexstudios.mergemaster.creator.TileGroupDetail;
import com.eugexstudios.utils.ColorManager;

public class Mode_3_Levels {

	public static Level get(int p_lvl)
	{
		switch(p_lvl)
	    { 
			case 1: return  LevelUtil.ParseSerial("3,3,-15014710:2,-15014710:2,-26881:2,-15014710:3,-26881:3,-26881:3,-15014710:2,-26881:3,-26881:3"); 
			case 2: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 3: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 4: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 5: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 6: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 7: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 8: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 9: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 10: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 11: return  LevelUtil.ParseSerial("AAAAAa");
			case 12: return  LevelUtil.ParseSerial("AAAAAa");  
			case 13: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 14: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 15: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 16: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 17: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 18: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 19: return  LevelUtil.ParseSerial("AAAAAa");
			case 20: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 21: return  LevelUtil.ParseSerial("AAAAAa");
			case 22: return  LevelUtil.ParseSerial("AAAAAa");  
			case 23: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 24: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 25: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 26: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 27: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 28: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 29: return  LevelUtil.ParseSerial("AAAAAa");  
			case 30: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 31: return  LevelUtil.ParseSerial("AAAAAa");
			case 32: return  LevelUtil.ParseSerial("AAAAAa");  
			case 33: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 34: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 35: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 36: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 37: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 38: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 39: return  LevelUtil.ParseSerial("AAAAAa");  
			case 40: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 41: return  LevelUtil.ParseSerial("AAAAAa");
			case 42: return  LevelUtil.ParseSerial("AAAAAa");  
			case 43: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 44: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 45: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 46: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 47: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 48: return  LevelUtil.ParseSerial("AAAAAa"); 
			case 49: return  LevelUtil.ParseSerial("AAAAAa");  
			case 50: return  LevelUtil.ParseSerial("AAAAAa");   

	    }
		return null;
	} 
	
//	private static Level GetLevel_af()
//	{ 
// 
//		int l_val[][] = new int[][]
//				{
//				   {1,1,1,1,1}
//				  ,{2,1,1,1,1}
//				  ,{2,1,2,1,1}
//				  ,{1,1,1,1,1}
//				  ,{1,1,1,1,1}
//				};  
//		int l_color[][] = new int[][]
//				{
//				    {ColorManager.CYAN_2,ColorManager.CYAN_2,ColorManager.ORANGE_2,ColorManager.ORANGE_2,ColorManager.ORANGE_2} 
//				   ,{ColorManager.CYAN_2,ColorManager.ORANGE_2,ColorManager.CYAN_2,ColorManager.ORANGE_2,ColorManager.ORANGE_2} 
//				   ,{ColorManager.CYAN_2,ColorManager.ORANGE_2,ColorManager.ORANGE_2,ColorManager.ORANGE_2,ColorManager.VIOLET_2} 
//				   ,{ColorManager.CYAN_2,ColorManager.ORANGE_2,ColorManager.ORANGE_2,ColorManager.ORANGE_2,ColorManager.VIOLET_2} 
//				   ,{ColorManager.MAROON_1,ColorManager.MAROON_1,ColorManager.MAROON_1,ColorManager.VIOLET_2,ColorManager.VIOLET_2} 
//				}; 
// 
//		TileGroupDetail l_detail =  new TileGroupDetail(l_val.length,l_val[0].length); 
//		Tile[][] l_tiles = LevelUtil.InitTile(l_detail, l_val,l_color); 
//		return new Level(l_tiles,l_color[0][0]);
//	} 
}
