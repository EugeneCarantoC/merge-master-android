package com.eugexstudios.mergemaster.levels;
 
import com.eugexstudios.mergemaster.Level;  
import com.eugexstudios.utils.Constants.game_mode;

public class LevelManager {
	 
	public static int GetMaxLevel(game_mode p_type)
	{
		switch(p_type)
		{
			case mode_1: return Mode_1_Levels.GetMaxLevel();
			case mode_2: return 1;
			case mode_3: return 1;
		}
		return 0;
	}

	public static Level GetLevel(game_mode p_type, int p_lvl)
	{ 
		switch(p_type)
		{
			case mode_1: return Mode_1_Levels.get(p_lvl);
			case mode_2: return Mode_2_Levels.get(p_lvl);
			case mode_3: return Mode_3_Levels.get(p_lvl);
		} 
		return null;
	}
	public static int determineStar(game_mode p_type, int p_lvl, int p_move)
	{ 
		switch(p_type)
		{
			case mode_1: return Mode_1_Levels.get(p_lvl).determineStar(p_move);
			case mode_2: return Mode_2_Levels.get(p_lvl).determineStar(p_move);
			case mode_3: return Mode_3_Levels.get(p_lvl).determineStar(p_move);
		} 
		return 0;
	}
	
	
}
