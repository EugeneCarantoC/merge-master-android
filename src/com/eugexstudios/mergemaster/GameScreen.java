package com.eugexstudios.mergemaster;

/*EugeX framework
 *April 12, 2015
 *Eugene C. Caranto
 * **/
//


/*
 * PROJECT NAME: TILE PROJECT WHEEL
 * DATE STARTED: 06/02/2018 2:59pm
 * 
 * 
 * */
 

import java.util.List;     
import android.graphics.Color; 
import android.graphics.Point;

import com.eugexstudios.adprovider.StartApp;
import com.eugexstudios.framework.Game;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Image;
import com.eugexstudios.framework.Screen;
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.mergemaster.creator.LevelCreatorScreen;
import com.eugexstudios.mergemaster.levels.LevelManager; 
import com.eugexstudios.utils.CONFIG;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Debugger; 
import com.eugexstudios.utils.SharedPref;
import com.eugexstudios.utils.genSet; 
import com.eugexstudios.utils.Constants.game_mode;

public class GameScreen extends Screen {
	enum androidState {
		Ready, Running, Paused, GameOver
	}

	public static enum LANGUAGE 
	{
		  ENGLISH (0)
		, JAPANESE (1)
		, SPANISH (2)
		, GERMAN(3)
		, KOREAN(4)
		, PORTUGUESE (5);
		
		private final int idx;
	    private LANGUAGE(int p_idx)
	    {
	        this.idx = p_idx;
	    }

	    public int getIdx() 
	    {
	        return idx;
	    }
	}
  
	public enum AppState {
		  onMenu, onPlay,   onCreator, SHOW_EXIT_AD,  onLevelSelect, onInstruction,
	}
	androidState state = androidState.Ready;
	 
	private static AppState 		   m_currentState;   
	private static boolean			   m_vibrateOn;
	private static boolean			   m_soundOn;
	private static boolean			   m_musicOn;
	private static LANGUAGE 		   m_lang;  
	private static PlayScreen     	   m_playScreen;
	private static LevelCreatorScreen  m_creatorScreen;
	private static InstructionScreen  m_instructionScreen;
	
	private static MenuScreen     	   m_menuScreen;  
	private static LevelSelectScreen   m_levelSelectScreen;
	TouchEvent m_lastTouchEvent;
	static game_mode m_gameMode;
	static int       m_level;
	
	public GameScreen(Game game) 
	{
		super(game);  
//		 AddQuickMergeAvailable(5);
//		 AddCoins(9999999);
 		if(hasGameData())
 		{  
 			LoadPrefData();  
 			updateAppOpenedCtr(); 			
 			checkVersionChange();
 		}
 		else
 		{
 			SetPrefDataForInitInstall();
 		}
 	
 		if(isInstructionBasicDone())
 		{
 	 		GoToMenu(false);	
 		}
 		else 
 		{ 
 			GoToInstructionBasics();
 		}
 		
 		////#################### KEEP THIS BLOCK COMMENTED WHEN ON PROD
 		PlayGame(game_mode.mode_1, 23,false);   	  
//			GoToInstructionBasics();
 		////#################### KEEP THIS BLOCK COMMENTED WHEN ON PROD
 
	 }
	
	private boolean isInstructionBasicDone() 
	{
		return game.GetSharedPrefBoolean(SharedPref.INSTRUCTION_BASIC_DONE);
	}
	
	private boolean hasGameData() 
	{
		return genSet.game.GetSharedPrefBoolean(SharedPref.INSTRUCTION_BASIC_DONE); 
	}
	
	private int GetAppOpenCtr()
	{
		return genSet.game.GetSharedPrefInt(SharedPref.APP_OPENED_CTR);
	}
	
	private void checkVersionChange() 
	{
	    if(!genSet.game.GetVersionName().equals(genSet.game.GetSharedPrefString(SharedPref.LAST_GAME_VERSION )))
	    { 
		    genSet.game.SetSharedPrefString(SharedPref.LAST_GAME_VERSION, genSet.game.GetVersionName()); 
//		    genSet.ShowToast("Version Changed");
	    }   
	}
	
	private void updateAppOpenedCtr() 
	{
		   genSet.game.SetSharedPrefInt(SharedPref.APP_OPENED_CTR, genSet.game.GetSharedPrefInt(SharedPref.APP_OPENED_CTR)+1);
	}
	
	public static String getGameTitle() 
	 {
		 return "Merge Master";
	 }
	
	public static void SkipCurrentLevel()
	 {
		 m_playScreen.SkipLevel();
		 genSet.ShowToast("Level skipped!");
	 }
	
	public static int[] getLevelStars(game_mode p_gameMode) 
	 {
		 int l_stars[] = new int[GetLevelReached(p_gameMode)];
		 for(int idx=0;idx<l_stars.length;idx++) 
		 {
			 l_stars[idx] = LevelManager.determineStar(p_gameMode,idx+1,GetBestMove(p_gameMode,idx+1));
		 }
		return l_stars;
	 }
	 
	public static int[] getBestMoves(game_mode p_gameMode) 
	 {
		 int l_stars[] = new int[GetLevelReached(p_gameMode)];
		 for(int idx=0;idx<l_stars.length;idx++) 
		 {
			 l_stars[idx] =  GetBestMove(p_gameMode,idx+1);
		 }
		return l_stars;
	 }
	 
 	 public static void GoToCreator()
	 {  
		 SetState(AppState.onCreator);
		 m_creatorScreen = new LevelCreatorScreen(3,3);   
	 }  

	 public static void PlayGame(game_mode p_gameMode, int p_level,boolean p_willReplay) 
	 { 
		SetState(AppState.onPlay); 
		m_gameMode = p_gameMode;
		m_level    = p_level;
	    m_playScreen = new PlayScreen(p_gameMode,p_level,p_willReplay);   
	 }

 	 public static void LevelComplete(game_mode p_mode, int p_level, int p_moveCount) 
	 { 
 		 int l_levelUnlocked =-1;
 		 switch(p_mode)
 		 {
 		 	case mode_1:
	 			l_levelUnlocked= genSet.game.GetSharedPrefInt(SharedPref.LVL_REACHED_MODE_1);
	 
	 			if(l_levelUnlocked==LevelManager.GetMaxLevel(m_gameMode))
	 			{

//	 	 	 		 genSet.ShowToast("All levels complete!");
	 			}
	 			else if(l_levelUnlocked<p_level+1)
	 			{
	 				genSet.game.SetSharedPrefInt(SharedPref.LVL_REACHED_MODE_1, l_levelUnlocked+1);
//	 	 	 		 genSet.ShowToast(genSet.game.GetSharedPrefInt(SharedPref.LVL_REACHED_MODE_1)+"");
	 			}
 			 break;
		default:
			break;
 		 } 
 		  
		String l_sharedPreffPrefix ="";
		switch(p_mode)
		{
			case mode_1: l_sharedPreffPrefix = SharedPref.BEST_MOVE_COUNT_A_; break; 
			case mode_2: l_sharedPreffPrefix = SharedPref.BEST_MOVE_COUNT_B_; break; 
			case mode_3: l_sharedPreffPrefix = SharedPref.BEST_MOVE_COUNT_C_; break; 
		}
		
		int l_bestMove = genSet.game.GetSharedPrefInt(l_sharedPreffPrefix+p_level);
		l_bestMove = l_bestMove==-1?10000:l_bestMove;
		if(p_moveCount<l_bestMove)
		{   
			genSet.game.SetSharedPrefInt(SharedPref.BEST_MOVE_COUNT_A_+p_level, p_moveCount); 
		}

	 } 

	 public static void GoToNextLevel() 
	 { 
		 m_level++;
		 boolean l_isReplay = GetLevelReached(m_gameMode)!=m_level;
		 PlayGame(m_gameMode, m_level,l_isReplay);
	 }	
	 
	 public static void ResetData()
	 { 
		 ClearData();
		 genSet.ShowToast("All data cleared!");
		 
	 }
	 
	 public static void Restart() 
	 { 
		 m_playScreen.Restart(true);
	 }
	 
	 public static void GoToInstructionBasics()
	 { 
		 SetState(AppState.onInstruction); 
		 m_instructionScreen = new InstructionScreen(InstructionScreen.type.BASICS);
	 }

	 public static void GoToMenu(boolean p_fadeIn)
	 { 
		 SetState(AppState.onMenu);
		 m_menuScreen = new MenuScreen(p_fadeIn); 
	 }

	 public static void ClearData()
	 { 
			genSet.game.SetSharedPrefBoolean(SharedPref.MUSIC_ON, true);
			genSet.game.SetSharedPrefBoolean(SharedPref.SOUND_ON, true); 
			genSet.game.SetSharedPrefBoolean(SharedPref.VIBRATE_ON, true);  
			genSet.game.SetSharedPrefBoolean(SharedPref.INSTRUCTION_BASIC_DONE, false);  
			genSet.game.SetSharedPrefInt(SharedPref.LANGUAGE_USED,  LANGUAGE.ENGLISH.getIdx());  

			genSet.game.SetSharedPrefInt(SharedPref.UNDO_AVAILABLE, 1); 
			genSet.game.SetSharedPrefInt(SharedPref.COINS, 1); 
			genSet.game.SetSharedPrefInt(SharedPref.QUICKMERGE_AVAILABLE, 1); 
			
			
			genSet.game.SetSharedPrefInt(SharedPref.LVL_REACHED_MODE_1, 1); 
			genSet.game.SetSharedPrefInt(SharedPref.LVL_REACHED_MODE_2, 1); 
			genSet.game.SetSharedPrefInt(SharedPref.LVL_REACHED_MODE_3, 1); 

			genSet.game.SetSharedPrefInt(SharedPref.SERIAL_CREATED_CTR, 1); 
			

			 for(int idx=0;idx<LevelManager.GetMaxLevel(game_mode.mode_1);idx++) 
			 {
					genSet.game.SetSharedPrefInt(SharedPref.BEST_MOVE_COUNT_A_+idx, 1000000); 
			 }

	 }

	 public static int GetUndoAvailable() 
	 {
		return genSet.game.GetSharedPrefInt(SharedPref.UNDO_AVAILABLE);
	 }

	 public static int GetQuickMergeAvailable() 
	 { 
		return genSet.game.GetSharedPrefInt(SharedPref.QUICKMERGE_AVAILABLE);
	 }
	 
	 public static void AddUndoAvailable(int p_add) 
	 {
		  genSet.game.SetSharedPrefInt(SharedPref.UNDO_AVAILABLE,genSet.game.GetSharedPrefInt(SharedPref.UNDO_AVAILABLE)+p_add);
	 } 
	  
	 public static void AddQuickMergeAvailable(int p_add) 
	 {
		  genSet.game.SetSharedPrefInt(SharedPref.QUICKMERGE_AVAILABLE,genSet.game.GetSharedPrefInt(SharedPref.QUICKMERGE_AVAILABLE)+p_add);
	 }
	 
	 public static int UseUndo() 
	 {
		  genSet.game.SetSharedPrefInt(SharedPref.UNDO_AVAILABLE,genSet.game.GetSharedPrefInt(SharedPref.UNDO_AVAILABLE)-1);
		  return  genSet.game.GetSharedPrefInt(SharedPref.UNDO_AVAILABLE);
	 }
	 
	 public static int UseQuickMerge() 
	 {
		  genSet.game.SetSharedPrefInt(SharedPref.QUICKMERGE_AVAILABLE,genSet.game.GetSharedPrefInt(SharedPref.QUICKMERGE_AVAILABLE)-1);
		  return  genSet.game.GetSharedPrefInt(SharedPref.QUICKMERGE_AVAILABLE);
	 }
	 
	 
	 public static int GetCoins() 
	 {
		return genSet.game.GetSharedPrefInt(SharedPref.COINS);
	 }

	 public static void AddCoins(int p_add) 
	 {
		  genSet.game.SetSharedPrefInt(SharedPref.COINS,genSet.game.GetSharedPrefInt(SharedPref.COINS)+p_add);
	 }
	 
	 public static void SetPrefDataForInitInstall()
	 {  
		genSet.game.SetSharedPrefBoolean(SharedPref.HAS_GAME_DATA, true);  
		genSet.game.SetSharedPrefInt(SharedPref.APP_OPENED_CTR, 1);  
	 	genSet.game.SetSharedPrefString(SharedPref.LAST_GAME_VERSION,  genSet.game.GetVersionName()); 
		ClearData(); 
		LoadPrefData(); 
	 }  
	
	public static int GetBestMove(game_mode p_mode, int p_level)
	{
		switch(p_mode)
		{
			case mode_1:return genSet.game.GetSharedPrefInt(SharedPref.BEST_MOVE_COUNT_A_+p_level);  
			case mode_2:return genSet.game.GetSharedPrefInt(SharedPref.BEST_MOVE_COUNT_B_+p_level);  
			case mode_3:return genSet.game.GetSharedPrefInt(SharedPref.BEST_MOVE_COUNT_C_+p_level);   
			default: return 10000000;
		}
		
	}
	
	private static void LoadPrefData()
	{ 
		m_soundOn      = genSet.game.GetSharedPrefBoolean(SharedPref.SOUND_ON);
		m_musicOn      = genSet.game.GetSharedPrefBoolean(SharedPref.MUSIC_ON);
		m_vibrateOn    = genSet.game.GetSharedPrefBoolean(SharedPref.VIBRATE_ON);   
		m_lang         = LANGUAGE.values()[genSet.game.GetSharedPrefInt(SharedPref.LANGUAGE_USED)]; 	 
	}
	
	public static void RateApp()
	{ 		
		genSet.game.RateApp();
	} 
	
	public static void SetLanguage(LANGUAGE p_lang)
	{
		m_lang         = p_lang;
		genSet.game.SetSharedPrefInt(SharedPref.LANGUAGE_USED, m_lang.getIdx()); 
	}
	
	public static void ToggleSoundSettings()
	{ 
		m_soundOn=!m_soundOn; 
		genSet.game.SetSharedPrefBoolean(SharedPref.SOUND_ON,m_soundOn);
	} 
	
	public static void ToggleMusicSettings()
	{ 
		m_musicOn=!m_musicOn; 
		genSet.game.SetSharedPrefBoolean(SharedPref.MUSIC_ON,m_musicOn);
	} 
	
	public static void ToggleVirbrateSettings()
	{ 
		m_vibrateOn=!m_vibrateOn; 
		genSet.game.SetSharedPrefBoolean(SharedPref.VIBRATE_ON,m_vibrateOn);
	} 
	
	public static boolean IsSoundOn()
	{
		return m_soundOn;
	}
	
	public static boolean IsMusicOn()
	{
		return m_musicOn;
	}	
	
	public static boolean IsVibrateOn()
	{
		return m_vibrateOn;
	}
	
	public static LANGUAGE GetLanguage()
	{
		return m_lang; 
	}
	 
	public static String GetLanguageCode()
	{
		switch(m_lang)
		{
		case ENGLISH: return "en";
		case JAPANESE: return "ja";
		case SPANISH: return "es";
		case GERMAN: return "de";
		case KOREAN: return "ko";
		case PORTUGUESE: return "pt";
		
		}
		return "en";
	} 
	 
	public static int GetLevelReached(game_mode p_gameMode)
	 { 
		switch(p_gameMode)
		{
			case mode_1:return genSet.game.GetSharedPrefInt(SharedPref.LVL_REACHED_MODE_1);  
			case mode_2:return genSet.game.GetSharedPrefInt(SharedPref.LVL_REACHED_MODE_2);  
			case mode_3:return genSet.game.GetSharedPrefInt(SharedPref.LVL_REACHED_MODE_3); 
			default: return 0;
		} 
	 }
	
	public static void ResetAllData()
	{  
		SetPrefDataForInitInstall();
		genSet.ShowToast("Data successfully cleared.");
	}
	
	public void gameUpdates(float p_deltaTime) 
	{  							  
		 
		switch (m_currentState)
		{
			case onCreator: m_creatorScreen.Updates(p_deltaTime);break;  
			case onPlay:  m_playScreen.Updates(p_deltaTime); break;  
			case onMenu:  m_menuScreen.Updates(p_deltaTime); break;  
			case onLevelSelect: m_levelSelectScreen.Updates(p_deltaTime);break; 
			case onInstruction:m_instructionScreen.Updates(p_deltaTime);break;
			default:break;
		}  
	}
 
	@Override
	public void paint(float deltaTime)
	{ 
		Graphics g = game.getGraphics(); 
		 g.drawRect(Constants.SCREEN_RECT, Color.WHITE);
		switch (m_currentState)
		{ 
			case onCreator: m_creatorScreen.Paint(g);break;  
			case onPlay:    m_playScreen.Paint(g);  break; 
			case onMenu:    m_menuScreen.Paint(g); break;   
			case onLevelSelect:m_levelSelectScreen.Paint(g);break;
			case onInstruction:m_instructionScreen.Paint(g);break;
			default:break;
		}  
  
//		if(Debugger.show_touches&&!Debugger.PROD_MODE &&m_lastTouchEvent!=null)
//		{      
//			genSet.setTextProperties( 35, Color.RED, Align.LEFT);
//			g.drawString(m_lastTouchEvent.type+"", 50, 50+36*0, genSet.paint); 
//			g.drawString(m_lastTouchEvent.x + "," + m_lastTouchEvent.y, 50, 50+36*1, genSet.paint); 
//		} 

//		genSet.setTextProperties(50, Color.RED, Align.CENTER);
//		g.drawString("FPS: " + genSet.game.getFPS()+"", 200,200, genSet.paint);
//		g.drawString("M: " + System.currentTimeMillis()+"", 200,400, genSet.paint);
		if(Debugger.show_grid&&!Debugger.PROD_MODE )
		{    
			g.drawLine(new Point(CONFIG.SCREEN_MID,0), new Point(CONFIG.SCREEN_MID,CONFIG.SCREEN_HEIGHT), Color.RED);
		}
		
	}
	
	private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
		int len = touchEvents.size();

		for (int i = 0; i < len; i++)
		{
			TouchEvent event = touchEvents.get(i);
			switch (m_currentState) 
			{ 
				case SHOW_EXIT_AD: 	break; 
				case onMenu:    m_menuScreen.TouchUpdates(event);    break;  
				case onCreator: m_creatorScreen.TouchUpdates(event); break;  
				case onPlay:    m_playScreen.TouchUpdates(event);    break; 
				case onLevelSelect: m_levelSelectScreen.TouchUpdates(event);	break;
				case onInstruction: 	m_instructionScreen.TouchUpdates(event);break;
				default: break;  
				
			}  
			
//			if(event.type == TouchEvent.TOUCH_DOWN)
//			{
//	 			game.hideSystemUI();
//			}
//				m_postool.positioningToolTouch(event);
			m_lastTouchEvent = event;
		}
	} 
	
	public static void RemoveAdRequest()
	{
		
	}
	
	public void PaintLevelSelect(Graphics g)
	{ 
//		PaintIdle(g); 
		m_levelSelectScreen.Paint(g);
	}

	public static void CloseGame() 
	{
		android.os.Process.killProcess(android.os.Process.myPid());
	}
	  
	@Override
	public void backButton() 
	{ 
		switch (m_currentState)
		{ 
			case onPlay:    m_playScreen.backButton(); break;  
			case onMenu: m_menuScreen.backButton(); break;  
			case onCreator:	m_creatorScreen.backButton(); break;
			case onLevelSelect: m_levelSelectScreen.backButton();break;
			case onInstruction:m_instructionScreen.backButton(); break;
			default: break;
		} 
	}
	 
	public static void SetState(AppState p_newState) 
	{ 
		m_currentState = p_newState;
	}
	
	public static AppState GetState() 
	{ 
		return m_currentState ;
	}
 

	public int[] paintButton(int g, Image buttonImage, int xPos, int yPos, int sourceX, int sourceY, int l, int w, int buttonValue, int d1, int d2) {

		return new int[] { xPos, yPos, l, w, buttonValue };
	}
 
	public boolean inBounds(TouchEvent event, int x, int y, int width, int height) 
	{
		if (event.x > x && event.x < x + width - 1 && event.y > y && event.y < y + height - 1)
			return true;
		else
			return false;
	}

	@Override
	public void pause() {
		if (state == androidState.Running)
			state = androidState.Paused;

		// if(gameState!=onPlayOver)
		// pauseGame();

	}

	@Override
	public void resume() 
	{
		if (state == androidState.Paused)
			state = androidState.Running;

	}

	public void update(float deltaTime) 
	{
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		if (state == androidState.Ready)
			updateReady(touchEvents);
		if (state == androidState.Running)
			updateRunning(touchEvents, deltaTime);
		if (state == androidState.Paused)
			updateRunning(touchEvents, deltaTime);


		gameUpdates(deltaTime);
	}

	private void updateReady(List<TouchEvent> touchEvents)
	{
		if (touchEvents.size() > 0)
			state = androidState.Running;
	} 
	
	@Override
	public void dispose()
	{ 
		game.showToast("dispose!"); 
	}

	public static void GoToLevelSelect() { 

		 SetState(AppState.onLevelSelect); 
		
	}

	public static void GoToLevelSelect(game_mode p_gameMode, int p_levelPlaying, boolean p_fadeIn) { 

		 m_levelSelectScreen = new LevelSelectScreen(p_gameMode, p_levelPlaying, p_fadeIn);
		 SetState(AppState.onLevelSelect); 
		
	}
	
	public static void InitializeLevelSelect(game_mode p_gameMode, int p_levelPlaying, boolean p_fadeIn) { 
 
		 m_levelSelectScreen = new LevelSelectScreen(p_gameMode, p_levelPlaying, p_fadeIn);
		
	}

	public static int getLevelPlaying() {
		return m_level;
	}

	public static void PurchUndo()
	{
		 AddUndoAvailable(Constants.UNDO_PURCH_QTY); 
		 m_playScreen.GoToPurchaseSuccessNotif(StartApp.VideoAdType.RewardToAddUndo);
		
	}
	
	public static void purchQucikMerge()
	{
		 AddQuickMergeAvailable(1); 
		 m_playScreen.GoToPurchaseSuccessNotif(StartApp.VideoAdType.RewardToAddQMerge);
		
	} 
	
	public static boolean isOnTutorial() {
		return m_currentState== AppState.onInstruction;
	}
 
}