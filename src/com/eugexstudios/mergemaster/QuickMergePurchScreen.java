package com.eugexstudios.mergemaster;

import com.eugexstudios.adprovider.AdProvider;
import com.eugexstudios.adprovider.StartApp.VideoAdType;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.mergemaster.AskUserWindow.QUESTION;
import com.eugexstudios.utils.AssetManager; 
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Screen;
import com.eugexstudios.utils.genSet;

import android.graphics.Color;

public class QuickMergePurchScreen extends Screen{

	private enum state{IDLE, NO_CONNECT, PLACEHOLDER_2};
	private state m_state;

	private final int
			BUY_BTN =1,
			WATCH_BTN=2,
			CLOSE_BTN=3,
			ESCAPE_BTN=4;
	
	private final static int WINDOW_POS_X = Constants.ScaleX(40);
	private final static int WINDOW_POS_Y = Constants.ScaleY(330);

	private final static int BUY_BTN_POS_X = Constants.ScaleX(70); 
	private final static int BUY_BTN_POS_Y = WINDOW_POS_Y+Constants.ScaleY(195); 

	private final static int WATCH_BTN_POS_X = Constants.ScaleX(70); 
	private final static int WATCH_BTN_POS_Y = WINDOW_POS_Y+Constants.ScaleY(390); 

	private final static int CLOSE_BTN_POS_X = Constants.ScaleX(612); 
	private final static int CLOSE_BTN_POS_Y = WINDOW_POS_Y+Constants.ScaleY(-18); 

	private static int DIM_COLOR = Color.argb(180, 0, 0, 0);
	
	private static AdProvider 		             m_adProvider; 
	private AskUserWindow			             m_notifWindow;
	private boolean 	                         m_close;
	
	public QuickMergePurchScreen() {
		m_state = state.IDLE;	
	    m_adProvider = new AdProvider(); 
		m_adProvider.RequestVideoAd(Constants.REWARD_VIDEO_AD_BUY_QMERGE,VideoAdType.RewardToAddQMerge);
		m_close=false;
 	}
	
	@Override
	protected void Paint(Graphics g) {
		g.drawRect(Constants.SCREEN_RECT, DIM_COLOR);
		switch(m_state) 
		{
			case IDLE:           PaintIdle(g);      break;
			case NO_CONNECT:     PaintNoConnect(g); break;
			case PLACEHOLDER_2: break;
		}
	}
	
	private void PaintIdle(Graphics g) 
	{
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, WINDOW_POS_Y, Constants.WINDOW_2);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, WINDOW_POS_Y, Constants.NEED_QMERGE_HEADER);
		 
//		drawRoundRect(g, BUY_BTN_POS_X,BUY_BTN_POS_Y,Constants.BUY_UNDO_BTN, Color.BLACK, Color.BLACK, BUY_BTN);
		g.drawImage(AssetManager.sprites, BUY_BTN_POS_X+(buttonPressed==BUY_BTN?-3:0) , BUY_BTN_POS_Y+(buttonPressed==BUY_BTN?4:0) , Constants.BUY_QMERGE_BTN);
		
//		drawRoundRect(g, WATCH_BTN_POS_X, WATCH_BTN_POS_Y, Constants.WATCH_FOR_UNDO_BTN, Color.BLACK, Color.BLACK, WATCH_BTN);
		g.drawImage(AssetManager.sprites, WATCH_BTN_POS_X + (buttonPressed==WATCH_BTN?-3:0), WATCH_BTN_POS_Y + (buttonPressed==WATCH_BTN?4:0) , Constants.WATCH_FOR_QMERGE_BTN);
		
//		drawRoundRect(g, CLOSE_BTN_POS_X, CLOSE_BTN_POS_Y, Constants.CLOSE_WINDOW_BTN, Color.BLACK, Color.BLACK, CLOSE_BTN);
		g.drawImage(AssetManager.sprites, CLOSE_BTN_POS_X + (buttonPressed==CLOSE_BTN?-3:0), CLOSE_BTN_POS_Y + (buttonPressed==CLOSE_BTN?4:0) , Constants.CLOSE_WINDOW_BTN);
		
  
	}
	
	private void IdleTouchUpdates(TouchEvent g)
	{
		if(g.type == TouchEvent.TOUCH_DOWN)
		{     

			drawRoundRect(g, BUY_BTN_POS_X,BUY_BTN_POS_Y,Constants.BUY_UNDO_BTN, Color.BLACK, Color.BLACK, BUY_BTN);
			drawRoundRect(g, WATCH_BTN_POS_X, WATCH_BTN_POS_Y, Constants.WATCH_FOR_UNDO_BTN, Color.BLACK, Color.BLACK, WATCH_BTN);
			drawRoundRect(g, CLOSE_BTN_POS_X, CLOSE_BTN_POS_Y, Constants.CLOSE_WINDOW_BTN, Color.BLACK, Color.BLACK, CLOSE_BTN);
			 
		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case BUY_BTN:     BuyQuickMerge();  break;
				case WATCH_BTN:   WatchVideo(); break; 
				case CLOSE_BTN: m_close=true;break;
			}

			if(buttonPressed>0) 
			{
				AssetManager.btn_1.play();
			}
			buttonPressed = 0;
		} 
	} 

	public boolean isClosed() 
	{
		return m_close;
	};
	
	private void PaintNoConnect(Graphics g) 
	{
		PaintIdle(g);
		m_notifWindow.Paint(g);
	}
	
	private void GoToNoConnectNotif() 
	{
		m_state = state.NO_CONNECT;
		m_notifWindow = new AskUserWindow(QUESTION.NO_CONNECT);
		AssetManager.err_pop_up.play();
		
	}
	
	private void BuyQuickMerge() 
	{
		if(GameScreen.GetCoins()<Constants.QMERGE_PURCH_COIN_COST)
		{
			genSet.ShowToast("Insufficient coins");
		}
		else 
		{
			GameScreen.AddCoins(-Constants.QMERGE_PURCH_COIN_COST);
			GameScreen.purchQucikMerge();
			m_close=true;
		}
	}
 
	private void WatchVideo()
	{ 
		if(m_adProvider.isVideoAdAvailable())
		{ 
			m_adProvider.SendImpression();
			m_adProvider.ShowVideoAds(); 
		} 
		else 
		{
			GoToNoConnectNotif();
		}
	}
	
	@Override
	protected void Updates(float p_deltaTime) {
		switch(m_state) 
		{
			case IDLE: break;
			case NO_CONNECT: if(m_notifWindow.isClosed()) { m_state = state.IDLE;} break;
			case PLACEHOLDER_2: break;
		}
	}

	@Override
	protected void TouchUpdates(TouchEvent g) {
		switch(m_state) 
		{
			case IDLE:         IdleTouchUpdates(g);break;
			case NO_CONNECT:   m_notifWindow.TouchUpdates(g); break;
			case PLACEHOLDER_2: break;
		}
		
	}

	@Override
	protected void backButton() {
		switch(m_state) 
		{
			case IDLE: m_close = true; break;
			case NO_CONNECT: m_state = state.IDLE;break;
			case PLACEHOLDER_2: break;
		}
	}

}
