package com.eugexstudios.mergemaster;

import android.graphics.Color;
import android.graphics.Paint.Align;
import android.graphics.Point;
import android.graphics.Rect; 

import com.eugexstudios.framework.Graphics; 
import com.eugexstudios.framework.ImageData;
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.mergemaster.levels.LevelManager;
import com.eugexstudios.utils.AssetManager;
import com.eugexstudios.utils.CONFIG;
import com.eugexstudios.utils.ColorManager;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Screen;
import com.eugexstudios.utils.genSet; 
import com.eugexstudios.utils.Constants.game_mode;

public class LevelSelectScreen extends Screen {
	
	final int 
	  YES_BTN        = 1
	, NO_BTN         = 2
	,HOME_BTN = 3000;
	       
	private Point SELECT_LEVEL_POS = new Point(15,103); 
	final int SHADOW_COLOR = Color.argb(62, 0, 0, 0); 
	final int LOCKED_LEVEL_COLOR = Color.argb(210, 100, 100, 100);
 
	
	enum states {
	  	 IDLE    
		  	,FADE_OUT    
		  	,FADE_IN    
	}; 
	private states                     m_state;  
	private int 			           m_maxLevel;
	private int 			           m_levelUnlocked;
	private game_mode m_gameMode;
    private enum exit_dest {MENU,PLAY};
    private exit_dest m_exitDest;
	private int m_levelDest;
	private boolean m_willReplayLevel;
	
	private ImageData HEADER_IMAGE = new ImageData(720,0,720,1280);
	private final int BTN_MARGIN_X = Constants.ScaleX(37);
	private final int BTN_MARGIN_Y = Constants.ScaleY(180);

	private final int BTN_GAP_X =  (int)(Constants.LEVEL_BTN.width*CONFIG.X_SCALE)+ Constants.ScaleX(19);
	private final int BTN_GAP_Y =  (int)(Constants.LEVEL_BTN.height*CONFIG.Y_SCALE)+ Constants.ScaleY(20);
	private final int TEXT_ALIGN_X = (int)(Constants.LEVEL_BTN.width*CONFIG.X_SCALE)/2;
	private final int TEXT_ALIGN_Y = (int)(Constants.LEVEL_BTN.height*CONFIG.Y_SCALE)/2+ Constants.ScaleY(32);
	
	private int                                  m_dragY; 
	private boolean                              m_onDrag;
	private int                                  m_holdPosY; 
	private Backbutton                           m_backButton; 
	private int DRAG_UP_LIMIT ;
	private int DRAG_DOWN_LIMIT        = CONFIG.SCREEN_HEIGHT-Constants.ScaleY(100);   
	private int SCROLL_LIMIT_DOWN      = 100;   
	private final int COLUMN_SIZE      = 4;   
	private int m_levelStars[];
	private int m_topMoves[];
	private boolean m_dragAllowed;
	private int m_touchDownY;;
	
	private float[] m_anim;

	private Background                           m_background;
	private float m_dragSlide;
	
	public LevelSelectScreen(game_mode p_gameMode, int p_lastLevel, boolean p_fadeIn) 
	{ 
//        genSet.game.getGraphics().drawRect(Constants.SCREEN_RECT,  ColorManager.GAME_THEME_1);
//		genSet.game.HideBannerAd();
		m_state = states.IDLE;    
		m_gameMode = p_gameMode;  
		m_dragY         = 0;
		m_holdPosY      = 0;
		m_onDrag        = false;
		m_backButton    = new Backbutton();  
		m_levelUnlocked = GameScreen.GetLevelReached(p_gameMode); 
		m_maxLevel      = LevelManager.GetMaxLevel(p_gameMode); 
        DRAG_UP_LIMIT   = -(((m_maxLevel/COLUMN_SIZE)*BTN_GAP_Y)-CONFIG.SCREEN_HEIGHT+Constants.ScaleY(450));   

        m_levelStars    = GameScreen.getLevelStars(m_gameMode);
        m_topMoves      = GameScreen.getBestMoves(m_gameMode);

		m_background = new Background();
        m_dragSlide     = 0;
        if(p_lastLevel>=15)
        {
        	m_dragY = -(BTN_GAP_Y*(((p_lastLevel-15)/COLUMN_SIZE)));
        } 
        
        if(p_fadeIn) 
        {
        	FadeIn();
        }
//        PlayGame(20);
	} 
  
	
	@Override
	public void Paint(Graphics g) {

		m_background.Paint(g); 
		g.setTypeface(AssetManager.Comforta,genSet.paint);
		switch(m_state)
		{
			case IDLE:     PaintIdle(g);break; 
			case FADE_OUT: PaintFadeOut(g);break;
			case FADE_IN:  PaintFadeIn(g);break;
		} 
		m_backButton.Paint(g);
	} 
	
	public void PaintFadeOut(Graphics g) 
	{ 
		PaintIdle(g);
		g.drawRect(Constants.SCREEN_RECT, Color.argb((int)(m_anim[0]), ColorManager.GAME_THEME_1_R,  ColorManager.GAME_THEME_1_G,  ColorManager.GAME_THEME_1_B));

		genSet.setTextProperties((35), Color.WHITE,Align.CENTER); 
		g.drawString("Loading", CONFIG.SCREEN_MID,CONFIG.SCREEN_MID_Y, genSet.paint);
	}
	
	public void PaintFadeIn(Graphics g) 
	{ 
		PaintIdle(g);
		g.drawRect(Constants.SCREEN_RECT, Color.argb((int)(m_anim[0]), ColorManager.GAME_THEME_1_R,  ColorManager.GAME_THEME_1_G,  ColorManager.GAME_THEME_1_B));
	}
		  
	private void PaintIdle(Graphics g) 
	{      

		for(int idx=0;idx<m_maxLevel/COLUMN_SIZE + (m_maxLevel%COLUMN_SIZE!=0?1:1);idx++) 
		{ 
			for(int idx2=0;idx2<COLUMN_SIZE;idx2++)
			{ 
				int l_btnIdx = idx*COLUMN_SIZE+idx2 +1;
				if(l_btnIdx-1<m_maxLevel)
				{
					int l_addPosX = BTN_MARGIN_X +(buttonPressed==(l_btnIdx)?-8:0) + idx2* BTN_GAP_X; 
					int l_addPosY = BTN_MARGIN_Y+ m_dragY+(buttonPressed==(l_btnIdx)?6:0) + idx*BTN_GAP_Y;
	//				drawRoundRect(g,  l_addPosX, l_addPosY,  Constants.LEVEL_BTN,Color.BLACK, Color.BLACK, l_btnIdx);
					if(l_btnIdx<m_levelUnlocked+1) 
					{
					 	if(l_btnIdx<m_levelUnlocked) 
					 	{
						 	g.drawImage(AssetManager.sprites, l_addPosX,l_addPosY, Constants.LEVEL_BTN);
					 		g.drawImage(AssetManager.sprites, l_addPosX,l_addPosY, Constants.LEVEL_STAR[m_levelStars[l_btnIdx-1]-1]);
					 		
					 	}
					 	else
					 	{ 
						 	g.drawImage(AssetManager.sprites, l_addPosX,l_addPosY, Constants.LEVEL_CURRENT_BTN); 
					 	}

						genSet.setTextProperties(40, SHADOW_COLOR, Align.CENTER);
					 	g.drawString((l_btnIdx)+"",  TEXT_ALIGN_X+l_addPosX+4,TEXT_ALIGN_Y+l_addPosY+3, genSet.paint);
						genSet.setTextProperties(40, Color.WHITE, Align.CENTER);
					 	g.drawString((l_btnIdx)+"",  TEXT_ALIGN_X+l_addPosX,TEXT_ALIGN_Y+l_addPosY, genSet.paint);
					 	 
					}
					else
					{ 
						genSet.setTextProperties(40, LOCKED_LEVEL_COLOR,Align.CENTER);
					 	g.drawImage(AssetManager.sprites,l_addPosX,l_addPosY, Constants.LEVEL_LOCK_BTN);
					 	g.drawString((l_btnIdx)+"",  TEXT_ALIGN_X+l_addPosX, TEXT_ALIGN_Y+l_addPosY-3, genSet.paint); 
					} 
				}

			} 
		} 
  
		g.drawImage(AssetManager.header_style_1,HEADER_IMAGE);
		genSet.setTextProperties((55), SHADOW_COLOR,Align.LEFT); 
		g.drawString("Select Level", SELECT_LEVEL_POS.x+4, SELECT_LEVEL_POS.y+4, genSet.paint);
		genSet.setTextProperties((55), Color.WHITE,Align.LEFT); 
		g.drawString("Select Level", SELECT_LEVEL_POS, genSet.paint);
		
//		drawRoundRect(g, HOME_BTN_BOUNDS, Color.RED,Color.RED, HOME_BTN);
		g.drawImage(AssetManager.sprites, Constants.LEVEL_SELECT_HOME_BTN);
		
//		genSet.setTextProperties(55, Color.DKGRAY,Align.CENTER);  
//		g.drawStringFont(m_dragY+"",CONFIG.SCREEN_MID+5, 170+5, AssetManager.Comforta, genSet.paint);
//		g.drawStringFont(DRAG_UP_LIMIT+"",CONFIG.SCREEN_MID, 170+70, AssetManager.Comforta, genSet.paint); 
		
	}
	private static Rect HOME_BTN_BOUNDS = new Rect(0, CONFIG.SCREEN_HEIGHT-Constants.ScaleY(100),Constants.ScaleX(150),CONFIG.SCREEN_HEIGHT);
	
	@Override
	public void Updates(float p_deltaTime) {
 

		m_background.Updates(p_deltaTime);
		if(m_backButton.isPressed())
		{ 
			backButton();
		}
		
		switch(m_state)
		{
			case IDLE:     IdleUpdates(p_deltaTime);    break; 
			case FADE_OUT: FadeOutUpdates(p_deltaTime); break; 
			case FADE_IN:  FadeInUpdates(p_deltaTime); break; 
		}  
	}

	@Override
	public void TouchUpdates(TouchEvent g) 
	{   
		switch(m_state)
		{
			case IDLE:    
				int l_adjustForBG= IdleTouchUpdates(g);
			    m_background.adjust(l_adjustForBG);   break; 
			case FADE_OUT:  break; 
			case FADE_IN:  ; break; 
		} 
	        
	} 

	private int IdleTouchUpdates(TouchEvent g) 
	{
		int l_dragSize=0;
		m_backButton.TouchUpdates(g);

		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			m_dragAllowed=true;
			for(int idx=0;idx<m_maxLevel/COLUMN_SIZE+(m_maxLevel%COLUMN_SIZE!=0?1:1);idx++) 
			{ 
				for(int idx2=0;idx2<COLUMN_SIZE;idx2++)
				{ 
					int l_btnIdx = idx*COLUMN_SIZE+idx2 +1;
					if(l_btnIdx<m_levelUnlocked+1) 
					{ 
						int l_addPosX = BTN_MARGIN_X +(buttonPressed==(l_btnIdx)?-8:0) + idx2* BTN_GAP_X; 
						int l_addPosY = BTN_MARGIN_Y+ m_dragY+(buttonPressed==(l_btnIdx)?6:0) + idx*BTN_GAP_Y;
						if(l_btnIdx<m_levelUnlocked+1) 
						{
							drawRoundRect(g,  l_addPosX, l_addPosY,  Constants.LEVEL_BTN,Color.BLACK, Color.BLACK, l_btnIdx);
						}
						else
						{  
						} 
					}
				} 
			} 
			m_touchDownY=g.y;

			drawRoundRect(g, HOME_BTN_BOUNDS, Color.RED,Color.RED, HOME_BTN);
		} 
		else if(g.type == TouchEvent.TOUCH_DRAGGED)
		{

			if(buttonPressed==0)
			{
				if(m_dragAllowed)
				{
					if(m_onDrag==false)
					{
						m_onDrag = true;
						m_holdPosY=g.y;
					}
					else
					{
						  l_dragSize = (int)((g.y -m_holdPosY)*1.2f);
						m_holdPosY= g.y; 
						m_dragY+=l_dragSize;  
						
//						if(m_dragY<DRAG_UP_LIMIT-CONFIG.SCREEN_HEIGHT/2)
//						{  
//							m_dragY  = DRAG_UP_LIMIT; 
//							m_onDrag=false;
//							m_dragAllowed=false;
//							l_dragSize=m_touchDownY-g.y;
//						}
						
						if(m_dragY>DRAG_DOWN_LIMIT)
						{  
							m_dragY  = 0; 
							m_onDrag=false;
							m_dragAllowed=false;
							l_dragSize=m_touchDownY-g.y;
						}
					}
				}
			}
			else 
			{
				buttonPressed=0;
				for(int idx=0;idx<m_maxLevel/COLUMN_SIZE+(m_maxLevel%COLUMN_SIZE!=0?1:1);idx++) 
				{ 
					for(int idx2=0;idx2<COLUMN_SIZE;idx2++)
					{ 
						int l_btnIdx = idx*COLUMN_SIZE+idx2 +1;
						if(l_btnIdx<m_levelUnlocked+1) 
						{ 
							int l_addPosX = BTN_MARGIN_X +(buttonPressed==(l_btnIdx)?-8:0) + idx2* BTN_GAP_X; 
							int l_addPosY = BTN_MARGIN_Y+ m_dragY+(buttonPressed==(l_btnIdx)?6:0) + idx*BTN_GAP_Y;
							if(l_btnIdx<m_levelUnlocked+1) 
							{
								drawRoundRect(g,  l_addPosX, l_addPosY,  Constants.LEVEL_BTN,Color.BLACK, Color.BLACK, l_btnIdx);
							} 
						}
					} 
				}
			}

			drawRoundRect(g, HOME_BTN_BOUNDS, Color.RED,Color.RED, HOME_BTN);
		}
		
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			if(buttonPressed!=0)
			{
				int l_level_dest =buttonPressed;   

				if(l_level_dest-1<m_levelUnlocked) 
				{
					boolean l_isRePlay = l_level_dest != m_levelUnlocked;
					 PlayGame(l_level_dest, l_isRePlay);
			    }
				
				else
				{
					switch(buttonPressed) 
					{
					case HOME_BTN: GameScreen.GoToMenu(false); break;
					}
				}
			}
			else
			{
				if(m_onDrag)
				{
					
					m_onDrag=false;   
					if(m_dragY<DRAG_UP_LIMIT)
					{ 
						m_dragY   = DRAG_UP_LIMIT;   
						l_dragSize=m_touchDownY-g.y;
					}
					
					if(m_dragY>SCROLL_LIMIT_DOWN)
					{ 
						m_dragY   = 0;   
						l_dragSize=m_touchDownY-g.y;
					}
					
//					if(m_dragY>MINIMUM_Y)
//					{
//						m_dragY   = MINIMUM_Y;   
//						l_dragSize=m_touchDownY-g.y;
//						
//					} 
					
//
//					if(l_dragSize>0)
//						m_dragSlide = -20;	
//					else
//						m_dragSlide = +20;
					
				}
			}
			if(buttonPressed!=0) 
			{ 
				AssetManager.btn_3.play();
			}
			buttonPressed=0;
		}
		
		return l_dragSize;
	}
 
	private void PlayGame(int p_levelDest, boolean p_isReplay) 
	{
		m_anim = new float[3];
		m_levelDest = p_levelDest;
		m_willReplayLevel = p_isReplay;
		FadeOut(exit_dest.PLAY);
	}  
	
	private void GoToMenu() 
	{ 
		FadeOut(exit_dest.MENU);
	}  
	
	private void FadeOut(exit_dest p_exitDest) 
	{
		m_exitDest = p_exitDest;
		m_state = states.FADE_OUT;
	}
	
	private void FadeIn() 
	{ 
		m_anim = new float[3];
		m_anim[0]=255;
		m_anim[1]=0;
		
		m_state = states.FADE_IN;
	}
	
	private void FadeInUpdates(float p_deltaTime)
	{
		/*
		 * 0- fade opacity for Argb
		 * 1- steps
		 * 2- anim buffer
		 * */
		
		if(m_anim[1]==0) 
		{
			if(m_anim[0]>0) 
			{
				m_anim[0] -= p_deltaTime*10;
				if(m_anim[0]<0)
					m_anim[0] = 0;
			}
			else 
			{
				m_anim[0] = 0;
				m_state = states.IDLE;
			}
		} 
		 
	}

	private void FadeOutUpdates(float p_deltaTime)
	{
		/*
		 * 0- fade opacity for Argb
		 * 1- steps
		 * 2- anim buffer
		 * */
		
		if(m_anim[1]==0) 
		{
			if(m_anim[0]<255) 
			{
				m_anim[0] += p_deltaTime*15;
				if(m_anim[0]>255)
					m_anim[0]=255;
			}
			else 
			{
				m_anim[0] = 255;
				m_anim[1] ++; 
				switch(m_exitDest) 
				{
					case PLAY:  break;
					case MENU: GameScreen.GoToMenu(true); break;
				}	
			}
		}
		else if(m_anim[1]==1) 
		{

			m_anim[2] += p_deltaTime*1;
			if(m_anim[2]>=100) 
			{ 
				switch(m_exitDest) 
				{
				case PLAY: GameScreen.PlayGame(m_gameMode, m_levelDest,m_willReplayLevel);  break;
				case MENU:   break;
				}
				m_anim[1]++;
			}
				
		}
		 
	}

	private void IdleUpdates(float p_deltaTime)
	{
		if((int)(m_dragSlide)>0) 
		{
			m_dragSlide-= 1*p_deltaTime;
			m_dragY -=m_dragSlide;
		}
		else if((int)(m_dragSlide)<0) 
		{
			m_dragSlide+= 1*p_deltaTime;
			m_dragY +=m_dragSlide;
		}
		
	}

	@Override
	public void backButton() 
	{		
		switch(m_state)
		{
			case IDLE: GameScreen.GoToMenu(false);break;
		default:
			break; 
		} 
		
	}

}
