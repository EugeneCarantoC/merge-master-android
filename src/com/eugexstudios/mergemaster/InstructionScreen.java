package com.eugexstudios.mergemaster;

import java.util.Vector;

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.mergemaster.creator.LevelUtil;
import com.eugexstudios.utils.AssetManager; 
import com.eugexstudios.utils.ColorManager;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Screen;
import com.eugexstudios.utils.SharedPref;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.Constants.game_mode;

import android.graphics.Color;
import android.graphics.Rect; 

public class InstructionScreen extends Screen {
	
	 enum type {
			BASICS 
		,PLACEHOLDER_2
		,PLACEHOLDER_3
		,PLACEHOLDER_4
		,PLACEHOLDER_5
		,PLACEHOLDER_6
		}; 
	private enum state {
		IDLE 
		,FADE_OUT
		,PLACEHOLDER_3
		,PLACEHOLDER_4
		,PLACEHOLDER_5
		,PLACEHOLDER_6
		}; 
	 
	private int m_instrPart;
	private boolean m_canNext; 
	private boolean m_isTalking;  

	private final int NEXT_BTN =1;
	private state m_state;
	private type m_type;
	private Master m_master;
	private PlayScreen m_playScreen;
	private static final int  NEXT_BTN_POS_X =Constants.ScaleX(510);
	private static final int  NEXT_BTN_POS_Y =Constants.ScaleY(1190);
	 
	private Vector<Rect> m_waitForInput;
	private int m_inputRequired;
	private int m_inputsCtr;
	
	public InstructionScreen(type p_type) 
	{ 
		m_type = p_type;
		m_state = state.IDLE;
		m_master = new Master();
		m_waitForInput = new Vector<Rect>();
		m_instrPart = 0;
		m_partDelay = 0;
		m_inputsCtr = 0;
		m_canNext = false; 
		m_inputRequired=0;
		m_playScreen = new PlayScreen(LevelUtil.ParseSerial(
		"<3,3,"
		+ "555:0,111:2,111:0,"
		+ "111:3,111:1,111:3,"
		+ "555:0,111:2,555:0>"
		));
 
	}
	@Override
	protected void Paint(Graphics g) { 
		switch(m_state)
		{
		case IDLE: PainIdle(g);break;
		case FADE_OUT: PaintFADE_OUT(g);break;
		case PLACEHOLDER_3: break;
		case PLACEHOLDER_4: break;
		case PLACEHOLDER_5: break;
		case PLACEHOLDER_6: break;
		}
	}
	
	private void PaintFADE_OUT(Graphics g) 
	{ 
		PainIdle(g);
		g.drawRect(Constants.SCREEN_RECT, Color.argb((int)(m_anim[0]), ColorManager.GAME_THEME_1_R,  ColorManager.GAME_THEME_1_G,  ColorManager.GAME_THEME_1_B));
		
	}
	private void PainIdle(Graphics g) 
	{ 
		switch (m_type) {
		case BASICS:  PaintBasics(g); break;
		case PLACEHOLDER_2: break;
		case PLACEHOLDER_3: break;
		case PLACEHOLDER_4: break;
		case PLACEHOLDER_5: break;
		case PLACEHOLDER_6: break; 
		default:
			break;
		}

	}
	
	private void PaintBasics(Graphics g) 
	{
		m_playScreen.Paint(g);  
		
		for(int idx=0;idx<m_waitForInput.size();idx++)
		{
			g.drawRect(m_waitForInput.get(idx), Color.argb(100, 0,0, 0));
		}
		
		m_master.Paint(g);
		
		if(m_canNext) 
		{
//			drawRoundRect(g,  NEXT_BTN_POS_X, NEXT_BTN_POS_Y, Constants.INSTR_NEXT,Color.BLACK,Color.BLACK,NEXT_BTN);
			g.drawImage(AssetManager.sprites, NEXT_BTN_POS_X + (buttonPressed==NEXT_BTN?3:0), NEXT_BTN_POS_Y+ (buttonPressed==NEXT_BTN?4:0), Constants.INSTR_NEXT);
		}
	}
	
	@Override
	protected void Updates(float p_deltaTime) {
		switch(m_state)
		{
		case IDLE: idleUpdates(p_deltaTime); break;
		case FADE_OUT: FadeOutUpdates(p_deltaTime);break;
		case PLACEHOLDER_3: break;
		case PLACEHOLDER_4: break;
		case PLACEHOLDER_5: break;
		case PLACEHOLDER_6: break;
		}
		
	}

	private float[] m_anim; 
	private void FadeOutUpdates(float p_deltaTime)
	{
		/*
		 * 0- fade opacity for Argb
		 * 1- steps
		 * 2- anim buffer
		 * */
		
		if(m_anim[1]==0) 
		{
			if(m_anim[0]<255) 
			{
				m_anim[0] += p_deltaTime*15;
				if(m_anim[0]>255)
					m_anim[0]=255;
			}
			else 
			{
				m_anim[0] = 255;
				m_anim[1] ++; 
		 
			}
		}
		else if(m_anim[1]==1) 
		{

			m_anim[2] += p_deltaTime*1;
			if(m_anim[2]>=100) 
			{ 
			 
				m_anim[1]++; 
	 			GameScreen.PlayGame(game_mode.mode_1, 1,true);   
			
			} 	
		}
		 
	}
	private void idleUpdates(float p_deltaTime) 
	{ 
		m_playScreen.Updates(p_deltaTime);	
		  
		m_master.Updates(p_deltaTime);

		switch(m_type)
		{
		case BASICS: basicsUpdates(p_deltaTime); break;
		}
		
	}

	private float m_partDelay;
	private void basicsUpdates(float p_deltaTime) 
	{
		if(m_instrPart==0)
		{
			//################################
			//## PART 0 - MASTER ENTRANCE
			//################################
			if(m_partDelay<10) // Delay to entrance
			{
				m_partDelay+=1*p_deltaTime;
			}
			else 
			{
				m_partDelay =0;
				m_instrPart =1;
				m_master.enter();
			}
		}
		else if(m_instrPart==1) 
		{
			if(m_master.isIdled())
			{
				//################################
				//## PART 1 - MASTER SAY 1
				//################################
				if(m_partDelay<1) // Delay to talk
				{
					m_partDelay+=1*p_deltaTime;
				}
				else 
				{
					m_partDelay = 0;
					m_instrPart = 2;
					m_canNext = false;
					m_isTalking = true;
					m_master.Say(new String[] {"So you want to be", "the next Merge ", "Master?"}); 
				}
			}
		}
		else if(m_instrPart==2) 
		{ 
			//################################
			//## PART 2 - WAIT FOR INTERUPTION OR NEXT BUTTON PRESS
			//################################
			

			if(m_isTalking) 
			{
				if(m_master.isWaitingFeedback()) 
				{
					m_canNext=true; 
				}
			}
			else
			{
				m_partDelay = 0;
				m_instrPart = 3;
				m_isTalking = true;
				m_canNext = false;
				m_master.Say(new String[] {"Let's see if you can", "master the art of ", "merging!"}); 
			}
		}

		else if(m_instrPart==3) 
		{ 
			//################################
			//## PART 3 - SHOW TILES 1
			//################################
			
			if(m_isTalking) 
			{
				if(m_master.isWaitingFeedback()) 
				{
					m_canNext=true; 
				}
			}
			else
			{
				m_partDelay = 10;
				m_instrPart = 4;   
				m_isTalking = true; 
				m_canNext = false;
				m_playScreen.AnimateEntry(false);
				m_master.Say(new String[] {"Tapping a tile will", "merge all tiles with", "the same color."}); 
				m_waitForInput = new Vector<Rect>();
				m_waitForInput.add(m_playScreen.getTileBounds(1,1));
				m_inputRequired = 1;
				m_inputsCtr=0;
				
			}
		}
		else if(m_instrPart==4) 
		{ 

			//################################
			//## PART 4 - HAS WAIT FOR INPUT 
			//################################
		}
		else if(m_instrPart==5) 
		{  
			if(m_isTalking) 
			{
				if(m_master.isWaitingFeedback()) 
				{
					m_canNext=true; 
				}
			}
			else
			{ 
				m_partDelay = 0;
				m_instrPart = 6;
				m_isTalking = true;
				m_canNext = false;
				m_master.Say(new String[] {"The goal is to", "MERGE all group of", "tiles into one."}); 
				
			}
		}
		
		else if(m_instrPart==6) 
		{  
			if(m_isTalking) 
			{
				if(m_master.isWaitingFeedback()) 
				{
					m_canNext=true; 
				}
			}
			else
			{ 
				m_partDelay = 0;
				m_instrPart = 7;
				m_isTalking = true;
				m_canNext = false;
				m_master.Say(new String[] {"Now, try merging", "it to one group."}); 
				m_waitForInput = new Vector<Rect>();
				m_waitForInput.add(m_playScreen.getTileBounds(1,1));
				m_inputRequired=2;
				m_inputsCtr = 0;
				
			}
		}
		else if(m_instrPart==7) 
		{ 
			
		}
		else if(m_instrPart==8) 
		{ 
			if(m_isTalking) 
			{
				if(m_master.isWaitingFeedback()) 
				{
					m_canNext=true; 
				}
			}
			else
			{ 
				m_partDelay = 0;
				m_instrPart = 9;
				m_isTalking = true;
				m_canNext = false;
				m_master.Say(new String[] {"Try disconnecting", "the group of tiles."}); 

				m_playScreen = new PlayScreen(LevelUtil.ParseSerial(
				"<3,3,"
				+ "555:0,111:0,111:0,"
				+ "111:2,111:1,111:2,"
				+ "555:0,111:0,555:0>"
				));
				m_playScreen.AnimateEntry(false);

				m_waitForInput = new Vector<Rect>();
				m_waitForInput.add(m_playScreen.getTileBounds(1,0));
				m_inputRequired=1;
				m_inputsCtr = 0;
				
			}
		}
		else if(m_instrPart==10) 
		{ 
			if(m_isTalking) 
			{
				if(m_master.isWaitingFeedback()) 
				{
					m_canNext=true; 
				}
			}
			else
			{ 
				m_partDelay = 0;
				m_instrPart = 11;
				m_isTalking = true;
				m_canNext = false;
				m_master.Say(new String[] {"Try merging all", "group of tiles."}); 

				m_playScreen = new PlayScreen(LevelUtil.ParseSerial(
				"<3,3,"
				+ "111:1,111:1,111:2,"
				+ "222:2,222:2,222:1,"
				+ "111:2,111:2,111:2>"
				));
				m_playScreen.AnimateEntry(false);
				m_waitForInput = new Vector<Rect>();
				m_waitForInput.add(m_playScreen.getTileBounds(0,1));
				m_waitForInput.add(m_playScreen.getTileBounds(1,1));
				m_waitForInput.add(m_playScreen.getTileBounds(2,1));
				m_inputRequired=6;
				m_inputsCtr = 0;
				
			}
		}
		else if(m_instrPart==12) 
		{ 

			if(m_isTalking) 
			{
				if(m_master.isWaitingFeedback()) 
				{
					m_canNext=true; 
				}
			}
			else
			{ 
				m_partDelay = 0;
				m_instrPart = 13;
				m_isTalking = true;
				m_canNext = false;
				m_master.Say(new String[] {"I will let you on", "your own for now."}); 
				m_waitForInput = new Vector<Rect>();  
				
			}
		}
		else if(m_instrPart==13) 
		{ 

			if(m_isTalking) 
			{
				if(m_master.isWaitingFeedback()) 
				{
					m_canNext=true; 
				}
			}
			else
			{ 
				m_partDelay = 0;
				m_instrPart = 14;
				m_isTalking = false;
				m_canNext = false;
				m_master.exit(); 
				
			}
		}
		else if(m_instrPart==14) 
		{ 
			if(m_master.isGone()) 
			{
				genSet.game.SetSharedPrefBoolean(SharedPref.INSTRUCTION_BASIC_DONE, true);
				 ExitInstruction();   
			}
		}
		 
	}
	
	private void ExitInstruction() 
	{
		m_state = state.FADE_OUT;
		m_anim = new float[4];
	}
	
	private boolean isRequiredTouched(TouchEvent g) 
	{
		for(int idx=0;idx<m_waitForInput.size();idx++) 
		{
			if(inBounds(g, m_waitForInput.get(idx))) 
			{
				return true;
			}
		}
		return false;
	}

	protected void IdleTouchEvents(TouchEvent g) 
	{ 
		MasterTouchUpdates(g);

		if(m_waitForInput.size()!=0&&isRequiredTouched(g)) 
		{
			m_playScreen.TouchUpdates(g); 
		}
		
		if(g.type== TouchEvent.TOUCH_UP) 
		{
			if(m_waitForInput.size()!=0&&isRequiredTouched(g)) 
			{
				m_inputsCtr++;
				
				if(m_inputsCtr>=m_inputRequired)
				{
					if(m_instrPart==4) 
					{
						m_partDelay = 0;
						m_instrPart=5;
						m_isTalking = true; 
						m_canNext = false;
						m_waitForInput= new Vector<Rect>();
						m_master.Say(new String[] {"Great!                               ", "Adjacent tiles will", "be reduced by 1."}  ); 
					}
					else if(m_instrPart==7) 
					{

						m_partDelay = 10;
						m_instrPart=8;
						m_isTalking = true; 
						m_canNext = false;
						m_waitForInput= new Vector<Rect>();
						m_master.Say(new String[] {"Good.               ", "Dont let a tile", "disconnect from", "its group."} );
 
					}
					else if(m_instrPart==9) 
					{

						m_partDelay = 0;
						m_instrPart=10;
						m_isTalking = true; 
						m_canNext = false;
						m_waitForInput= new Vector<Rect>();
						m_master.Say(new String[] {"See?                                     ", "Game ends when" ,"you let a single tile", "disconnected!"}); 
 
					}
					else if(m_instrPart==11) 
					{
						m_partDelay = 0;
						m_instrPart=12;
						m_isTalking = true; 
						m_canNext = false;
						m_waitForInput= new Vector<Rect>();
						m_master.Say(new String[] {"Hmmmm....                                                                                     ", "I hope you're ready."}); 
 					}
				 
				}
			}
		}
		
		
	}
	
	protected void MasterTouchUpdates(TouchEvent g) 
	{
		if(g.type == TouchEvent.TOUCH_DOWN )
		{ 
			if(m_canNext) 
			{
				drawRoundRect(g,  NEXT_BTN_POS_X, NEXT_BTN_POS_Y, Constants.INSTR_NEXT,Color.BLACK,Color.BLACK,NEXT_BTN);
			}
 
			if(m_waitForInput.size()==0)
			{
				if(m_isTalking)
				{
					if(m_master.isWaitingFeedback()) 
					{ 
						m_master.feedBack();
						m_isTalking=false;
					}
					else if(m_master.isTalking()) 
					{
						m_master.interupt();
						m_canNext = true;
					} 
				}
			}
		} 
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case NEXT_BTN:  NextButtonPressed();break; 
			}
			buttonPressed =0;
		 } 
	}
	
	private void NextButtonPressed() 
	{
		if(m_master.isWaitingFeedback()) 
		{
			m_master.feedBack(); 
			m_isTalking = false; 
		}  
		
	}
	
	@Override
	protected void TouchUpdates(TouchEvent g) {
		switch(m_state)
		{
		case IDLE: IdleTouchEvents(g); break;
		case FADE_OUT: break;
		case PLACEHOLDER_3: break;
		case PLACEHOLDER_4: break;
		case PLACEHOLDER_5: break;
		case PLACEHOLDER_6: break;
		}
		
	}

	@Override
	protected void backButton() {
		switch(m_state)
		{
		case IDLE: 
			m_instrPart = 0;break;
		case FADE_OUT: break;
		case PLACEHOLDER_3: break;
		case PLACEHOLDER_4: break;
		case PLACEHOLDER_5: break;
		case PLACEHOLDER_6: break;
		}
		
	}

	
}
