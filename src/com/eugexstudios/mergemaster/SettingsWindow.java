package com.eugexstudios.mergemaster;

import android.graphics.Color;
import android.graphics.Paint.Align;

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;  
import com.eugexstudios.mergemaster.AskUserWindow.QUESTION;
import com.eugexstudios.utils.AssetManager;
import com.eugexstudios.utils.CONFIG;
import com.eugexstudios.utils.ColorManager;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Screen;
import com.eugexstudios.utils.SharedPref;
import com.eugexstudios.utils.genSet;

public class SettingsWindow extends Screen{

	private final byte SOUND_BTN =1;
	private final byte MUSIC_BTN =2; 
	private final byte ABOUT_BTN  =3;
	private final byte RESET_DATA_BTN  =4;
	private final byte GOTO_PUBLISHER_PAGE  =5; 
	private final byte CLOSE_ABOUT_BTN  =6;
	private final byte SHARE_APP_BTN  =7;
	private final byte CLOSE_BTN  =8; 
	

	
	
	private enum state {
			IDLE
			,ASK_RESET
			,ABOUT
			,PLACEHOLDER_3
			,PLACEHOLDER_4
			,PLACEHOLDER_5
			,PLACEHOLDER_6
			}; 

	private state m_state;
	private AskUserWindow m_askWindow;

	private boolean m_soundOn;
	private boolean m_musicOn;

	private static String VERSION_NAME;
	public SettingsWindow()
	{
		m_state = state.IDLE;
		m_soundOn = genSet.game.GetSharedPrefBoolean(SharedPref.SOUND_ON);
		m_musicOn = genSet.game.GetSharedPrefBoolean(SharedPref.MUSIC_ON);
		VERSION_NAME = genSet.game.GetVersionName();   
				
	}
	@Override
	public void Paint(Graphics g) {

		g.setTypeface(AssetManager.Comforta,genSet.paint);
		switch(m_state)
		{
			case IDLE:  PaintIdle(g); break;
			case ASK_RESET: PaintAskToReset(g); break;
			case ABOUT: PaintAboutWindow(g);break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		} 
	}
	
	private void PaintAskToReset(Graphics g)
	{
		PaintIdle(g);
		m_askWindow.Paint(g);
	}

	private final static int WINDOW_POS_X = Constants.ScaleX(40);
	private final static int WINDOW_POS_Y = Constants.ScaleY(300);
	private final static int ADJUST_POS_Y = (int)(Constants.WINDOW_BODY_1.height*CONFIG.Y_SCALE); 
	private final static int WINDOW_HEIGHT = 6;

	private final static int CLOSE_BTN_POS_X = Constants.ScaleX(633); 
	private final static int CLOSE_BTN_POS_Y = WINDOW_POS_Y+Constants.ScaleY(-5); 
	private final static int BUTTON_COLOR         = Color.rgb(242,61,61); 
	private void PaintIdle(Graphics g)
	{
		g.drawRect(Constants.SCREEN_RECT, Color.argb(150, 0, 0, 0));
//		g.drawRect(Constants.SCREEN_RECT, Color.argb(150, 0, 0, 0));
//		g.drawRoundRect(Constants.WINDOW_D, 20, 20, ColorManager.WINDOWBODY_COLOR);
//		g.drawRoundRect(Constants.WINDOW_HEADER_D, 20, 20, ColorManager.WINDOWHEADER_COLOR);
//		g.drawRect(Constants.WINDOW_HEADER_D.left,Constants.WINDOW_HEADER_D.bottom-20,Constants.WINDOW_HEADER_D.width(),20,ColorManager.WINDOWHEADER_COLOR2);
		 
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, WINDOW_POS_Y, Constants.WINDOW_HEAD_1b);
		for(int idx=0;idx<WINDOW_HEIGHT;idx++)
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, WINDOW_POS_Y+ idx*Constants.WINDOW_BODY_1.height           +Constants.WINDOW_HEAD_1b.height, Constants.WINDOW_BODY_1);
	 	g.drawImage(AssetManager.sprites, WINDOW_POS_X, WINDOW_POS_Y+ WINDOW_HEIGHT*Constants.WINDOW_BODY_1.height +Constants.WINDOW_HEAD_1b.height, Constants.WINDOW_BOTTOM_1);

		int l_textSize = 50;
		genSet.setTextProperties(l_textSize, Color.WHITE, Align.CENTER);
	    g.drawString("SETTINGS", CONFIG.SCREEN_MID, WINDOW_POS_Y+((int)Constants.WINDOW_HEAD_1b.height/2)+l_textSize/2, genSet.paint);  
 
	    
		genSet.setTextProperties(40, Color.WHITE, Align.CENTER);
	    drawRoundRect(g, Constants.SOUND_BTN, m_soundOn?BUTTON_COLOR:Color.LTGRAY, Color.DKGRAY, SOUND_BTN);   
	    g.drawString("SOUND", Constants.SOUND_BTN, genSet.paint); 

	    drawRoundRect(g, Constants.MUSIC_BTN, m_musicOn?BUTTON_COLOR:Color.LTGRAY, Color.DKGRAY, MUSIC_BTN);    
	    g.drawString("MUSIC", Constants.MUSIC_BTN, genSet.paint);  

	    drawRoundRect(g, Constants.ABOUT_BTN, BUTTON_COLOR, Color.DKGRAY, ABOUT_BTN);    
	    g.drawString("ABOUT", Constants.ABOUT_BTN, genSet.paint); 

	    drawRoundRect(g, Constants.RESET_DATA_BTN, BUTTON_COLOR, Color.DKGRAY, RESET_DATA_BTN);    
	    g.drawString("RESET DATA", Constants.RESET_DATA_BTN, genSet.paint); 
	    

//		drawRoundRect(g, CLOSE_BTN_POS_X, CLOSE_BTN_POS_Y, Constants.CLOSE_WINDOW_BTN, Color.BLACK, Color.BLACK, CLOSE_BTN);
		g.drawImage(AssetManager.sprites, CLOSE_BTN_POS_X + (buttonPressed==CLOSE_BTN?-3:0), CLOSE_BTN_POS_Y + (buttonPressed==CLOSE_BTN?4:0) , Constants.CLOSE_WINDOW_BTN);
 
	}
	private void PaintAboutWindow(Graphics g)
	{ 
		g.drawRect(Constants.SCREEN_RECT, Color.argb(150, 0, 0, 0)); 
		g.drawRoundRect(Constants.ABOUT_WINDOW, 20, 20, Color.WHITE);
		g.drawRoundRect(Constants.ABOUT_WINDOW_HEADER, 20, 20,BUTTON_COLOR);
		g.drawRect(Constants.ABOUT_WINDOW_HEADER.left,Constants.ABOUT_WINDOW_HEADER.bottom-20,Constants.ABOUT_WINDOW_HEADER.width(),20,  ColorManager.WINDOWHEADER_COLOR2);

		genSet.setTextProperties(55, Color.WHITE, Align.CENTER); 
	    g.drawString("About", Constants.ABOUT_WINDOW_HEADER, genSet.paint); 
	  
		genSet.setTextProperties(50, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Merge Master", CONFIG.SCREEN_MID, Constants.ScaleY(400), AssetManager.Comforta,genSet.paint);
		 
		genSet.setTextProperties(30, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Version "+VERSION_NAME , CONFIG.SCREEN_MID,Constants.ScaleY(451), AssetManager.Comforta,genSet.paint);
		
		genSet.setTextProperties(30, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Created by", CONFIG.SCREEN_MID, Constants.ScaleY(560), AssetManager.Comforta,genSet.paint);
		genSet.setTextProperties(45, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Eugex Studios", CONFIG.SCREEN_MID, Constants.ScaleY(615), AssetManager.Comforta,genSet.paint);
	 	
		genSet.setTextProperties(28, Color.DKGRAY, Align.CENTER); 
		g.drawString( "Play more from Eugex Studios", CONFIG.SCREEN_MID, Constants.ScaleY(700),genSet.paint); 
		drawRoundRect(g, Constants.PUBLISHER_PAGE_BTN,BUTTON_COLOR,ColorManager.WINDOWHEADER_COLOR2, GOTO_PUBLISHER_PAGE);   
		genSet.setTextProperties(35, Color.WHITE, Align.CENTER); 
		g.drawString("EXPLORE GAMES",  Constants.PUBLISHER_PAGE_BTN, genSet.paint);

		genSet.setTextProperties(28, Color.DKGRAY, Align.CENTER); 
		g.drawStringFont("Share this game to your friends!", CONFIG.SCREEN_MID, Constants.ScaleY(860), AssetManager.Comforta,genSet.paint); 
		drawRoundRect(g, Constants.SHARE_APP_BTN, BUTTON_COLOR,ColorManager.WINDOWHEADER_COLOR2, SHARE_APP_BTN);  
		genSet.setTextProperties(35, Color.WHITE, Align.CENTER);  
		g.drawString("SHARE NOW", Constants.SHARE_APP_BTN, genSet.paint); 

		drawRoundRect(g, Constants.CLOSE_ABOUT_BTN, Color.argb(100, 20, 20, 20), Color.argb(150, 20, 20, 20), CLOSE_ABOUT_BTN);    
		genSet.setTextProperties(35, Color.WHITE, Align.CENTER);   
		g.drawString("X",  Constants.CLOSE_ABOUT_BTN, genSet.paint); 
		  
	}
	@Override
	public void Updates(float p_deltaTime) {
		switch(m_state)
		{
			case IDLE:   break;
			case ASK_RESET:  
				if(m_askWindow.isClosed())
				{
					m_state = state.IDLE;
				}
				break;
			case ABOUT: break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		}
		
	}

	@Override
	public void TouchUpdates(TouchEvent g) {
		switch(m_state)
		{
			case IDLE:   IdleTouchUpdates(g);break;
			case ASK_RESET:  m_askWindow.TouchUpdates(g);break;
			case ABOUT:AboutTouchUpdates(g); break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		}
		
	}
	public void AboutTouchUpdates(TouchEvent g) {
        
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			drawRoundRect(g, Constants.CLOSE_ABOUT_BTN,    Color.argb(100, 20, 20, 20),Color.LTGRAY, CLOSE_ABOUT_BTN);    
			drawRoundRect(g, Constants.SHARE_APP_BTN,      Color.DKGRAY, Color.LTGRAY, SHARE_APP_BTN);  
			drawRoundRect(g, Constants.PUBLISHER_PAGE_BTN, Color.DKGRAY, Color.LTGRAY, GOTO_PUBLISHER_PAGE);   

			if(buttonPressed!=0)
			{
				AssetManager.btn_1.play();
			}
		}
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case CLOSE_ABOUT_BTN:     m_state = state.IDLE;            break; 
				case SHARE_APP_BTN:       genSet.game.ShareApp();          break;  
				case GOTO_PUBLISHER_PAGE: genSet.game.GoToPublisherPage(); break;   
			}
			if(buttonPressed!=0) {
			
			
				buttonPressed=0;
				
			}
		} 
		
	}
	public void IdleTouchUpdates(TouchEvent g) {
        
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
		    drawRoundRect(g, Constants.SOUND_BTN, Color.RED, Color.DKGRAY, SOUND_BTN);   
		    drawRoundRect(g, Constants.MUSIC_BTN, Color.RED, Color.DKGRAY, MUSIC_BTN);      
		    drawRoundRect(g, Constants.ABOUT_BTN, BUTTON_COLOR, Color.DKGRAY, ABOUT_BTN);    
		    drawRoundRect(g, Constants.RESET_DATA_BTN, BUTTON_COLOR, Color.DKGRAY, RESET_DATA_BTN);    
			drawRoundRect(g, CLOSE_BTN_POS_X, CLOSE_BTN_POS_Y, Constants.CLOSE_WINDOW_BTN, Color.BLACK, Color.BLACK, CLOSE_BTN);

			if(buttonPressed!=0)
			{
				AssetManager.btn_1.play();
			}
		    
		}
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case SOUND_BTN:
					m_soundOn = !m_soundOn;
					GameScreen.ToggleSoundSettings(); break;  
				case MUSIC_BTN: 
					m_musicOn = !m_musicOn;
					GameScreen.ToggleMusicSettings(); break;  
				case ABOUT_BTN: GoToAbout(); break;  
				case RESET_DATA_BTN: AskToReset();  break;  
				case CLOSE_BTN:  GameScreen.GoToMenu(false);break;
			}
			 
			if(buttonPressed!=0)
				buttonPressed=0;
		} 
	}
	
	protected void GoToAbout() {
		m_state = state.ABOUT;
		
	}
	protected void AskToReset() {

		m_state =state.ASK_RESET;
		m_askWindow = new AskUserWindow(QUESTION.ASK_RESET); 
		AssetManager.ask_reset_popup.play();
	}
	@Override
	public void backButton() {
		switch(m_state)
		{
			case IDLE:  GameScreen.GoToMenu(false); break;
			case ASK_RESET: m_state = state.IDLE; break;
			case ABOUT: m_state = state.IDLE; break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		}
		
	}

}
