package com.eugexstudios.mergemaster.creator;

import android.graphics.Point;
 


import com.eugexstudios.mergemaster.Level;
import com.eugexstudios.mergemaster.Tile;

public class LevelUtil {
	 

	  final static char START_TAG = '<';
	  final static char END_TAG = '>';
		public static Level ParseSerial(String p_serial)
		{
			return ParseSerial(1000,1000,p_serial);
		}
		public static Level ParseSerial(int p_min3,String p_serial)
		{
			return ParseSerial(p_min3,p_min3+3,p_serial);
		}
	public static Level ParseSerial(int p_min3, int p_min2, String p_serial)
	{
		p_serial = p_serial.replace(START_TAG+"", "");
		p_serial = p_serial.replace(END_TAG+"", "");
		
		String[] l_serial = p_serial.split(",");
		int l_row =  Integer.parseInt(l_serial[0]);
		int l_col =  Integer.parseInt(l_serial[1]); 
		int l_serialCtr =2;

		int l_val[][]   = new int[l_row][l_col];
		int l_tileType[][] = new int[l_row][l_col]; 
		TileGroupDetail l_detail =  new TileGroupDetail(l_row,l_col); 
		
		for(int idxY=0;idxY<l_row;idxY++)
		{ 
			for(int idxX=0;idxX<l_col;idxX++)
			{  
				String[] tileDetailStr = l_serial[l_serialCtr].split(":");  
				l_tileType[idxY][idxX]   = Integer.parseInt(tileDetailStr[0]);
				l_val[idxY][idxX] = Integer.parseInt(tileDetailStr[1]);
				l_serialCtr++; 
			}
		} 
		
		Tile[][] l_tiles = InitTile(l_detail, l_val,l_tileType); 
		return new Level(l_tiles, l_col, p_min3, p_min2); 
	}

	public static Tile[][] cloneTiles(Tile[][] p_tiles)
	{
		Tile[][] l_tiles = new Tile[p_tiles.length][p_tiles[0].length];
		for(int idx=0;idx<p_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<p_tiles[0].length;idx2++)
			{
				l_tiles[idx][idx2] = new Tile(p_tiles[idx][idx2]);
			}
		}
		return l_tiles;
	}
	 
	public static Tile[][] InitTile(TileGroupDetail p_tileGrpDet, int [][] p_val,int [][] p_color)
	{
		TileGroupDetail det = p_tileGrpDet;
		Tile[][] l_tiles = new Tile[det.row][det.col];

		for(int idxY=0;idxY<det.row;idxY++)
		{
			for(int idxX=0;idxX<det.col;idxX++)
			{
				l_tiles[idxY][idxX]  
						= new Tile(
								  new Point( 
										  (int)(det.marginX+((det.tileWidth+det.gapX)*idxX))
										  ,(int)(det.marginY+((det.tileHeight+det.gapY)*idxY)))
								 ,p_color[idxY][idxX]
								 ,p_val[idxY][idxX]
						   	     ,det.tileWidth
						         ,det.tileHeight
						         ,det.col
								);
			}
		}
		return l_tiles;
	}
	
	private static int CalculateMinMove()
	{
		int l_minMove = 0;
		
		
		 
		
		return l_minMove;
	}
 
}
