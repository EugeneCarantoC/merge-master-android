package com.eugexstudios.mergemaster.creator;

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.ColorManager;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.genSet;

import android.graphics.Color;
import android.graphics.Point;

public class CreatorTile {
  
	private boolean m_isPressed;
	
	private Point m_screenPos; 
	private int   m_val; 
	private int   m_tileType;
	private int   m_width;
	private int   m_height; 
 
     
	  
	public CreatorTile(Point p_screenPos, int p_tileType, int p_val, int p_width, int p_height)
	{  
		m_screenPos = new Point(p_screenPos); 
		m_tileType     = p_tileType;
		m_val	    = p_val;
		m_width	    = p_width;
		m_height    = p_height;  
	} 	
	
	public void paint_tile(Graphics g)
	{  
		drawRoundRect(
				   g
				   , m_screenPos.x 
				   , m_screenPos.y 
				   , m_width
				   , m_height
				   , m_tileType, Color.GRAY
		           );  
		 
	} 
	 public void paint_value(Graphics g)
	{  
		if(m_val==0) return;
		genSet.setTextProperties(40, Color.BLACK);
		g.drawString(m_val+"", m_screenPos.x+m_width/2, m_screenPos.y + m_height,  genSet.paint);
	} 
	public void paint_color_idx(Graphics g, int p_idx)
	{  
		if(m_val==0) return;
		genSet.setTextProperties(30, Color.GRAY);
		g.drawString(p_idx+"", m_screenPos.x+m_width/2, m_screenPos.y + m_height/2,  genSet.paint);
	} 
	public void addValue(int p_val)
	{
		m_val+=p_val;
		m_isPressed=false;
	} 
	public void Updates(float p_deltaTime)
	{
		 
	} 
	
	public boolean isPressed(TouchEvent g)
	{ 
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
		  return drawRoundRect(
				   g
				   , m_screenPos.x 
				   , m_screenPos.y 
				   , m_width
				   , m_height
				   , m_tileType, m_tileType
		        );
		}
		
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			m_isPressed = false;
		}
		return false;
	}
	  
	public void drawRoundRect(Graphics g, int p_x, int p_y, int p_len, int p_wid, int p_color,  int p_color2 )
	{   
		g.drawRoundRect( p_x ,p_y+p_wid/2, p_len, p_wid, 20, 20,m_isPressed?p_color2:p_color);   
 	 }  
	
	public boolean drawRoundRect(TouchEvent g, int p_x, int p_y, int p_len, int p_wid, int p_color,  int p_color2)
	{    
		if(inBounds(g, p_x ,p_y+p_wid/2, p_len, p_wid))
			m_isPressed=true;
		else
			m_isPressed=false;
		
		return m_isPressed;
	} 
	public boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
		 
		if (event.x > x && event.x < x + width - 1 && event.y > y && event.y < y + height - 1)
			return true; 
		else
			return false;
	} 
	
	public int getColor()
	{
		return m_tileType;
	}
	public void setColor(int p_color,boolean p_resetVal)
	{
		m_tileType = p_color;
		if(p_resetVal)
			m_val = 0;
	}
	public Point getScreenPos()
	{
		return m_screenPos;
	}
	public String serialize()
	{
		
		String l_serial = getType()+":" + m_val; 
		return l_serial;
	}
	private int getType() 
	{
		final int Color1 = Color.rgb(212, 56, 56);
		final int Color2 = Color.rgb(225, 106, 48);
		final int Color3 = Color.rgb(255, 199, 47);
		final int Color4 =  Color.rgb(117, 2019, 71);
		final int Color5 =  Color.rgb(50, 174, 65);

		     if(m_tileType==Color1) return 111;
		else if(m_tileType==Color2) return 222;
		else if(m_tileType==Color3) return 333;  
		else if(m_tileType==Color4) return 444;  
		else if(m_tileType==Color5) return 555;  
		else return 0;
	}
	
}
