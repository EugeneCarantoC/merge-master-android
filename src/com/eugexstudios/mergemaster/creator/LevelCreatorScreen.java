package com.eugexstudios.mergemaster.creator;
 
import java.util.Vector;

import android.graphics.Color;
import android.graphics.Point;
 

import android.graphics.Paint.Align;

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.mergemaster.GameScreen;
import com.eugexstudios.mergemaster.PlayScreen;
import com.eugexstudios.mergemaster.GameScreen.AppState;
import com.eugexstudios.utils.ColorManager;
import com.eugexstudios.utils.Saved;
import com.eugexstudios.utils.Screen;
import com.eugexstudios.utils.genSet;

public class LevelCreatorScreen extends Screen{


	private int ROW_LIMIT = 6;
	private int ROW_MIN = 3;
	private int COL_LIMIT = 8;
	private int COL_MIN = 3;
	
	
	enum state {
		    COLOR_SET
	      , VALUE_SET
	      , ON_TEST_PLAY
	      , PLACEHOLDER_3
	      , PLACEHOLDER_4
	      , PLACEHOLDER_5
	      , PLACEHOLDER_6
	      , PLACEHOLDER_7
	      };
	private state m_state;
	private CreatorTile m_tiles[][]; 
	private Vector<int[]> m_colorAvailable;
	int [][] m_tileColor; 

	final int COLOR_SEL_BTN =100;
	final int ADDCOLOR_BTN =2;
	final int SET_VALUE_TOGGLE =3;
	final int X_DIM_BTN =4;
	final int Y_DIM_BTN =5;
	final int PRINT_BTN =6;
	final int SEND_BTN  =9;
	final int TEST_PLAY_BTN =7;
	private final int COLOR_SEL_BTNSIZE =75;
	private final int COLOR_SEL_BTNSGAP =8;
	
	private int m_row, m_col;
	
	public LevelCreatorScreen(int p_row, int p_col)
	{
		
		initialize(p_row, p_col); 
		
//			Saved.SaveSerialData(genSet.getFileIO(), SerializeCreated(), false);
		
	}
	
	public void initialize(int p_row, int p_col)
	{
		m_row = p_row;
		m_col = p_col;
		TileGroupDetail l_tileGrpDet = new TileGroupDetail(p_row,p_col);
		m_state = state.COLOR_SET;
		m_tiles = new CreatorTile[p_row][p_col];
		 
		m_tileColor = new int[p_row][p_col];
		
		for(int idxY=0;idxY<p_row;idxY++)
		{ 
			for(int idxX=0;idxX<p_col;idxX++)
			{ 
				m_tiles[idxY][idxX] 
						= new CreatorTile(
						new Point((int)(l_tileGrpDet.marginX+((l_tileGrpDet.tileWidth+l_tileGrpDet.gapX)*idxX)),  (int)(l_tileGrpDet.marginY+((l_tileGrpDet.tileWidth+l_tileGrpDet.gapY)*idxY)))
						   ,ColorManager.COLORS[0]
						   ,1
						   ,l_tileGrpDet.tileWidth
						   ,l_tileGrpDet.tileHeight
						 ); 
			} 
		}  
		m_colorAvailable = new Vector<int[]>();  
		m_colorAvailable.add(getAvailColor()); 
	}
	
	@Override
	public void Paint(Graphics g) 
	{ 
		switch(m_state)
		{
			case VALUE_SET: 
			case COLOR_SET:
				PaintPlay(g);
				PaintBtns(g);
			break;
			case ON_TEST_PLAY:m_playScreen.Paint(g); break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_7: break;
		}
		
	
	}
	private void PaintBtns(Graphics g)
	{
		paintColorSelector(g);
		  
		genSet.setTextProperties(33, Color.WHITE, Align.CENTER);

		drawRoundRect(g, 20, 1100, 220, 100, Color.DKGRAY, Color.GRAY, SET_VALUE_TOGGLE); 
		g.drawString(m_state==state.VALUE_SET? "Set color": "Set value", 135,1160, genSet.paint); 
		
		drawRoundRect(g, 255, 1100, 220, 100, Color.DKGRAY, Color.GRAY, TEST_PLAY_BTN);    
		g.drawString("Play", 255+110,1160, genSet.paint);
		 
		drawRoundRect(g, 490, 1100, 220, 100, Color.DKGRAY, Color.GRAY, PRINT_BTN );    
		g.drawString("Print", 490+110,1160, genSet.paint);
		 
		genSet.setTextProperties(40, Color.WHITE); 
		drawRoundRect(g,        535, 100, 80, 80, Color.DKGRAY, Color.GRAY, X_DIM_BTN);  
		g.drawString(m_row +"", 545+30,150, genSet.paint);
		
		drawRoundRect(g, 535+90, 100, 80, 80, Color.DKGRAY, Color.GRAY, Y_DIM_BTN);  
		g.drawString(m_col +"", 545+100+20,150, genSet.paint);

		drawRoundRect(g, 20, 990, 220, 100, Color.DKGRAY, Color.GRAY, SEND_BTN); 
		g.drawString( "Send", 130, 1050, genSet.paint); 
	}
	
	private void paintColorSelector(Graphics g)
	{ 
		for(int idx=0;idx<m_colorAvailable.size();idx++)
		{ 
			drawRoundRect(
					   g
					   , 20+(COLOR_SEL_BTNSIZE+COLOR_SEL_BTNSGAP)*idx 
					   , 0
					   , COLOR_SEL_BTNSIZE
					   , COLOR_SEL_BTNSIZE
					   , ColorManager.COLORS[m_colorAvailable.get(idx)[0]]
					   , ColorManager.COLORS[m_colorAvailable.get(idx)[0]]
					   , COLOR_SEL_BTN+idx
			           );  
		} 
		
		drawRoundRect(
				   g
				   , 20+(COLOR_SEL_BTNSIZE+COLOR_SEL_BTNSGAP)*m_colorAvailable.size()
				   , 0
				   , COLOR_SEL_BTNSIZE
				   , COLOR_SEL_BTNSIZE+30
				   , Color.GRAY
				   , Color.LTGRAY
				   , ADDCOLOR_BTN 
		           );   
	 }
	
	public void PaintPlay(Graphics g)
	{ 
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{
				m_tiles[idx][idx2].paint_tile(g);
				m_tiles[idx][idx2].paint_value(g);
//				m_tiles[idx][idx2].paint_color_idx(g,m_tileColor[idx][idx2]); 
			}
		} 
	}
	
	@Override
	public void Updates(float p_deltaTime) {
		switch(m_state)
		{
			case VALUE_SET:
			case COLOR_SET:  break;  
			case ON_TEST_PLAY: m_playScreen.Updates(p_deltaTime);break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_7: break;
		}
	} 

	@Override
	public void TouchUpdates(TouchEvent g) {
		switch(m_state)
		{
			case COLOR_SET: 
				if(m_colorAvailable.size()>0)
				{ 
					SetColorTouch(g);
				}  
				btnTouchEvents(g); 
				break; 
			case VALUE_SET:  
				AddTileValueTouch(g); 
				btnTouchEvents(g); 
				break;
			case ON_TEST_PLAY: m_playScreen.TouchUpdates(g);break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_7: break;
		} 
		
	}
	
	private PlayScreen m_playScreen;
	
	private void PlayGame(String p_serial) 
	{
		 m_state = state.ON_TEST_PLAY; 
	     m_playScreen = new PlayScreen(LevelUtil.ParseSerial(p_serial));   
	}
	 
	public void btnTouchEvents(TouchEvent g)
	{ 
		if(g.type == TouchEvent.TOUCH_DOWN)
		{ 

			drawRoundRect(g, 20, 990, 220, 100, Color.DKGRAY, Color.GRAY, SEND_BTN); 
			
			drawRoundRect(g, 20, 1100, 220, 100, Color.DKGRAY, Color.GRAY, SET_VALUE_TOGGLE); 
			drawRoundRect(g, 255, 1100, 220, 100, Color.DKGRAY, Color.GRAY, TEST_PLAY_BTN );    
			drawRoundRect(g, 490, 1100, 220, 100, Color.DKGRAY, Color.GRAY, PRINT_BTN); 
			
			drawRoundRect(g, 535, 100, 80, 80, Color.DKGRAY, Color.GRAY, X_DIM_BTN);   
			drawRoundRect(g, 535+90, 100, 80, 80, Color.DKGRAY, Color.GRAY, Y_DIM_BTN);  
			
			
			for(int idx=0;idx<m_colorAvailable.size();idx++)
			{ 
				drawRoundRect(
						   g
						   , 20+(COLOR_SEL_BTNSIZE+COLOR_SEL_BTNSGAP)*idx 
						   , 0
						   , COLOR_SEL_BTNSIZE
						   , COLOR_SEL_BTNSIZE
						   , ColorManager.COLORS[m_colorAvailable.get(idx)[0]]
						   , m_colorAvailable.get(idx)[0]
						   , COLOR_SEL_BTN+idx
				           );  
			}
			
			drawRoundRect(
					   g
					   , 20+(COLOR_SEL_BTNSIZE+COLOR_SEL_BTNSGAP)*m_colorAvailable.size()
					   , 0
					   , COLOR_SEL_BTNSIZE
					   , COLOR_SEL_BTNSIZE+30
					   , Color.GRAY
					   , Color.LTGRAY
					   , ADDCOLOR_BTN 
			           );   
		}

		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case SEND_BTN:      genSet.game.SendLevelSerial(SerializeCreated()); break;
				case TEST_PLAY_BTN: 
					
//					genSet.ShowToast(SerializeCreated());
					PlayGame(SerializeCreated());
					
					break;
				case PRINT_BTN: 	
					try{
					Saved.SaveSerialData(genSet.getFileIO(), SerializeCreated());
					 }catch(Exception asd)
					{
						 genSet.ShowToast("X");
					}
					
					break; 
				case X_DIM_BTN:
					if(m_row<ROW_LIMIT)
					{
						m_row++;
					}
					else
					{
						m_row = ROW_MIN;
					}
					initialize(m_row, m_col);
					break;
				case Y_DIM_BTN: 

					if(m_col<COL_LIMIT)
					{
						m_col++;
					}
					else
					{
						m_col = COL_MIN;
					}
					initialize(m_row, m_col);
					break;
				case ADDCOLOR_BTN:
					m_colorAvailable.add(getAvailColor()); 
					break;
				case SET_VALUE_TOGGLE: 
					m_state = m_state==state.COLOR_SET?state.VALUE_SET:state.COLOR_SET;
					break;
			}
			if(buttonPressed>=COLOR_SEL_BTN)
			{  
				int l_colorAvailIdx = buttonPressed-COLOR_SEL_BTN; 
				boolean has_dup=false;
				do
				{
					if(m_colorAvailable.get(l_colorAvailIdx)[0]+1<ColorManager.COLORS.length)
						m_colorAvailable.get(l_colorAvailIdx)[0]++;
					else 
						m_colorAvailable.get(l_colorAvailIdx)[0]=0;

				    has_dup=false;
					for(int idx=0;idx<m_colorAvailable.size();idx++)
					{
						if(idx!=l_colorAvailIdx)
						{
							if(m_colorAvailable.get(idx)[0]==m_colorAvailable.get(l_colorAvailIdx)[0])
							{
								has_dup=true; 
								break;
							}
						}
					} 
				}while(has_dup);

				refreshTileColors();
			}
			 
			buttonPressed = 0;
		} 
	 }

	private void refreshTileColors()
	{
		for(int idxY=0;idxY<m_tiles.length;idxY++)
		{ 
			for(int idxX=0;idxX<m_tiles[0].length;idxX++)
			{  
				if(m_tiles[idxY][idxX].getColor()!=ColorManager.EMPTY_TILE_COLOR)
					m_tiles[idxY][idxX].setColor(ColorManager.COLORS[m_colorAvailable.get(m_tileColor[idxY][idxX])[0]],false); 
			}		
		}
	}
	
	private int[] getAvailColor()
	{
		boolean has_dup=false;
		int l_colorIdx=0;
		do
		{ 
		    has_dup=false;
			for(int idx=0;idx<m_colorAvailable.size();idx++)
			{ 
				if(m_colorAvailable.get(idx)[0]== l_colorIdx)
				{
					has_dup=true;  
				}  
			}
			if(has_dup)
			{
				l_colorIdx++;
			}
		}while(has_dup); 
		
		return new int[]{l_colorIdx}; 
	}
	private String SerializeCreated()
	{
		String l_serial = LevelUtil.START_TAG+"";
		l_serial+= m_tiles.length +"," + m_tiles[0].length +",";
		
		for(int idxY=0;idxY<m_tiles.length;idxY++)
		{ 
			for(int idxX=0;idxX<m_tiles[0].length;idxX++)
			{
				l_serial += m_tiles[idxY][idxX].serialize() +",";
			}
		} 
		l_serial =l_serial.substring(0,l_serial.length()-1);
		l_serial += LevelUtil.END_TAG;
		return l_serial; 
	}
	
	public void SetColorTouch(TouchEvent g)
	{
		for(int idxY=0;idxY<m_tiles.length;idxY++)
		{ 
			for(int idxX=0;idxX<m_tiles[0].length;idxX++)
			{
				if(m_tiles[idxY][idxX].isPressed(g))
				{ 
					if(m_tiles[idxY][idxX].getColor()!=ColorManager.EMPTY_TILE_COLOR)
					{
						if(m_tileColor[idxY][idxX]+1<m_colorAvailable.size())
						{
							m_tileColor[idxY][idxX]++;
						}
						else
						{
							m_tileColor[idxY][idxX] = 0;
						} 
					}  
					m_tiles[idxY][idxX].setColor(ColorManager.COLORS[m_colorAvailable.get(m_tileColor[idxY][idxX])[0]],true );
					break;
				}
			}
		} 
	} 

	public void AddTileValueTouch(TouchEvent g) {  
		
		for(int idxY=0;idxY<m_tiles.length;idxY++)
		{ 
			for(int idxX=0;idxX<m_tiles[0].length;idxX++)
			{
				if(m_tiles[idxY][idxX].isPressed(g))
				{ 
//					AddTileValueToAdjacent(idxY,idxX);  
					m_tiles[idxY][idxX].addValue(1); 
					break;
				}
			}
		}  
	} 
	  
	
	private void AddTileValueToAdjacent(int p_y, int p_x)
	{
		/*
		 *  A1-A2-A3
		 *  B1-B2-B3
		 *  C1-C2-C3
		 * */
		
		int l_color = m_tiles[p_y][p_x].getColor();
		if(p_y-1>=0)
		{ 
			if(p_x-1>=0)
				if(l_color==m_tiles[p_y-1][p_x-1].getColor())
					m_tiles[p_y-1][p_x-1].addValue(1);  //A1

			if(l_color==m_tiles[p_y-1][p_x].getColor())
				m_tiles[p_y-1][p_x].addValue(1);; //A2 
			
			if(p_x+1<m_tiles[0].length) 
				if(l_color==m_tiles[p_y-1][p_x+1].getColor())
					m_tiles[p_y-1][p_x+1].addValue(1); //A3 
		}

		if(p_x-1>=0)
			if(l_color==m_tiles[p_y][p_x-1].getColor())
				m_tiles[p_y][p_x-1].addValue(1); //B1

			m_tiles[p_y][p_x].addValue(1); //B2 
				
		if(p_x+1<m_tiles[0].length)
			if(l_color==m_tiles[p_y][p_x+1].getColor())
				m_tiles[p_y][p_x+1].addValue(1);//B3
		
		
		if(p_y+1<m_tiles.length)
		{  
			if(p_x-1>=0)
				if(l_color==m_tiles[p_y+1][p_x-1].getColor())
					m_tiles[p_y+1][p_x-1].addValue(1); //C1

			if(l_color==m_tiles[p_y+1][p_x].getColor())
				m_tiles[p_y+1][p_x].addValue(1);  //C2
				
			if(p_x+1<m_tiles[0].length)
				if(l_color==m_tiles[p_y+1][p_x+1].getColor())
					m_tiles[p_y+1][p_x+1].addValue(1); //C3
		}
	} 
	 
	@Override
	public void backButton() {
		switch(m_state)
		{  

			case VALUE_SET: 
			case COLOR_SET: GameScreen.GoToMenu(false);break;
			case ON_TEST_PLAY: m_state = state.COLOR_SET; break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_7: break;
		} 
	} 

}
