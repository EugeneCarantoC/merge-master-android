package com.eugexstudios.mergemaster.creator;

import com.eugexstudios.utils.CONFIG;
import com.eugexstudios.utils.Constants;

public class TileGroupDetail {

	public int row;
	public int col;
	public float marginX;
	public float marginY;
	public int tileWidth;
	public int tileHeight;
	public int gapX, gapY;
	
	public TileGroupDetail(int p_row, int p_col)
	{
		row        = p_row;
		col        = p_col;
		marginX    = Constants.TILES_MARGIN_X;
		marginY    = Constants.TILES_MARGIN_Y;
		tileWidth  = (int)((CONFIG.SCREEN_WIDTH-marginX*2)/row);
		gapX        = (tileWidth)/(row+1);  
		tileWidth  -= gapX; 
		marginX    += gapX/2;
		tileHeight  =tileWidth;
		switch(col) 
		{
		case 3: gapY =gapX+Constants.ScaleY(2);
			    tileHeight +=Constants.ScaleY(23);
		break; 
		case 4: gapY =gapX+Constants.ScaleY(2);
				tileHeight +=Constants.ScaleY(22);
				break; 
		case 5: gapY =gapX+Constants.ScaleY(2);
				tileHeight +=Constants.ScaleY(18);
				marginY-=60;
		break; 
		}
		
		marginY -=gapY;
	}
}
