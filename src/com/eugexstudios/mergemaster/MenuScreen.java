package com.eugexstudios.mergemaster;
 
import android.graphics.Color;
import android.graphics.Rect;  
import com.eugexstudios.adprovider.AdProvider; 
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.mergemaster.AskUserWindow.QUESTION;
import com.eugexstudios.mergemaster.LevelSelectScreen.states;
import com.eugexstudios.mergemaster.levels.LevelManager;
import com.eugexstudios.utils.AssetManager;
import com.eugexstudios.utils.CONFIG;
import com.eugexstudios.utils.ColorManager;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Debugger;
import com.eugexstudios.utils.Screen;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.Constants.game_mode;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;

public class MenuScreen extends Screen{
	
	private final byte 
		    PLAY_NOW_BTN         = 1
		  , SETTINGS_BTN         = 2
	      , SHARE_BTN            = 3
   	      , MOREGAMES_BTN        = 4 
   	      , COLORTWIRL_BTN       = 5
   	      , LINESWINGER_BTN      = 6
   	      , CREATOR_BTN  	     = 11
   	    		
		;

	final Rect MOREGAMES_WINDOW_RECT = new Rect(
			  Constants.ScaleX(0.085f)
			, Constants.ScaleY(0.29f)
			, Constants.ScaleX(0.085f)+ Constants.ScaleX(0.83f)
			, Constants.ScaleY(0.29f)+ Constants.ScaleY(0.59f)
			);

	final static int MOREGAMES_WINDOW_POS_X = Constants.ScaleX(30);
	final static int MOREGAMES_WINDOW_POS_Y = Constants.ScaleY(320);
	final static int MOREGAMES_BTNS_POS_Y = MOREGAMES_WINDOW_POS_Y+Constants.ScaleY(180);
	
    private enum exit_dest {LEVEL_SELECT};
    private enum state{
			 IDLE
			,ASK_EXIT 
			,SETTINGS
			,MOREGAMES 
		  	,FADE_OUT    
		  	,FADE_IN   
	};
	
	private exit_dest                            m_exitDest;
	private state                                m_state;
	private AskUserWindow                        m_askWindow; 
	private SettingsWindow                       m_settingsWindow; 
	private AdProvider                           m_adProvider;   
	private boolean                              m_offerShown;
	private Background                           m_background;
	private final int DIM_COLOR      = Color.argb(200, 0, 0, 0);
	private final int WINDOW_COLOR   = Color.rgb(255, 245, 245);
	private final int SHADOW_COLOR   = Color.argb(30, 0, 0, 0);
	private float[] m_anim;
	
	public MenuScreen(boolean p_fadeIn) 
	{
		m_state      = state.IDLE; 
	    m_adProvider = new AdProvider();    
	    m_adProvider.RequestNativeAd(Constants.NATIVE_AD_MENU);  
	    m_adProvider.RequestOfferWallAd();  
	    m_offerShown = false;  
		m_background = new Background();
//		genSet.game.HideBannerAd(); 
		
		if(p_fadeIn) 
		{
			FadeIn();
		}
	}
	 
	private void AskToExit()
	{ 
		m_state =state.ASK_EXIT;
		m_askWindow = new AskUserWindow(QUESTION.EXIT_GAME); 
		AssetManager.ask_exit_popup.play();
	}
	
	@Override
	public void Paint(Graphics g) { 
		m_background.Paint(g);
		switch(m_state)
		{ 
			case IDLE:         PaintIdle(g);        break;
			case ASK_EXIT:     PaintAskExit(g);     break; 
			case SETTINGS:     PaintSettings(g);    break; 
			case MOREGAMES:    PaintMoreGames(g); 	break;
			case FADE_OUT:     PaintFadeOut(g);		break;
			case FADE_IN: 	   PaintFadeIn(g);		break;
			default: break;
		}

	}
	

	public void PaintFadeOut(Graphics g) 
	{ 
		PaintIdle(g);
		g.drawRect(Constants.SCREEN_RECT, Color.argb((int)(m_anim[0]), ColorManager.GAME_THEME_1_R,  ColorManager.GAME_THEME_1_G,  ColorManager.GAME_THEME_1_B));
	}
	
	public void PaintFadeIn(Graphics g) 
	{ 
		PaintIdle(g);
		g.drawRect(Constants.SCREEN_RECT, Color.argb((int)(m_anim[0]), ColorManager.GAME_THEME_1_R,  ColorManager.GAME_THEME_1_G,  ColorManager.GAME_THEME_1_B));
	}
		

	public void PaintSettings(Graphics g)
	{ 
		PaintIdle(g);
		m_settingsWindow.Paint(g);
	}
	
	public void PaintMoreGames(Graphics g)
	{  
		PaintIdle(g); 
		g.drawRect(Constants.SCREEN_RECT, DIM_COLOR); 
 
		g.drawImage(AssetManager.sprites, MOREGAMES_WINDOW_POS_X, MOREGAMES_WINDOW_POS_Y, Constants.WINDOW_1,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		g.drawImage(AssetManager.sprites, MOREGAMES_WINDOW_POS_X, MOREGAMES_WINDOW_POS_Y, Constants.MORE_GAMES_HEADER, CONFIG.X_SCALE, CONFIG.Y_SCALE); 

//	     drawRoundRect(g, 0,MOREGAMES_BTNS_POS_Y,Constants.COLORTWIRL_BTN, Color.BLACK,Color.BLACK, COLORTWIRL_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		g.drawImage(AssetManager.sprites,  buttonPressed==COLORTWIRL_BTN?4:0, MOREGAMES_BTNS_POS_Y+(buttonPressed==COLORTWIRL_BTN?4:0), Constants.COLORTWIRL_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		
//	     drawRoundRect(g, 0,MOREGAMES_BTNS_POS_Y,Constants.LINESWINGER_BTN, Color.BLACK,Color.BLACK, LINESWINGER_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		g.drawImage(AssetManager.sprites, buttonPressed==LINESWINGER_BTN?4:0, MOREGAMES_BTNS_POS_Y+(buttonPressed==LINESWINGER_BTN?4:0), Constants.LINESWINGER_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		
	}
	
	public void PaintIdle(Graphics g)
	{  
//	     g.drawImage(AssetManager.bg,0,0,0,0 ,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT,CONFIG.X_SCALE,CONFIG.Y_SCALE);
	     g.drawImage(AssetManager.sprites,0,0,Constants.MENU_LOGO,CONFIG.X_SCALE,CONFIG.Y_SCALE);
//	     drawRoundRect(g, Constants.PLAY_BTN, Color.WHITE,Color.WHITE, PLAY_NOW_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
	     g.drawImage(AssetManager.sprites,buttonPressed==PLAY_NOW_BTN?5:0,buttonPressed==PLAY_NOW_BTN?7:0,Constants.PLAY_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE); 


//	     drawRoundRect(g, Constants.MENU_SETTINGS, Color.WHITE,Color.WHITE, SETTINGS_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
	     g.drawImage(AssetManager.sprites, buttonPressed==SETTINGS_BTN?4:0, buttonPressed==SETTINGS_BTN?4:0, Constants.MENU_SETTINGS,CONFIG.X_SCALE,CONFIG.Y_SCALE); 
	     
//	     drawRoundRect(g, Constants.MENU_SHARE, Color.WHITE,Color.WHITE, SHARE_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
	     g.drawImage(AssetManager.sprites,  buttonPressed==SHARE_BTN?4:0,  buttonPressed==SHARE_BTN?4:0, Constants.MENU_SHARE,CONFIG.X_SCALE,CONFIG.Y_SCALE); 
	     
//	     drawRoundRect(g, Constants.MENU_MOREGAMES, Color.WHITE,Color.WHITE, MOREGAMES_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
	     g.drawImage(AssetManager.sprites, buttonPressed==MOREGAMES_BTN?4:0, buttonPressed==MOREGAMES_BTN?4:0, Constants.MENU_MOREGAMES,CONFIG.X_SCALE,CONFIG.Y_SCALE); 
	     
	     if(m_adProvider.isOfferWallReady()&&m_offerShown==false)
		 { // if offerwall is available
//	    	 g.drawCircle(Constants.OFFER_BTN.right-Constants.ScaleX(5),  Constants.OFFER_BTN.top+Constants.ScaleX(5), Constants.ScaleX(18), Color.RED);
		 } 
	}
	
	public void PaintAskExit(Graphics g) { 
		PaintIdle(g);
		m_askWindow.Paint(g); 
		
	} 
 
	@Override
	public void Updates(float p_deltaTime) {
		m_background.Updates(p_deltaTime);
		switch(m_state)
		{ 
			case IDLE: break;
			case ASK_EXIT:
				if(m_askWindow.isClosed())
				{
					m_state = state.IDLE;
				}
				break; 
			case SETTINGS:     m_settingsWindow.Updates(p_deltaTime);break; 
			case MOREGAMES: break;
			case FADE_OUT: FadeOutUpdates(p_deltaTime); break; 
			case FADE_IN:  FadeInUpdates(p_deltaTime); break; 
			default: break;
		} 
	}  

	@Override
	public void TouchUpdates(TouchEvent g) {
		switch(m_state)
		{ 
			case IDLE:         IdleTouchUpdates(g);                 break; 
			case SETTINGS:     m_settingsWindow.TouchUpdates(g);    break; 
			case ASK_EXIT:     m_askWindow.TouchUpdates(g);         break;
			case MOREGAMES:    MoreGamesTouchUpdates(g);            break;
			default: break;
		}
		
	}
	
	private void FadeIn() 
	{ 
		m_anim = new float[3];
		m_anim[0]=255;
		m_anim[1]=0;
		
		m_state = state.FADE_IN;
	}
	
	private void FadeOut(exit_dest l_exitDest) 
	{ 
		m_exitDest =l_exitDest;
		m_anim = new float[3];
		m_anim[0]=0;
		m_anim[1]=0;
		
		m_state = state.FADE_OUT;
	}
	
	private void FadeInUpdates(float p_deltaTime)
	{
		/*
		 * 0- fade opacity for Argb
		 * 1- steps
		 * 2- anim buffer
		 * */
		
		if(m_anim[1]==0) 
		{
			if(m_anim[0]>0) 
			{
				m_anim[0] -= p_deltaTime*10;
				if(m_anim[0]<0)
					m_anim[0] = 0;
			}
			else 
			{
				m_anim[0] = 0;
				m_state = state.IDLE;
			}
		} 
		 
	}

	
	private void FadeOutUpdates(float p_deltaTime)
	{
		/*
		 * 0- fade opacity for Argb
		 * 1- steps
		 * 2- anim buffer
		 * */
		
		if(m_anim[1]==0) 
		{
			if(m_anim[0]<255) 
			{
				m_anim[0] += p_deltaTime*15;
				if(m_anim[0]>255)
					m_anim[0]=255;
			}
			else 
			{
				m_anim[0] = 255;
				m_anim[1] ++;
				GameScreen.InitializeLevelSelect(game_mode.mode_1,GameScreen.GetLevelReached(game_mode.mode_1),true); 
			}
		}
		else if(m_anim[1]==1) 
		{

			m_anim[2] += p_deltaTime*1;
			if(m_anim[2]>=5) // hang time
			{ 
				GameScreen.GoToLevelSelect(); 
				m_anim[1]++;
			}
				
		}
		 
	}
	
    public void MoreGamesTouchUpdates(TouchEvent g) {
        
		if(g.type == TouchEvent.TOUCH_DOWN)
		{ 
		     drawRoundRect(g, 0,MOREGAMES_BTNS_POS_Y,Constants.COLORTWIRL_BTN, Color.BLACK,Color.BLACK, COLORTWIRL_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		     drawRoundRect(g, 0,MOREGAMES_BTNS_POS_Y,Constants.LINESWINGER_BTN, Color.BLACK,Color.BLACK, LINESWINGER_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		}
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case COLORTWIRL_BTN:  genSet.game.GoToColorTwirlLink();   break;  
				case LINESWINGER_BTN: genSet.game.GoToLineSwingerLink();  break;    
			}
			
			if(buttonPressed!=0)
				buttonPressed=0;
		} 
	}

	public void IdleTouchUpdates(TouchEvent g) {
	           
		if(g.type == TouchEvent.TOUCH_DOWN)
		{ 
		     drawRoundRect(g, Constants.PLAY_BTN, Color.WHITE,Color.WHITE, PLAY_NOW_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		     drawRoundRect(g, Constants.MENU_SETTINGS, Color.WHITE,Color.WHITE, SETTINGS_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		     drawRoundRect(g, Constants.MENU_SHARE, Color.WHITE,Color.WHITE, SHARE_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		     drawRoundRect(g, Constants.MENU_MOREGAMES, Color.WHITE,Color.WHITE, MOREGAMES_BTN,CONFIG.X_SCALE,CONFIG.Y_SCALE);

		}
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case PLAY_NOW_BTN: 
					AssetManager.btn_2.play();
					GameScreen.GoToLevelSelect(game_mode.mode_1,GameScreen.GetLevelReached(game_mode.mode_1),false);  
					break;  
			
				case SETTINGS_BTN: 
					AssetManager.btn_1.play();
					GoToSettings();  
					break;   
					
				case SHARE_BTN:
					AssetManager.btn_1.play();
					genSet.game.ShareApp(); 
					break;
					
				case MOREGAMES_BTN: 

					AssetManager.btn_1.play();
					if(Debugger.eneble_creator&&!Debugger.PROD_MODE) 
					{ 
						GameScreen.GoToCreator();
					}
					else 
					{
						m_state = state.MOREGAMES;
					}
					
					break;
			}
			 
		} 
	}
	 
	private void ShowOfferWall()
	{
		if(m_adProvider.isOfferWallReady())
		{
			m_offerShown = true;
			m_adProvider.ShowOfferWall(
					new AdDisplayListener() {
					    @Override
					    public void adHidden(Ad ad) {
					    }
					    @Override
					    public void adDisplayed(Ad ad) {
					    }
					    @Override
					    public void adClicked(Ad ad) {
					    }
					    @Override
					    public void adNotDisplayed(Ad ad) {
					    }
					}
					);
		}
	}
	
	private void GoToSettings()
	{ 
		m_state = state.SETTINGS;
		m_settingsWindow = new SettingsWindow(); 
	}
	
	@Override
	public void backButton() { 
		switch(m_state)
		{ 
			case IDLE:        AskToExit();        break;
			case ASK_EXIT:    m_state = state.IDLE; break; 
			case SETTINGS:    m_settingsWindow.backButton();;break; 
			case MOREGAMES:  m_state = state.IDLE;break;
			default: break;
		}
		
	}

}
