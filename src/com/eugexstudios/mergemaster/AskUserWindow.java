package com.eugexstudios.mergemaster;

import android.graphics.Color; 
import android.graphics.Paint.Align; 
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.AssetManager;
import com.eugexstudios.utils.CONFIG;
import com.eugexstudios.utils.ColorManager;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Screen;
import com.eugexstudios.utils.genSet;

public class AskUserWindow extends Screen{
	
	public enum QUESTION { 
		  	   EXIT_PLAY
			  ,EXIT_GAME 
			  ,GO_TO_MENU
			  ,GO_TO_LEVELSELECT
			  ,ASK_RESET
			  ,RATE_US
			  ,NO_CONNECT
			  ,PURCH_SUCCESS_UNDO
			  ,PURCH_SUCCESS_QMERGE
			  ,PLACEHOLDER_8
		};
		
		private final static int WINDOW_POS_X = Constants.ScaleX(40);
		private final static int WINDOW_POS_Y = Constants.ScaleY(230);
		private final static int ADJUST_POS_Y = (int)(Constants.WINDOW_BODY_1.height*CONFIG.Y_SCALE);
		private final static int RATE_WINDOW_HEIGHT =2;
		
		
    private final byte 
		  YES_BTN         = 1
					, NO_BTN          = 2 
							, OKAY_BTN          = 3
		;
   
	private QUESTION			       m_qType; 
	private boolean					   m_isClose;   

	private final static int BUTTON_COLOR         = Color.rgb(242,61,61); 
	public AskUserWindow(QUESTION p_qType) 
	{ 
		m_qType     = p_qType;
		m_isClose   = false;  
	}
	
	public AskUserWindow(QUESTION p_qType, int p_themeColor) 
	{ 
		m_qType      = p_qType; 
		m_isClose    = false; 
	}
	 
	public boolean isClosed()
	{
		return m_isClose;
	}
	
	@Override
	public void Paint(Graphics g) 
	{ 
		switch(m_qType)
		{  
			case EXIT_PLAY:     PaintExitPlay(g); break;  
			case EXIT_GAME:     PaintExitGame(g);  break;
			case GO_TO_MENU:    PaintGoToMenu(g); break;
			case ASK_RESET:     PaintAskReset(g);break;
			case RATE_US:       PaintAskToRate(g);break;
			case NO_CONNECT:    PaintNoConnection(g); break;
			case PURCH_SUCCESS_UNDO: PaintPurchSuccessUndo(g);break;
			case PURCH_SUCCESS_QMERGE: PaintPurchSuccessQMerge(g);break;
			
			
		default:
			break;
		}
	}

	private void PaintNoConnection(Graphics g)
	{  

		g.drawRect(Constants.SCREEN_RECT, Color.argb(200, 0, 0, 0));
		PaintWindowNotif(g, "NOT AVAILABLE","Try again or check your connection");       
	}
	private void PaintPurchSuccessUndo(Graphics g)
	{   
		g.drawRect(Constants.SCREEN_RECT, Color.argb(200, 0, 0, 0));
		PaintWindowNotif(g, "SUCCESS","3 UNDO now available");       
	}
	private void PaintPurchSuccessQMerge(Graphics g)
	{   
		g.drawRect(Constants.SCREEN_RECT, Color.argb(200, 0, 0, 0));
		PaintWindowNotif(g, "SUCCESS","1 Quick Merge now available");       
	}
	private void PaintGoToMenu(Graphics g)
	{  

		g.drawRect(Constants.SCREEN_RECT, Color.argb(180, 0, 0, 0));
		PaintWindowQuestion(g, "Go to menu","Are you sure want to exit?");       
	}

	private void PaintAskReset(Graphics g)
	{  

		g.drawRect(Constants.SCREEN_RECT, Color.argb(160, 0, 0, 0));
		PaintWindowQuestion(g, "Reset Data","All data will be erased, proceed?");       
	}

	private void PaintExitPlay(Graphics g) 
	{ 
		g.drawRect(Constants.SCREEN_RECT, Color.argb(120, 0, 0, 0));
		PaintWindowQuestion(g, "LEAVE GAME","Are you sure?");        
	}
	
	private void PaintWindowNotif(Graphics g,  String p_title, String p_question) 
	{ 
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, DEFAULT_QUESTION_POS_Y, Constants.WINDOW_HEAD_1b);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, Constants.WINDOW_HEAD_1b.height+ DEFAULT_QUESTION_POS_Y, Constants.WINDOW_BODY_1);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, Constants.WINDOW_BODY_1.height+Constants.WINDOW_HEAD_1b.height+ DEFAULT_QUESTION_POS_Y, Constants.WINDOW_BODY_1);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, Constants.WINDOW_BODY_1.height+Constants.WINDOW_HEAD_1b.height+ DEFAULT_QUESTION_POS_Y+ Constants.WINDOW_BODY_1.height, Constants.WINDOW_BOTTOM_1);

		int l_textSize = 48;
		genSet.setTextProperties(l_textSize, Color.WHITE, Align.CENTER);
	    g.drawString(p_title, CONFIG.SCREEN_MID, DEFAULT_QUESTION_POS_Y+((int)Constants.WINDOW_HEAD_1b.height/2)+l_textSize/2, genSet.paint);  

		l_textSize = 31;
		genSet.setTextProperties(l_textSize, Color.DKGRAY, Align.CENTER);
	    g.drawString(p_question, CONFIG.SCREEN_MID, Constants.WINDOW_BODY_1.height/4+DEFAULT_QUESTION_POS_Y+((int)Constants.WINDOW_HEAD_1b.height)+l_textSize, genSet.paint);
	    
		genSet.setTextProperties(35, Color.WHITE, Align.CENTER);
		drawRoundRect(g, Constants.ASK_WINDOW_BTN_3, Color.DKGRAY, Color.GRAY, OKAY_BTN);  
	    g.drawString("Okay", Constants.ASK_WINDOW_BTN_3, genSet.paint);
		 
	}
	
	private void PaintAskToRate(Graphics g) 
	{ 
		g.drawRect(Constants.SCREEN_RECT, Color.argb(120, 0, 0, 0));

		g.drawImage(AssetManager.sprites, WINDOW_POS_X, DEFAULT_QUESTION_POS_Y, Constants.WINDOW_HEAD_1b);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, Constants.WINDOW_HEAD_1b.height+ DEFAULT_QUESTION_POS_Y, Constants.WINDOW_BODY_1);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, Constants.WINDOW_HEAD_1b.height+ DEFAULT_QUESTION_POS_Y+ Constants.WINDOW_BODY_1.height, Constants.WINDOW_BOTTOM_1);

		int l_textSize = 48;
		genSet.setTextProperties(l_textSize, Color.WHITE, Align.CENTER);
	    g.drawString( "RATE US", CONFIG.SCREEN_MID, DEFAULT_QUESTION_POS_Y+((int)Constants.WINDOW_HEAD_1b.height/2)+l_textSize/2, genSet.paint);  

		l_textSize = 35;
		genSet.setTextProperties(l_textSize, Color.DKGRAY, Align.CENTER);
	    g.drawString("Did you like " + GameScreen.getGameTitle() + "?", CONFIG.SCREEN_MID, DEFAULT_QUESTION_POS_Y+((int)Constants.WINDOW_HEAD_1b.height)+l_textSize, genSet.paint);

		
	}
	private void PaintWindow(Graphics g,int p_height ) 
	{
		int l_yAdjust = p_height<4?((4-p_height)*ADJUST_POS_Y):0; 
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, l_yAdjust+WINDOW_POS_Y, Constants.WINDOW_HEAD_1b);
		for(int idx=0;idx<p_height;idx++) 
		{
			g.drawImage(AssetManager.sprites, WINDOW_POS_X, l_yAdjust+Constants.WINDOW_HEAD_1b.height+ WINDOW_POS_Y+idx*ADJUST_POS_Y, Constants.WINDOW_BODY_1);
		}
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, l_yAdjust+Constants.WINDOW_HEAD_1b.height+ WINDOW_POS_Y+p_height*ADJUST_POS_Y, Constants.WINDOW_BOTTOM_1);
	 
				 
	}

	private final static int DEFAULT_QUESTION_POS_Y = Constants.ScaleY(450);
	private void PaintWindowQuestion(Graphics g,  String p_title, String p_question) 
	{ 
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, DEFAULT_QUESTION_POS_Y, Constants.WINDOW_HEAD_1b);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, Constants.WINDOW_HEAD_1b.height+ DEFAULT_QUESTION_POS_Y, Constants.WINDOW_BODY_1);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, Constants.WINDOW_BODY_1.height+Constants.WINDOW_HEAD_1b.height+ DEFAULT_QUESTION_POS_Y, Constants.WINDOW_BODY_1);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, Constants.WINDOW_BODY_1.height+Constants.WINDOW_HEAD_1b.height+ DEFAULT_QUESTION_POS_Y+ Constants.WINDOW_BODY_1.height, Constants.WINDOW_BOTTOM_1);

		int l_textSize = 48;
		genSet.setTextProperties(l_textSize, Color.WHITE, Align.CENTER);
	    g.drawString(p_title, CONFIG.SCREEN_MID, DEFAULT_QUESTION_POS_Y+((int)Constants.WINDOW_HEAD_1b.height/2)+l_textSize/2, genSet.paint);  

		l_textSize = 35;
		genSet.setTextProperties(l_textSize, Color.DKGRAY, Align.CENTER);
	    g.drawString(p_question, CONFIG.SCREEN_MID, Constants.WINDOW_BODY_1.height/4+DEFAULT_QUESTION_POS_Y+((int)Constants.WINDOW_HEAD_1b.height)+l_textSize, genSet.paint);
	    
		genSet.setTextProperties(35, Color.WHITE, Align.CENTER);
		drawRoundRect(g, Constants.ASK_WINDOW_BTN_1, Color.DKGRAY, Color.GRAY, YES_BTN);  
	    g.drawString("Yes", Constants.ASK_WINDOW_BTN_1, genSet.paint);
		
		drawRoundRect(g, Constants.ASK_WINDOW_BTN_2, Color.DKGRAY, Color.GRAY, NO_BTN);
	    g.drawString("No", Constants.ASK_WINDOW_BTN_2, genSet.paint);
	}
	@Override
	public void Updates(float p_deltaTime) 
	{ 
		switch(m_qType)
		{ 
			case EXIT_PLAY:     break; 
			case GO_TO_MENU: break;
			case ASK_RESET: break;
			case RATE_US: break;
			case NO_CONNECT: break;
			case PURCH_SUCCESS_UNDO: break;
			case PURCH_SUCCESS_QMERGE: break;
			default: break;
		}
		
	}
	
	@Override
	public void TouchUpdates(TouchEvent g) 
	{ 
		switch(m_qType)
		{   
			case EXIT_GAME:  ExitGameTouchUpdates(g);break; 
			case GO_TO_MENU: GoToMenuTouchUpdates(g);break; 
			case ASK_RESET:  ResetTouchUpdates(g);   break; 
			case NO_CONNECT: NotifWindowTouchUpdates(g);   break; 
			case PURCH_SUCCESS_UNDO: 
			case PURCH_SUCCESS_QMERGE: NotifWindowTouchUpdates(g); break;
			default:break; 
		}	
		
		if(buttonPressed!=0 &&g.type == TouchEvent.TOUCH_DOWN)
		{ 
			AssetManager.btn_1.play();
		}
	}
	
	public void NotifWindowTouchUpdates(TouchEvent g) 
	{
	 
		drawRoundRect(g, Constants.ASK_WINDOW_BTN_3, Color.DKGRAY, Color.GRAY, OKAY_BTN); 
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case OKAY_BTN:   m_isClose = true; break;  
			} 

			if(buttonPressed!=0)
				buttonPressed=0;
		} 
	} 
	
	
	public boolean TouchUpdatesRet(TouchEvent g) 
	{ 
		switch(m_qType)
		{  
			case EXIT_PLAY: return GoToLevelSelectTouchUpdates(g);   
		}	
		
		if(buttonPressed!=0 &&g.type == TouchEvent.TOUCH_DOWN)
		{ 
			AssetManager.btn_1.play();
		}
		
		return false;
	}
	public void ResetTouchUpdates(TouchEvent g) 
	{

		drawRoundRect(g, Constants.ASK_WINDOW_BTN_1, Color.DKGRAY, Color.GRAY, YES_BTN);
		drawRoundRect(g, Constants.ASK_WINDOW_BTN_2, Color.DKGRAY, Color.GRAY, NO_BTN);
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case YES_BTN: 
					 m_isClose = true;
					GameScreen.ResetData(); break; 
				case NO_BTN: m_isClose=true;  break;  
			} 

			if(buttonPressed!=0)
				buttonPressed=0;
		} 
	} 
	
	
	public boolean GoToLevelSelectTouchUpdates(TouchEvent g) 
	{

		drawRoundRect(g, Constants.ASK_WINDOW_BTN_1, Color.DKGRAY, Color.GRAY, YES_BTN);
		drawRoundRect(g, Constants.ASK_WINDOW_BTN_2, Color.DKGRAY, Color.GRAY, NO_BTN);
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case YES_BTN:  return true; 
				case NO_BTN: m_isClose=true;  break;  
			} 

			if(buttonPressed!=0)
				buttonPressed=0;
		} 
		return false;
	} 
	public void GoToMenuTouchUpdates(TouchEvent g) 
	{

		drawRoundRect(g, Constants.ASK_WINDOW_BTN_1, Color.DKGRAY, Color.GRAY, YES_BTN);
		drawRoundRect(g, Constants.ASK_WINDOW_BTN_2, Color.DKGRAY, Color.GRAY, NO_BTN);
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case YES_BTN:  GameScreen.GoToMenu(true); break; 
				case NO_BTN: m_isClose=true;  break;  
			} 

			if(buttonPressed!=0)
				buttonPressed=0;
		} 
	} 
	public void ExitGameTouchUpdates(TouchEvent g) 
	{

		drawRoundRect(g, Constants.ASK_WINDOW_BTN_1, Color.DKGRAY, Color.GRAY, YES_BTN);
		drawRoundRect(g, Constants.ASK_WINDOW_BTN_2, Color.DKGRAY, Color.GRAY, NO_BTN);
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case YES_BTN:  GameScreen.CloseGame(); break; 
				case NO_BTN: m_isClose=true;  break;  
			} 

			if(buttonPressed!=0)
				buttonPressed=0;
		} 
	} 
	
	
	@Override
	public void backButton() 
	{	
		switch(m_qType)
		{
			case EXIT_PLAY:     m_isClose = true; break; 
			case GO_TO_MENU: m_isClose = true;break;
			case ASK_RESET:  m_isClose = true;break;
			case RATE_US: break;
			case NO_CONNECT: break;
			case PURCH_SUCCESS_UNDO: 
			case PURCH_SUCCESS_QMERGE: m_isClose=true;break;
			default: 	break;
		} 
	} 
	
	private void PaintExitGame(Graphics g)
	{  

		g.drawRect(Constants.SCREEN_RECT, Color.argb(120, 0, 0, 0));
		PaintWindowQuestion(g, "EXIT GAME","Are you sure want to exit?");       
	}

	 
	private void PaintWindowB(Graphics g,String p_title, String p_question)
	{  
		g.setTypeface(AssetManager.Comforta,genSet.paint);
		g.drawRect(Constants.SCREEN_RECT, Color.argb(150, 0, 0, 0));
		g.drawRoundRect(Constants.ASK_WINDOW_B, 20, 20, BUTTON_COLOR);
		g.drawRoundRect(Constants.ASK_WINDOW_HEADER_B, 20, 20, BUTTON_COLOR);
		g.drawRect(Constants.ASK_WINDOW_HEADER_B.left,Constants.ASK_WINDOW_HEADER_B.bottom-20,Constants.ASK_WINDOW_HEADER_B.width(),20,  BUTTON_COLOR);
 
		genSet.setTextProperties(45, Color.WHITE, Align.CENTER);
	    g.drawString(p_title, Constants.ASK_WINDOW_HEADER_B, genSet.paint);  

		genSet.setTextProperties(35, Color.DKGRAY, Align.CENTER);
	    g.drawString(p_question, Constants.ASK_WINDOW_B, genSet.paint);
	    
		genSet.setTextProperties(35, Color.WHITE, Align.CENTER);
		drawRoundRect(g, Constants.ASK_WINDOW_BTN_1, Color.DKGRAY, Color.GRAY, YES_BTN); 
		
	    g.drawString("Yes", Constants.ASK_WINDOW_BTN_1, genSet.paint);
		
		drawRoundRect(g, Constants.ASK_WINDOW_BTN_2, Color.DKGRAY, Color.GRAY, NO_BTN);
	    g.drawString("No", Constants.ASK_WINDOW_BTN_2, genSet.paint);
	} 
  
}
