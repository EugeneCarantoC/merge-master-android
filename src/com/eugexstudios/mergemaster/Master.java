package com.eugexstudios.mergemaster;

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.ImageData;
import com.eugexstudios.utils.AssetManager;
import com.eugexstudios.utils.genSet;

import android.graphics.Color;
import android.graphics.Paint.Align;

public class Master {

	private enum state{ not_visib, entering, idled, exiting};
	private enum gesture{still, hand_moved};
	private enum face{looking,look_away,eyes_closed,pissed};

	private final ImageData STILL       = new ImageData(0,500*0,350,500);
	private final ImageData HAND_MOVED  = new ImageData(0,500*1,350,500);

	private final ImageData LOOKING       = new ImageData(350*1,500*0,350,500);
	private final ImageData LOOK_AWAY     = new ImageData(350*1,500*1,350,500);
	private final ImageData EYES_CLOSED   = new ImageData(350*1,500*2,350,500);
	private final ImageData PISSED 		  = new ImageData(350*1,500*3,350,500);
	
	private final ImageData HEAD_STILL  = new ImageData(350*2,500*0,350,500);
	
	private final int TEXT_COLOR = Color.rgb(10, 10, 10);

	private String m_saying_line[];
	private String m_say_line[];
	
	private enum say_type {stmt_1,stmt_2,stmt_3,stmt_4,stmt_5,stmt_6,stmt_7,stmt_8}
	private say_type m_sayType;
	
	private state m_state;  
	private int m_xPos;
	private int m_yPos;
	private face m_face;
	private gesture m_gesture;
	private ImageData m_faceImage;
	private ImageData m_gestureImage;
	public Master() 
	{
		setGesture(gesture.still);
		setFace(face.looking);
		m_xPos = -50;
		m_yPos = 770; 
		m_sayType = say_type.stmt_1;  
		m_sayState = say_state.none;
		m_state = state.not_visib;
 
	}

	private enum say_state{none, pop_up, saying, said, pop_out};
	private say_state m_sayState;
	
	public void Paint(Graphics g) 
	{  
		switch(m_state) 
		{
		case entering:  
			g.drawImage(AssetManager.master, m_xPos-(m_anim[1]-m_anim[0]), m_yPos, m_gestureImage);
			g.drawImage(AssetManager.master, m_xPos-(m_anim[1]-m_anim[0]), m_yPos, HEAD_STILL);  
			g.drawImage(AssetManager.master, m_xPos-(m_anim[1]-m_anim[0]), m_yPos, m_faceImage);
			break;
		case idled:  
			g.drawImage(AssetManager.master, m_xPos, m_yPos, m_gestureImage);
			g.drawImage(AssetManager.master, m_xPos, m_yPos, HEAD_STILL);  
			g.drawImage(AssetManager.master, m_xPos, m_yPos, m_faceImage);
			switch(m_sayState) 
			{
				case pop_up: break;
				case saying: PaintSay(g); 	break;
				case said:   PaintSay(g); 	break;
				case pop_out: break; 
				default:break;
			} 
			break;
		case exiting: 
			g.drawImage(AssetManager.master, m_xPos-m_anim[0], m_yPos, m_gestureImage);
			g.drawImage(AssetManager.master, m_xPos-m_anim[0], m_yPos, HEAD_STILL);  
			g.drawImage(AssetManager.master, m_xPos-m_anim[0], m_yPos, m_faceImage);
			break; 
		default:break;
		} 
		
//		g.drawString(m_sayState.toString()+"", 100, 200, genSet.paint);
		
	}
	
	private void setFace(face p_face)
	{ 
		m_face = p_face;
		switch(m_face) 
		{
		case looking: m_faceImage = LOOKING;break;
		case look_away: m_faceImage = LOOK_AWAY;break;
		case eyes_closed:  m_faceImage = EYES_CLOSED;break;
		case pissed: m_faceImage = PISSED;break;
		}
		
	}

	
	public void Say(String p_str[])
	{ 
		m_sayState = say_state.pop_up;
		if(p_str.length==1)
		{
			m_sayType = say_type.stmt_1;
		}
		else if(p_str.length==2)
		{
			m_sayType = say_type.stmt_2; 
		}
		else if(p_str.length==3)
		{
			m_sayType = say_type.stmt_3; 
		}
		else if(p_str.length==4)
		{
			m_sayType = say_type.stmt_4; 
		}
		else if(p_str.length==5)
		{
			m_sayType = say_type.stmt_5; 
		}
		else if(p_str.length==6)
		{
			m_sayType = say_type.stmt_6; 
		}
		else if(p_str.length==7)
		{
			m_sayType = say_type.stmt_7; 
		}
		else if(p_str.length==8)
		{
			m_sayType = say_type.stmt_8; 
		} 
		m_anim = new float[5];
		m_anim[0] =0;
		m_anim[1] =0;
		m_say_line = p_str; 

		m_saying_line = new String[m_say_line.length]; 
		for(int idx=0;idx<m_saying_line.length;idx++)
			m_saying_line[idx]="";
	} 
	
	 
	private void PaintSay(Graphics g) 
	{ 
		genSet.setTextProperties(32, TEXT_COLOR, Align.LEFT);
		switch(m_sayType) 
		{
		case stmt_1: 
			genSet.setTextProperties(36, TEXT_COLOR, Align.CENTER);
			g.drawImage(AssetManager.speech_bubble, m_xPos+330, m_yPos, SPEECH_BUBBLE_4);   
				g.drawString(m_saying_line[0],  m_xPos+330+141, m_yPos+305  , genSet.paint);  
			break; 
		case stmt_2: 
			genSet.setTextProperties(34, TEXT_COLOR, Align.LEFT);
			g.drawImage(AssetManager.speech_bubble, m_xPos+330, m_yPos, SPEECH_BUBBLE_2);  
			for(int idx=0;idx<m_saying_line.length;idx++)
				g.drawString(m_saying_line[idx],  m_xPos+330+57, m_yPos+271 +45*idx, genSet.paint); 
			break;
		case stmt_3:   
			g.drawImage(AssetManager.speech_bubble, m_xPos+330, m_yPos, SPEECH_BUBBLE_2); 
			for(int idx=0;idx<m_saying_line.length;idx++)
				g.drawString(m_saying_line[idx],  m_xPos+330+60, m_yPos+253 +39*idx, genSet.paint); 
			break;
		case stmt_4: 
			g.drawImage(AssetManager.speech_bubble, m_xPos+330, m_yPos, SPEECH_BUBBLE_3); 
			for(int idx=0;idx<m_saying_line.length;idx++)
				g.drawString(m_saying_line[idx],  m_xPos+330+60, m_yPos+218 +40*idx, genSet.paint);  
			break; 
		case stmt_5:  
				g.drawImage(AssetManager.speech_bubble, m_xPos+330, m_yPos, SPEECH_BUBBLE_1);   
				for(int idx=0;idx<m_saying_line.length;idx++)
					g.drawString(m_saying_line[idx],  m_xPos+330+60, m_yPos+111 +42*idx, genSet.paint); 
			break;
		case stmt_6:  
			g.drawImage(AssetManager.speech_bubble, m_xPos+330, m_yPos, SPEECH_BUBBLE_1);   
			for(int idx=0;idx<m_saying_line.length;idx++)
				g.drawString(m_saying_line[idx],  m_xPos+330+60, m_yPos+85 +41*idx, genSet.paint); 
			break;

		case stmt_7:  
			g.drawImage(AssetManager.speech_bubble, m_xPos+330, m_yPos, SPEECH_BUBBLE_1);   
			for(int idx=0;idx<m_saying_line.length;idx++)
				g.drawString(m_saying_line[idx],  m_xPos+330+65, m_yPos+74 +41*idx, genSet.paint); 
		break;

		case stmt_8:  
			g.drawImage(AssetManager.speech_bubble, m_xPos+330, m_yPos, SPEECH_BUBBLE_1);   
			for(int idx=0;idx<m_saying_line.length;idx++)
				g.drawString(m_saying_line[idx],  m_xPos+330+60, m_yPos+63 +41*idx, genSet.paint); 
		break;
		}
	}
	
	private final ImageData SPEECH_BUBBLE_1 = new ImageData(410*0,0,410,405);
	private final ImageData SPEECH_BUBBLE_2 = new ImageData(410*1,0,410,405);
	private final ImageData SPEECH_BUBBLE_3 = new ImageData(410*2,0,410,405);
	private final ImageData SPEECH_BUBBLE_4 = new ImageData(410*3,0,410,405);
	 
	private void setGesture(gesture p_gesture)
	{
		m_gesture = p_gesture;
		switch(m_gesture) 
		{
			case still: 	 m_gestureImage = STILL; 		break;
			case hand_moved: m_gestureImage = HAND_MOVED;  break; 
		}
		
	}
	
	private float m_anim[];
	private void sayUpdates(float p_deltaTime)
	{
		switch(m_sayState) 
		{
			default:break;
			case pop_up: m_sayState = say_state.saying; 	 break;
			case saying: 
				/* m_anim[0] - saying line idx
				/* m_anim[1] - saying line end idx
				 * m_anim[2] - tick delay
				 */

				 if((int)(m_anim[2])<1f) 
				 {
					 m_anim[2] += 1*p_deltaTime;
				 }
				 else
				 {
					 m_anim[2] =0;
					 m_saying_line[(int)(m_anim[0])]=m_say_line[(int)(m_anim[0])].substring(0,(int)(m_anim[1]));
						
						if((int)(m_anim[0])<m_say_line.length)
						{
							if((int)(m_anim[1])<m_say_line[(int)(m_anim[0])].length()) 
							{
								 m_anim[1]++;
							}
							else 
							{
								 m_anim[0]++;
								 m_anim[1]=1;

								if(m_anim[0]==m_say_line.length)
								{ 
									m_sayState = say_state.said;
								}
									
							}  
						}

				 } 
				
				break;
			case said:  
				break;
			case pop_out: break;
		}
	}
	public void Updates(float p_deltaTime) 
	{  
		switch(m_state)
		{
		case entering:  enterUpdates(p_deltaTime); break; 
		case idled: 	sayUpdates(p_deltaTime);   break;
		case exiting: 	exitUpdates(p_deltaTime);  break;
		default:break;
		}
		
	}
	
	public void interupt() 
	{
		if(m_sayState==say_state.saying) {
			m_sayState = say_state.said;
			m_saying_line = m_say_line;
		}
	}
	
	public void feedBack() 
	{
		m_sayState = say_state.none;
	} 
	
	
	public void enter() 
	{
		m_state = state.entering;
		m_anim = new float[5];
		m_anim[0] = 0;	 // x pos
		m_anim[1] = 300; // x adjusted
	}
	
	private void enterUpdates(float p_deltaTime) 
	{
		/*
		 * m_anim[0] - x pos
		 * m_anim[1] - x adjusted
		 * */
		m_anim[0] += 20*p_deltaTime;
		
		if(m_anim[0]>=m_anim[1]) 
		{ 
			m_state = state.idled;
		}
	}
	public void exit() 
	{
		m_state = state.exiting;
		m_anim = new float[5];
		m_anim[0] = 0;	 // x pos
		m_anim[1] = 300; // x to adjust
	}
	
	private void exitUpdates(float p_deltaTime) 
	{
		/*
		 * m_anim[0] - x pos
		 * m_anim[1] - x adjusted
		 * */
		m_anim[0] += 20*p_deltaTime;
		
		if(m_anim[0]>=m_anim[1]) 
		{ 
			m_state = state.not_visib;
		}
	}
	
	
	public boolean isTalking() {
		return m_sayState==say_state.saying;
	}
	
	public boolean isWaitingFeedback() {
		return m_sayState==say_state.said;
	}

	public boolean isIdled() {
		return m_state == state.idled;
	}

	public boolean isGone() {

		return m_state == state.not_visib;
	}
	 
	
}
