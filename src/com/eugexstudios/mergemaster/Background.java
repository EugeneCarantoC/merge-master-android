package com.eugexstudios.mergemaster;

import java.util.Random;

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.AssetManager;
import com.eugexstudios.utils.CONFIG;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Screen; 

import android.graphics.Color;

public class Background extends Screen{

	private float m_yAdjust;
	private final int BG_COLOR =Color.rgb(249,237,180);
	private int[][] m_clouds;
	
	public Background() 
	{
		m_yAdjust =CONFIG.SCREEN_HEIGHT-AssetManager.mountain.getHeight()*CONFIG.Y_SCALE;

		int l_cloundKinds = 4;
		int l_xLimit = CONFIG.SCREEN_WIDTH;
		int l_yLimit = (int)(((1280*3)-(int)( AssetManager.mountain.getHeight()*0.92f))*CONFIG.Y_SCALE);
		
		Random l_rand = new Random();
		m_clouds = new int[l_rand.nextInt(6)+8][4];
		for(int idx=0;idx<m_clouds.length;idx++) 
		{ 			
			m_clouds[idx] = new int[]{l_rand.nextInt(l_xLimit),-l_rand.nextInt(l_yLimit),l_rand.nextInt(l_cloundKinds), (l_rand.nextBoolean()?1:-1)};
			
		}
		 
	}
	final static int MOUNTAIN_BASE = Constants.ScaleY(700);
	@Override
	protected void Paint(Graphics g) { 
		g.drawRect(Constants.SCREEN_RECT,BG_COLOR);
		g.drawImage(AssetManager.mountain, 0,MOUNTAIN_BASE+(int)(m_yAdjust),0,0,AssetManager.mountain.getWidth(),AssetManager.mountain.getHeight(),CONFIG.X_SCALE,CONFIG.Y_SCALE);
		for(int idx=0;idx<m_clouds.length;idx++) 
		{ 			
//			g.drawRect( Constants.CLOUDS[m_clouds[idx][2]], (int)m_clouds[idx][0], (int)(m_yAdjust+m_clouds[idx][1]), Color.RED);
			g.drawImage(AssetManager.sprites, m_clouds[idx][0], m_yAdjust+m_clouds[idx][1], Constants.CLOUDS[m_clouds[idx][2]]); 
		}
 
	}

	@Override
	protected void Updates(float p_deltaTime) {
		for(int idx=0;idx<m_clouds.length;idx++) 
		{ 	
			m_clouds[idx][0]+=m_clouds[idx][3];
			
			if(m_clouds[idx][0]>CONFIG.SCREEN_WIDTH&&m_clouds[idx][3]>0)//if going left
				m_clouds[idx][0]=-Constants.CLOUDS[m_clouds[idx][2]].width-20;

			if(m_clouds[idx][0]<-Constants.CLOUDS[m_clouds[idx][2]].width&&m_clouds[idx][3]<0)//if going right
				m_clouds[idx][0]=CONFIG.SCREEN_WIDTH;
		}
		
	}
	public void adjust(int p_yAdd) 
	{
		m_yAdjust+=(float)(p_yAdd/4);
	}
	@Override
	protected void TouchUpdates(TouchEvent g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void backButton() {
		// TODO Auto-generated method stub
		
	}

}
