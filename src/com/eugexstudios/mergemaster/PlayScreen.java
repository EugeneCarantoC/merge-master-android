	package com.eugexstudios.mergemaster;

import java.util.Vector;
 
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Paint.Align;

import com.eugexstudios.adprovider.StartApp.VideoAdType;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.ImageData;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.mergemaster.AskUserWindow.QUESTION; 
import com.eugexstudios.mergemaster.creator.LevelUtil;
import com.eugexstudios.mergemaster.levels.LevelManager;
import com.eugexstudios.utils.AssetManager;
import com.eugexstudios.utils.CONFIG;
import com.eugexstudios.utils.ColorManager;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Constants.game_mode;
import com.eugexstudios.utils.Debugger;
import com.eugexstudios.utils.Screen;
import com.eugexstudios.utils.genSet;

public class PlayScreen extends Screen{

	private enum exit_dest {menu, levelSelect};
	public enum state {
	    PLAY
      , PASSING
      , GAMEOVER
      , TO_GAMEOVER
      , LEVEL_COMPLETE
      , EXIT_PLAY
      , ENTRY_ANIM
      , TO_COMPLETE
      , NO_TILE 
      , ASK_TO_RATE
      , PLACEHOLDER
      , LEVEL_BAR_SHINE
      , EXIT_FADE
      , PURCH_UNDO
      , PURCH_QMERGE
      , PURCH_SUCCESS
      , QUICK_MERGE_SELECT
      , QUICK_MERGE_ANIM
      };
      
	private final byte UNDO_BTN =1;
	private final byte RESTART_BTN =2;	
	private final byte QUICK_MERGE_BTN =3;	
	private final byte CANCEL_QUICK_MERGE_BTN =4;	
	private final int DEBUG_PREV_BTN =999;
	private final int DEBUG_NEXT_BTN =998; 
	
	private final int ANIM_LIMIT =8;  

	private final int SHADOW_COLOR     = Color.argb(68, 0, 0, 0);
	private final int SHADOW_COLOR_2   = Color.argb(42, 0, 0, 0);
	private final int BACK_COVER_COLOR = Color.argb(30, 249, 237, 180);
	private final Rect DEBUG_BTN_PREV = new Rect(230,20,400,120);
	private final Rect DEBUG_BTN_NEXT = new Rect(420,20,590,120);
	private static final Point UNDO_STR_POS = new Point( Constants.CTR_IMG.x+Constants.CTR_IMG.width/2, Constants.CTR_IMG.y+((int)(Constants.CTR_IMG.height*0.65f))); 
	private static final int QUICKMERGE_BTN_ADJUST = -135;
	
	
	private static final Point COIN_POS = new Point( Constants.ScaleX(10),Constants.ScaleY(170));
	private static final Point COIN_TEXT_POS = new Point((int)(Constants.GAMEPLAY_COIN.width*CONFIG.Y_SCALE+Constants.ScaleX(15)), COIN_POS.y+Constants.ScaleY(42));
	
	private static final int LEVEL_BAR_TYPE =0;
	

	private LevelCompleteWindow                  m_levelCompleteWindow;
	private GameOverWindow                       m_gameOverWindow;
	private AskUserWindow                        m_askWindow;
	private UndoPurchScreen                      m_undoPurchScreen;
	private QuickMergePurchScreen                m_quickMergePurchScreen;
	private Background                           m_background;
	
	private Level                                m_levelData; 
	private game_mode                            m_mode;
	private int                                  m_level;

	private static state                         m_state;
	private state                                m_lastState;
	private exit_dest                            m_exitDest; 
	
	private int                                  m_undoAvailable;
	private int                                  m_coins;
	private int                                  m_quickMergeAvailable;
	
	private Tile                                 m_tiles[][];
	private int                                  m_tileCount; 
	private boolean                              m_continuesPass;
	private int                                  m_groupCount; 
	private int                                  m_expectedGroupCount; 
	private Point                                m_passTargetPt;
	private int                                  m_passValOnTarget;
	private Vector<Point>                        m_leftBehindTiles; 
	private Vector<int[][]>                      m_tilesValueHistory;
	private Vector<boolean[][]>                  m_tileMergeStatus;
	private int                                  m_tileHistCtr;
	private int                                  m_bestMove;
	private int                                  m_moveCtr;
	
	private ImageData                            m_levelBarImage; 
	private float[]                              m_anim; 
	private boolean                              m_replay;
	private boolean 							 m_coinVisib; 		
	private boolean 							 m_undoVisib; 		
	private boolean 							 m_levelBarVisib; 		
	
	private boolean 							 m_quickMergeVisib; 		
	private boolean 							 m_restartVisib; 				 
	private Rect TILE_BACK_RECT;
	private Rect COIN_BACK_RECT;
	
	public PlayScreen(game_mode p_mode, int p_level, boolean p_isReplay)
	{    
		genSet.game.getGraphics().setTypeface(AssetManager.Comforta,genSet.paint);
		m_levelData = LevelManager.GetLevel(p_mode,p_level); 
		m_background = new Background();  
		SetTileBackRect(m_levelData.getTileSize());
		m_mode  = p_mode;
		m_level = p_level;
		m_replay = p_isReplay;
		SetLevelBarImage(LEVEL_BAR_TYPE);
		GoToPlayEntraceAnim();   
	}  
 	
	public PlayScreen(Level p_levelData)
	{
		// For game creator test   
		genSet.game.getGraphics().setTypeface(AssetManager.Comforta,genSet.paint);
		m_levelData = p_levelData; 
		m_background = new Background();  
		m_mode  = game_mode.mode_1;
		m_level = 0;
		SetLevelBarImage(LEVEL_BAR_TYPE);
		Restart(true);  
	}  
	 
	protected void Restart(boolean p_hasFade)
	{
		SetState(state.PLAY);
		m_anim  = new float[ANIM_LIMIT]; 
		m_tiles = LevelUtil.cloneTiles(m_levelData.getTiles());    
		int[] l_tileAndGroupCnt = GetTileAndGroupCount();
		m_tileCount             = l_tileAndGroupCnt[0]; 
		m_expectedGroupCount    = l_tileAndGroupCnt[1];
		m_groupCount            = m_expectedGroupCount;   
		m_continuesPass         = false;
		UpdateLineAdjacent();  
		m_undoAvailable         = GameScreen.GetUndoAvailable();
		m_quickMergeAvailable         = GameScreen.GetQuickMergeAvailable();
		
		UpdateCoins();
		//initiate tile history
		m_tilesValueHistory = new Vector<int[][]>();
		m_tileMergeStatus = new Vector<boolean[][]>();
		m_tileHistCtr = 0;
		SaveHistory();
		
		m_moveCtr = 0;
		m_bestMove = GameScreen.GetBestMove(m_mode, m_level);
		m_bestMove = m_bestMove==-1?100000000:m_bestMove;
  
		
		if(GameScreen.isOnTutorial()) 
		{
			m_coinVisib =false;
			m_undoVisib =false;
			m_levelBarVisib=false;
			m_quickMergeVisib=false;
			m_restartVisib = false;
			SetState(state.NO_TILE);
		}
		else 
		{
			m_coinVisib =true;
			m_undoVisib = true;
			m_quickMergeVisib=true;
			m_restartVisib =true;
			m_levelBarVisib=true;
			AnimateEntry(p_hasFade);  
		}

		
//
//		 AskToExit(); 
//		 SetState(state.LEVEL_COMPLETE);
//		m_levelCompleteWindow = new LevelCompleteWindow(3, true);
		

//		 SetState(state.GAMEOVER);
//		 m_gameOverWindow = new GameOverWindow();
 
	}  
	
	@Override
	public void Paint(Graphics g) { 
		
		m_background.Paint(g);
		switch(m_state)
		{ 
 
			case LEVEL_BAR_SHINE: 
			case PASSING:  		 
			case PLAY:           PaintPlay(g);            break;  
			case GAMEOVER:       PaintGameOver(g);        break;
			case EXIT_PLAY:	     PaintExitPlay(g);        break; 
			case EXIT_FADE:      PaintExitFadeAnim(g);    break;
			case PURCH_UNDO:     PaintPurchUndo(g);       break;
			case PURCH_QMERGE:   PaintQMergePurch(g);     break; 
			case ENTRY_ANIM:     PaintEntryAnim(g);       break;
			case TO_GAMEOVER:    PaintToGameOver(g);      break;
			case TO_COMPLETE:    PaintToComplete(g);      break;
			case ASK_TO_RATE:    PaintAskToRate(g);       break;
			case NO_TILE: paintNoTile(g);     break;
			case LEVEL_COMPLETE: PaintToLevelComplete(g); break; 
			case PURCH_SUCCESS:  PaintPurchSuccess(g);   break; 
			case QUICK_MERGE_ANIM:  PaintQuickMergeAnim(g);            break;   
			case QUICK_MERGE_SELECT:   PaintQuickMergeSelect(g);    break;  
			default:
				break;
		} 
		
		 genSet.setTextProperties(30, Color.WHITE,Align.LEFT); 
//	     g.drawString(m_state.toString() +"", CONFIG.SCREEN_WIDTH -200, CONFIG.SCREEN_HEIGHT-20, genSet.paint);
		 if(Debugger.show_stats && !Debugger.PROD_MODE)
 		 {  
			 genSet.setTextProperties(40, Color.WHITE,Align.LEFT);
//		     g.drawString("Tile count: " + m_tileCount, 170,40+30*0,  genSet.paint);
//		     g.drawString("Group count: " + m_groupCount, 170,40+30*1,  genSet.paint);
		     g.drawString(+m_moveCtr+ "/" + m_bestMove, 50,CONFIG.SCREEN_HEIGHT-50,  genSet.paint);    
 		 } 
		 if(!Debugger.PROD_MODE&&Debugger.has_level_mover)
 		 {  
			drawRoundRect(g, DEBUG_BTN_PREV, Color.DKGRAY,Color.DKGRAY,DEBUG_PREV_BTN);
			drawRoundRect(g, DEBUG_BTN_NEXT, Color.DKGRAY,Color.DKGRAY,DEBUG_NEXT_BTN);
 		 }
	} 
	

	@Override
	public void Updates(float p_deltaTime) 
	{
		m_background.Updates(p_deltaTime);
		switch(m_state)
		{
			case PASSING:
			case PLAY:              TileUpdates(p_deltaTime);                   break;
			case GAMEOVER:          m_gameOverWindow.Updates(p_deltaTime);      break;
			case EXIT_FADE:         ExitUpdates(p_deltaTime);                   break;
			case EXIT_PLAY:         ExitPlayUpdates(p_deltaTime);               break;  
			case ENTRY_ANIM:        EntryUpdates(p_deltaTime);                  break;
			case PURCH_UNDO:        PurchUndoUpdates(p_deltaTime);              break;
			case PURCH_QMERGE:      PurchQuickMergeUpdates(p_deltaTime);		break;
			case TO_GAMEOVER:       toGameOverUpdates(p_deltaTime);             break;
			case TO_COMPLETE:       toCompleteUpdates(p_deltaTime);             break; 
			case LEVEL_COMPLETE:    m_levelCompleteWindow.Updates(p_deltaTime); break; 
			case LEVEL_BAR_SHINE:   LevelBarAnimUpdates(p_deltaTime);           break;
			case NO_TILE:  break; 
			case ASK_TO_RATE:     break;
			case PLACEHOLDER:     break;
			case PURCH_SUCCESS:  PurchSuccessUpdates(p_deltaTime);  break;

			case QUICK_MERGE_SELECT:  break;
			case QUICK_MERGE_ANIM: quickMergeUpdates(p_deltaTime); break;

			default:
				break;
		}
	}
	
	@Override
	public void TouchUpdates(TouchEvent g) {
		
		switch(m_state)
		{  
			case PLAY:           PlayTouchUpdates(g);                    break;
			case EXIT_PLAY:      ExitPlayTouchUpdates(g);                break;
			case GAMEOVER:       m_gameOverWindow.TouchUpdates(g);       break;
			case PURCH_UNDO:     m_undoPurchScreen.TouchUpdates(g);      break; 
			case PURCH_QMERGE:   m_quickMergePurchScreen.TouchUpdates(g);break;
			case LEVEL_COMPLETE: m_levelCompleteWindow.TouchUpdates(g);  break;			
			case TO_COMPLETE: 		break; 
			case NO_TILE:  	break; 
			case ASK_TO_RATE:  		break;
			case PLACEHOLDER:  		break;
			case LEVEL_BAR_SHINE:   break;
			case EXIT_FADE:  		break;
			case PURCH_SUCCESS:  m_askWindow.TouchUpdates(g);	break;
			case QUICK_MERGE_SELECT:QuickMergeTouchUpdate(g);  break;
			default:
				break;
		} 
	}

	private void PurchSuccessUpdates(float p_deltaTime) 
	{
		m_askWindow.Updates(p_deltaTime);
		if(m_askWindow.isClosed()) 
		{
			SetState(state.PLAY);
		}
	}
	
	private void GoToPlayEntraceAnim() 
	{   
		Restart(true);  
		m_anim[0] = 255; 
	}

	protected void SetState(state p_state)
	{
		m_lastState = m_state;
		m_state     = p_state;
	}
	
	private void GoToPurchUndo() 
	{
		SetState(state.PURCH_UNDO);
		m_undoPurchScreen = new UndoPurchScreen();
	}
	
	private void GoToPurchQuickMerge()
	{
		SetState(state.PURCH_QMERGE);
		m_quickMergePurchScreen = new QuickMergePurchScreen();
	}	
	
	private void PaintPurchUndo(Graphics g)
	{
	    PaintPlay(g);  
	    m_undoPurchScreen.Paint(g); 
	}

	private void PaintQMergePurch(Graphics g)
	{
	    PaintPlay(g);  
	    m_quickMergePurchScreen.Paint(g); 
	}
	
	private void PaintEntryAnim(Graphics g)
	{
	    PaintPlay(g);  
		g.drawRect(Constants.SCREEN_RECT, Color.argb((int)(m_anim[0]), ColorManager.GAME_THEME_1_R,  ColorManager.GAME_THEME_1_G,  ColorManager.GAME_THEME_1_B));

	}
	
	private void PaintExitFadeAnim(Graphics g)
	{
	    PaintPlay(g);  
		g.drawRect(Constants.SCREEN_RECT, Color.argb((int)(m_anim[0]), ColorManager.GAME_THEME_1_R,  ColorManager.GAME_THEME_1_G,  ColorManager.GAME_THEME_1_B));

	} 
	 
	private void PaintPurchSuccess(Graphics g)
	{ 
		PaintPlay(g); 
		m_askWindow.Paint(g);
	}
	
	private void PaintAskToRate(Graphics g)
	{ 
		PaintPlay(g); 
		m_askWindow.Paint(g);
	}
  
 
	private void PaintToLevelComplete(Graphics g)
	{ 
		PaintPlay(g);
		m_levelCompleteWindow.Paint(g);
	}

	private void PaintExitPlay(Graphics g)
	{
		PaintPlay(g); 
		m_askWindow.Paint(g);
	}
	 
	private void PaintToComplete(Graphics g)
	{ 
		PaintPlay(g);
	}
	
	private void PaintToGameOver(Graphics g)
	{  

		g.drawRect(Constants.SCREEN_RECT, Color.argb(50, 255, 1, 12)); // Red back fade
		PaintPlay(g);
	}
	public state getState() 
	{
		return m_state;
	}
	
	private void PaintGameOver(Graphics g)
	{ 

		PaintPlay(g);
		m_gameOverWindow.Paint(g);
	} 
	
	private void SetLevelBarImage(int p_levelBarType)
	 {
		 m_levelBarImage = new ImageData(-145,15,0,125*p_levelBarType,350,125);
	 }
	
	private void PaintHeaders(Graphics g)
	{  
		if(m_state==state.QUICK_MERGE_SELECT||m_state==state.QUICK_MERGE_ANIM)
			g.drawImage(AssetManager.header_style_1,0,0,720*2,0,720,1280);
		else
			g.drawImage(AssetManager.header_style_1,0,0);

		if(m_levelBarVisib)
		{
			g.drawImage(AssetManager.levelbar, m_levelBarImage);
			
			if(m_state==state.LEVEL_BAR_SHINE) 
			{
				g.drawImage(AssetManager.levelbar_anim, m_levelBarImage.x, m_levelBarImage.y, 0,-4+(125*((int)(m_anim[2]))), 350,125); 
			}
			genSet.setTextProperties(83, SHADOW_COLOR,Align.CENTER); 
			g.drawString("" + m_level, 90+4, 100+6, genSet.paint);
			genSet.setTextProperties(83, Color.WHITE,Align.CENTER); 
			g.drawString("" + m_level, 90, 100, genSet.paint);
		}
	}
	


	
	private void PaintPlay(Graphics g)
	{ 
		g.drawRect(Constants.SCREEN_RECT, BACK_COVER_COLOR);
		PaintHeaders(g);
		PaintTiles(g);
		
		if(m_undoVisib) 
		{ 
			PaintUndoButton(g); 
		}
		
		if(m_coinVisib) 
		{
			PaintCoins(g); 
		}
		
		if(m_restartVisib) 
		{
			g.drawImage(AssetManager.sprites,  buttonPressed==RESTART_BTN?Constants.RESTART_PLAY_PRESS:Constants.RESTART_PLAY);
		} 

		if(m_quickMergeVisib) 
		{
			PaintQuickMergeButton(g);
		}
	}
	
	private void paintNoTile(Graphics g)
	{ 
		g.drawRect(Constants.SCREEN_RECT, BACK_COVER_COLOR);
		PaintHeaders(g);
//		PaintTiles(g);
		
		if(m_undoVisib) 
		{ 
			PaintUndoButton(g); 
		}
		
		if(m_coinVisib) 
		{
			PaintCoins(g); 
		}
		
		if(m_restartVisib) 
		{
			g.drawImage(AssetManager.sprites,  buttonPressed==RESTART_BTN?Constants.RESTART_PLAY_PRESS:Constants.RESTART_PLAY);
		}
		

		if(m_quickMergeVisib) 
		{
			PaintQuickMergeButton(g);
		} 
	}

	
	private void PaintQuickMergeSelect(Graphics g)
	{  
		 
		g.drawRect(Constants.SCREEN_RECT, BACK_COVER_COLOR);
		
//		if(m_undoVisib) 
//		{ 
//			PaintUndoButton(g); 
//		}
//		
		if(m_coinVisib) 
		{
			PaintCoins(g); 
		}
		
//		if(m_restartVisib) 
//		{
//			g.drawImage(AssetManager.sprites,  buttonPressed==RESTART_BTN?Constants.RESTART_PLAY_PRESS:Constants.RESTART_PLAY);
//		} 

//		if(m_quickMergeVisib) 
//		{
//			PaintQuickMergeButton(g);
//		}

		g.drawRect(Constants.SCREEN_RECT,Color.argb(140, 253, 208, 54)); 
		
		PaintHeaders(g);
		PaintTiles(g); 
		
//		drawRoundRect(g, CANCEL_QUICK_MERGE.x,CANCEL_QUICK_MERGE.y,Constants.CANCEL_MERGE_BTN, Color.BLACK, Color.BLACK, CANCEL_QUICK_MERGE_BTN); 
		g.drawImage(AssetManager.sprites, CANCEL_QUICK_MERGE.x+(buttonPressed==CANCEL_QUICK_MERGE_BTN?-4:0), CANCEL_QUICK_MERGE.y+(buttonPressed==CANCEL_QUICK_MERGE_BTN?4:0),  Constants.CANCEL_MERGE_BTN);
	 
		
	}
	
	private static final Point CANCEL_QUICK_MERGE = new Point(CONFIG.SCREEN_MID- Constants.CANCEL_MERGE_BTN.width/2, 1080);
	private void PaintQuickMergeAnim(Graphics g)
	{ 
		PaintQuickMergeSelect(g);
		m_tiles[m_quickMergeMain.y][m_quickMergeMain.x].paint(g); 
	}
	
	private void PaintCoins(Graphics g)
	{ 
		g.drawRoundRect(COIN_BACK_RECT, 10,10,ColorManager.EMPTY_TILE_COLOR);
		g.drawImage(AssetManager.sprites, COIN_POS.x,COIN_POS.y, Constants.GAMEPLAY_COIN);
		
		genSet.setTextProperties(46, SHADOW_COLOR_2,Align.LEFT); 
		g.drawString("" + m_coins,COIN_TEXT_POS.x+4, COIN_TEXT_POS.y+4, genSet.paint);
		genSet.setTextProperties(46, Color.WHITE,Align.LEFT); 
		g.drawString("" + m_coins,COIN_TEXT_POS, genSet.paint);
	}
	
	private void PaintUndoButton(Graphics g)
	{
//		drawRoundRect(g, Constants.UNDO_BTN, Color.BLACK, Color.BLACK, UNDO_BTN);
		g.drawImage(AssetManager.sprites, buttonPressed==UNDO_BTN?-4:0,buttonPressed==UNDO_BTN?4:0, CanUndo()?Constants.UNDO_BTN:Constants.UNDO_UNAVAIL_BTN);
		
		g.drawImage(AssetManager.sprites, Constants.CTR_IMG );
		
		genSet.setTextProperties(25, SHADOW_COLOR,Align.CENTER); 
		g.drawString("" + (m_undoAvailable==0?"+":m_undoAvailable),UNDO_STR_POS.x+2, UNDO_STR_POS.y+2, genSet.paint);
		genSet.setTextProperties(25, Color.WHITE,Align.CENTER); 
		g.drawString("" + (m_undoAvailable==0?"+":m_undoAvailable),UNDO_STR_POS, genSet.paint);
		
	}
	
	private void PaintQuickMergeButton(Graphics g)
	{
//		drawRoundRect(g, Constants.QUICK_MERGE_BTN, Color.BLACK, Color.BLACK, QUICK_MERGE_BTN);
		g.drawImage(AssetManager.sprites, buttonPressed==QUICK_MERGE_BTN?-4:0,buttonPressed==QUICK_MERGE_BTN?4:0, Constants.QUICK_MERGE_BTN);
		
		g.drawImage(AssetManager.sprites, QUICKMERGE_BTN_ADJUST,0, Constants.CTR_IMG );
		 
		genSet.setTextProperties(25, SHADOW_COLOR,Align.CENTER); 
		g.drawString("" + (m_quickMergeAvailable==0?"+":m_quickMergeAvailable),UNDO_STR_POS.x+QUICKMERGE_BTN_ADJUST+2, UNDO_STR_POS.y+2, genSet.paint);
		genSet.setTextProperties(25, Color.WHITE,Align.CENTER); 
		g.drawString("" + (m_quickMergeAvailable==0?"+":m_quickMergeAvailable),UNDO_STR_POS.x+QUICKMERGE_BTN_ADJUST, UNDO_STR_POS.y, genSet.paint);
		
	}
	
	private void UpdateCoins() 
	{
		m_coins = GameScreen.GetCoins() ;
		COIN_BACK_RECT = new Rect(
				  0
				, COIN_POS.y-Constants.ScaleY(3)
				,  Constants.ScaleX(70)+((m_coins+"").length())* Constants.ScaleX(25)
				, COIN_POS.y+Constants.ScaleY(60)
			);
	}
	
	private void PaintTiles(Graphics g)
	{   
		g.drawRoundRect(TILE_BACK_RECT, 30,30, ColorManager.EMPTY_TILE_COLOR);
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{ 
				m_tiles[idx][idx2].paintAdjacents(g); 
			}
		}
		
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{
				m_tiles[idx][idx2].paint(g);
			}
			
		}
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{ 
				m_tiles[idx][idx2].paint_anim(g);
			}
		} 
	} 

	private void ExitPlayUpdates(float p_deltaTime) 
	{
		if(m_askWindow.isClosed())
		{
			SetState(state.PLAY);
		}
	}

	private void debugPlayBtns(TouchEvent g) 
	{

		if(g.type == TouchEvent.TOUCH_DOWN) 
		{
			 if(!Debugger.PROD_MODE&&Debugger.has_level_mover)
			 {
				drawRoundRect(g, DEBUG_BTN_PREV, Color.DKGRAY,Color.DKGRAY,DEBUG_PREV_BTN);
				drawRoundRect(g, DEBUG_BTN_NEXT, Color.DKGRAY,Color.DKGRAY,DEBUG_NEXT_BTN);
			}
		 }

		if(g.type == TouchEvent.TOUCH_UP) 
		{
			switch(buttonPressed) 
			{
			case DEBUG_PREV_BTN:  GameScreen.PlayGame(m_mode, m_level-1,false);break;
			case DEBUG_NEXT_BTN:  GameScreen.PlayGame(m_mode, m_level+1,false);break; 
			}
		}
	}
 
	private void GoToLevelSelect() 
	{
		m_exitDest = exit_dest.levelSelect;
		SetState(state.EXIT_FADE);
		m_anim = new float[ANIM_LIMIT];
		m_anim[0]=0;
		m_anim[1]=0;
	}
	
	private void PurchUndoUpdates(float p_deltaTime) 
	{ 
		m_undoPurchScreen.Updates(p_deltaTime);
		
		if(m_undoPurchScreen.isClosed()) 
		{
			SetState(m_lastState);
		}
	}
	
	private void PurchQuickMergeUpdates(float p_deltaTime) 
	{ 
		m_quickMergePurchScreen.Updates(p_deltaTime);
		
		if(m_quickMergePurchScreen.isClosed()) 
		{
			SetState(m_lastState);
		}
	} 
	
	private void ExitUpdates(float p_deltaTime) 
	{ 
		/*
		 * 0- fade opacity for Argb
		 * 1- steps
		 * 2- anim buffer
		 * */ 
		if(m_anim[1]==0) 
		{
			if(m_anim[0]<255) 
			{
				m_anim[0] += p_deltaTime*12; 
				if(m_anim[0]>255) 
				{
					m_anim[0]=255;
				}
				
			}
			else 
			{
				m_anim[0]=255;
				GameScreen.InitializeLevelSelect(m_mode,m_level,true); 
				m_anim[1]++;
			}
		}
		else if(m_anim[1]==1) 
		{    
			switch(m_exitDest) 
			{
				case levelSelect: GameScreen.GoToLevelSelect();  break;
				case menu:break;
			}
			
		}
		 
		 
		
	}
	
	private void EntryUpdates(float p_deltaTime) 
	{

		/*
		 * 0- fade opacity for Argb
		 * 1- steps
		 * 2- anim buffer
		 * */ 
		if(m_anim[1]==0) 
		{
			if(m_anim[0]>0) 
			{
				m_anim[0] -= p_deltaTime*12;
				if(m_anim[0]<0)
					m_anim[0]=0;
			}
			else 
			{
				m_anim[0] =0;
				m_anim[1]++;
			}
		}
		else if(m_anim[1]==1) 
		{   
			SetState(state.PLAY);  // this state needs to be recorded for ShineLevelBar status
			ShineLevelBar();
		}
		 
		 
		TileUpdates(p_deltaTime);	
	}
	
	private void ShineLevelBar() 
	{ 
		SetState(state.LEVEL_BAR_SHINE);
		m_anim = new float[ANIM_LIMIT]; 
		m_anim[0] = 0;    // Buffer
		m_anim[1] = 0.01f; // Speed
		m_anim[2] = 0;    // Anim ctr
	}
	
	private void LevelBarAnimUpdates(float p_deltaTime)
	{

//		m_anim[0] // Buffer
//		m_anim[1] // Speed
//		m_anim[2] // Anim ctr
		
		m_anim[0] += p_deltaTime*m_anim[1];
		
		if((int)(m_anim[1])%10==0) 
		{
			m_anim[2]+=1;		
		}
		
		if(m_anim[2]==20)//Frame limit
		{
			SetState(m_lastState);
		} 

		TileUpdates(p_deltaTime);	
	}
	
	private void toCompleteUpdates(float p_deltaTime)
	{ 
		TileUpdates(p_deltaTime);
		
		/*
		 * 0 - buffer
		 * 1 - anim phase
		 * */ 
 
		m_anim[0] += 1*p_deltaTime;
		
		if(m_anim[1]==0)
		{ 
			if(m_anim[0]>50)
			{
				expandInMergedTiles();
				m_anim[0]=0;
				m_anim[1]++;
			}
		} 
		else if(m_anim[1]==1)
		{
			if(m_anim[0]>40)
			{
				expandOutMergedTiles();
				AssetManager.all_merged.play();
				m_anim[0]=0;
				m_anim[1]++;
			}
		} 
		else if(m_anim[1]==2)
		{
			if(m_anim[0]>30)
			{ 
				goToLevelComplete();	
			}
		}  
	}
	
	private void goToLevelComplete()
	{
		if(GameScreen.isOnTutorial()) return;
		SetState(state.LEVEL_COMPLETE);
		m_levelCompleteWindow = new LevelCompleteWindow(m_levelData.determineStar(m_moveCtr), true);
		GameScreen.LevelComplete(m_mode, m_level, m_moveCtr); 
		m_replay=true;
	} 
	
	public void SkipLevel() {  

		SetState(state.LEVEL_COMPLETE);
		m_levelCompleteWindow = new LevelCompleteWindow(1, true);//1 star
		GameScreen.LevelComplete(m_mode, m_level, 10000000); 
	}
		
	private void toGameOverUpdates(float p_deltaTime)
	{ 
		TileUpdates(p_deltaTime);
		/*
		 * 0 - countdown
		 * */
		float l_delayToGameOver = 60f; 
		
		if(m_anim[0]<l_delayToGameOver)
		{
			m_anim[0] += 1*p_deltaTime;
		}
		else
		{ 

			if(GameScreen.isOnTutorial()==false)
			{ 
				SetState(state.GAMEOVER);
				m_gameOverWindow = new GameOverWindow();
				AssetManager.game_over_popup.play();
			}
		}
		
	}
	
	public static boolean isGameComplete()
	{ 
		return m_state==state.TO_COMPLETE;
		
	}
	
	private void updatePlayStatus()
	{  
		int[] l_tileAndGroupCnt = GetTileAndGroupCount();
		m_tileCount  = l_tileAndGroupCnt[0];
		m_groupCount = l_tileAndGroupCnt[1];
		
		if(m_groupCount!=m_expectedGroupCount)
		{
			SetState(state.TO_GAMEOVER);

			AssetManager.tile_let_go.play(0.8f);
			for(int idx=0;idx<m_leftBehindTiles.size();idx++)
			{  
				if(m_tiles[m_leftBehindTiles.get(idx).x][m_leftBehindTiles.get(idx).y].isMerged()==false)
				{
					m_tiles[m_leftBehindTiles.get(idx).x][m_leftBehindTiles.get(idx).y].highlight();
				}
				if(m_tiles[m_leftBehindTiles.get(idx).x][m_leftBehindTiles.get(idx).y].isMerged()==false)
				{
					m_tiles[m_leftBehindTiles.get(idx).x][m_leftBehindTiles.get(idx).y].Disconnect();
				}
			}
			
		} 
		if(m_tileCount == m_expectedGroupCount)
		{ 
			GoToLevelComplete();
			AssetManager.tile_merged.play();
		}
		  
		if(m_state!=state.TO_GAMEOVER && m_lastState== state.PASSING)
		{
			// Handle last tile that was on MERGING and not updated to MERGED
			int l_adjacentTile  = countAdjacent(m_passTargetPt.y,m_passTargetPt.x);  
			if(l_adjacentTile==0)
			{  
				m_tiles[m_passTargetPt.y][m_passTargetPt.x].expandOut(0.08f,0.1f);  
				m_tiles[m_passTargetPt.y][m_passTargetPt.x].MarkAsMerged(); 
				AssetManager.tile_merged.play();
			}
		}
	}  
	
	private int countSameTiles(int p_tileType, boolean p_includeMerged)
	{
		int l_same =0;
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{
				if(m_tiles[idx][idx2].getTileType() == p_tileType)
				{
					if(p_includeMerged)
					{ 
						l_same++;
					}
					else
					{
						if(m_tiles[idx][idx2].isMerged()==false)
						{
							l_same++;
						}
					}
				}
				 
			}
		}
		
		return l_same;
		
	}

	private void GoToLevelComplete()
	{  
		SetState(state.TO_COMPLETE);
		m_anim = new float[ANIM_LIMIT];
		m_anim[0]=0;
		GetTileAndGroupCount(); // to update m_leftBehindTiles vector 
	}
	
	private void expandInMergedTiles()
	{
		for(int idx=0;idx<m_leftBehindTiles.size();idx++)
		{  
			m_tiles[m_leftBehindTiles.get(idx).x][m_leftBehindTiles.get(idx).y].expandInHold(0.085f,0.1f);  
			
		} 
	}
	
	private void expandOutMergedTiles()
	{ 
		for(int idx=0;idx<m_leftBehindTiles.size();idx++)
		{  
			m_tiles[m_leftBehindTiles.get(idx).x][m_leftBehindTiles.get(idx).y].expandOut(0.1f,0.1f);  
		} 
	}

	private void ShowAdjacents()
	{
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{
				m_tiles[idx][idx2].ShowAdjacent();
			}
		}
	}
	
	private void quickMergeUpdates(float p_deltaTime)
	{  
		boolean l_isTileOnPrepareQuickMerge= false;
		boolean l_isTileOnQuickMergeMagnet= false;
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{
				// ###########  QUICK MERGE PHASE 2 ########### 
				l_isTileOnPrepareQuickMerge = m_tiles[idx][idx2].isOnPrepareQuickMerge();
				l_isTileOnQuickMergeMagnet   = m_tiles[idx][idx2].isOnQuickMergeMagnet();
				m_tiles[idx][idx2].Updates(p_deltaTime);  
				if(l_isTileOnPrepareQuickMerge &&  m_tiles[idx][idx2].isOnQuickMergeMagnet()) 
				{ 
					// Signal to animate quick merge of other tile from group 
					QuickMergeAnimateGroup(idx2,idx); 
				}
				// ###########  QUICK MERGE PHASE 2 ########### 
				

				
				// ###########  QUICK MERGE PHASE 3 ########### 
				if(l_isTileOnQuickMergeMagnet &&m_tiles[idx][idx2].isAtribNone()) 
				{ 
					SetState(state.PLAY); 
					QuickMergeGroup(idx2,idx); 
				    m_tiles[idx][idx2].MarkAsMerged();  

					SaveHistory();
					UpdateLineAdjacent();
					updatePlayStatus(); 
					break;
				}
				
				// ###########  QUICK MERGE PHASE 3 ########### 
				
			}
		}
	}
	private void TileUpdates(float p_deltaTime)
	{ 
		boolean onAnimation = false;
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{
				m_tiles[idx][idx2].Updates(p_deltaTime); 
				
				if(m_tiles[idx][idx2].isOnAnim())
				{
					onAnimation=true;
				}
			}
		}
		
		if( m_state==state.PASSING   && onAnimation==false)
		{
			switch(m_state)
			{ 
				case PASSING:
					SetState(state.PLAY);
					m_tiles[m_passTargetPt.y][m_passTargetPt.x].addValue(m_passValOnTarget);  
					m_tiles[m_passTargetPt.y][m_passTargetPt.x].SetState(Tile.state.NONE);// STATE CHANGE INSTANCE 1 - Close
					int l_remainingTile = countSameTiles(m_tiles[m_passTargetPt.y][m_passTargetPt.x].getTileType(),false );
					int l_adjacentTile  = countAdjacent(m_passTargetPt.y,m_passTargetPt.x);  
					if(m_continuesPass)
					{
						 if(l_remainingTile>1||l_adjacentTile==1)
						{
							PassAdjacentSameColor(m_passTargetPt.y, m_passTargetPt.x);
							SetState(state.PASSING);  
							
							l_adjacentTile  = countAdjacent(m_passTargetPt.y,m_passTargetPt.x);  
							if(l_adjacentTile==0) 
							{  
								m_continuesPass=false; 
								m_tiles[m_passTargetPt.y][m_passTargetPt.x].expandOut(0.08f,0.1f);  //EXPAND ANIM End - A
								m_tiles[m_passTargetPt.y][m_passTargetPt.x].MarkAsMerged();
								SaveHistory(); 
							}
							 
						} 
						else
						{     
							m_continuesPass=false; 
							m_tiles[m_passTargetPt.y][m_passTargetPt.x].expandOut(0.08f,0.1f);  //EXPAND ANIM End - A
							m_tiles[m_passTargetPt.y][m_passTargetPt.x].MarkAsMerged();
							SaveHistory(); 
						}
					}
					else if(l_remainingTile==1)
					{  
						m_tiles[m_passTargetPt.y][m_passTargetPt.x].expandOut(0.08f,0.1f);   //EXPAND ANIM End - A
						if(m_tiles[m_passTargetPt.y][m_passTargetPt.x].isMerged()==false)
						{
							SaveHistory();
						} 
						m_tiles[m_passTargetPt.y][m_passTargetPt.x].MarkAsMerged();  
 
					}
					else 
					{  
						m_tiles[m_passTargetPt.y][m_passTargetPt.x].expandOut(0.08f,0.1f);  //EXPAND ANIM End - A
						if(m_tiles[m_passTargetPt.y][m_passTargetPt.x].isMerged()==false)
						{
							SaveHistory();
						} 
					}

					UpdateLineAdjacent();
					break;
			default:
				break;
			}  
		    
			updatePlayStatus(); 
		} 
	}

	private void SetTileBackRect(int p_tileSize) 
	{
		
		switch (p_tileSize) {
		case 3:
			TILE_BACK_RECT = new Rect(
					  Constants.TILES_MARGIN_X
					, Constants.TILES_MARGIN_Y-Constants.ScaleY(80)
					, Constants.TILES_MARGIN_X+CONFIG.SCREEN_WIDTH-Constants.TILES_MARGIN_X*2
					, Constants.TILES_MARGIN_Y-Constants.ScaleY(80)+CONFIG.SCREEN_HEIGHT-Constants.TILES_MARGIN_Y*2+Constants.ScaleY(115)
				);
			break;
		case 4:
			TILE_BACK_RECT = new Rect(
					  Constants.TILES_MARGIN_X-Constants.ScaleX(15)
					, Constants.TILES_MARGIN_Y-Constants.ScaleY(65)
					, Constants.TILES_MARGIN_X-Constants.ScaleX(10)+CONFIG.SCREEN_WIDTH-Constants.TILES_MARGIN_X*2+Constants.ScaleX(30)
					, Constants.TILES_MARGIN_Y-Constants.ScaleY(65)+CONFIG.SCREEN_HEIGHT-Constants.TILES_MARGIN_Y*2+Constants.ScaleY(165)
				);
			break;
		case 5:
			TILE_BACK_RECT = new Rect(
					  Constants.TILES_MARGIN_X-Constants.ScaleX(15)
					, Constants.TILES_MARGIN_Y-Constants.ScaleY(105)
					, Constants.TILES_MARGIN_X-Constants.ScaleX(15)+CONFIG.SCREEN_WIDTH-Constants.TILES_MARGIN_X*2+Constants.ScaleX(30)
					, Constants.TILES_MARGIN_Y-Constants.ScaleY(100)+CONFIG.SCREEN_HEIGHT-Constants.TILES_MARGIN_Y*2+Constants.ScaleY(160)
				);
			break;

		default:
			break;
		}
	}

	private void ExitPlayTouchUpdates(TouchEvent g)
	{

		if(m_askWindow.TouchUpdatesRet(g))
		{
			GoToLevelSelect();
		}
	}
	
	private void PlayTouchUpdates(TouchEvent g)
	{
		TileTouchUpdates(g);  
		if(!Debugger.PROD_MODE)
		{
			debugPlayBtns(g);
		}
		if(g.type == TouchEvent.TOUCH_DOWN)
		{ 

			drawRoundRect(g, -200,0, Constants.UNDO_BTN, Color.BLACK, Color.BLACK, QUICK_MERGE_BTN);
//			AnimateEntry();
			if(m_restartVisib) 
			{
				drawRoundRect(g,  Constants.RESTART_PLAY,  Color.BLACK,Color.BLACK, RESTART_BTN); 
			}
			if(m_undoVisib)
			{
				drawRoundRect(g, Constants.UNDO_BTN, Color.BLACK, Color.BLACK, UNDO_BTN);
			}
			if(m_quickMergeVisib)
			{ 
				drawRoundRect(g, Constants.QUICK_MERGE_BTN, Color.BLACK, Color.BLACK, QUICK_MERGE_BTN);
			}
		} 
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
			case RESTART_BTN: Restart(true); break;
			case UNDO_BTN: Undo(); break;
			case QUICK_MERGE_BTN:  		QuickMerge();  	break;
			}
			buttonPressed =0;
		 } 
	} 
	
	private void QuickMergeTouchUpdate(TouchEvent g)
	{
		QuickMergeTileTouchUpdates(g);   
		
		if(g.type == TouchEvent.TOUCH_DOWN)
		{ 
			drawRoundRect(g, CANCEL_QUICK_MERGE.x,CANCEL_QUICK_MERGE.y,Constants.CANCEL_MERGE_BTN, Color.BLACK, Color.BLACK, CANCEL_QUICK_MERGE_BTN); 
		} 
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
			case CANCEL_QUICK_MERGE_BTN: SetState(state.PLAY);break; 
			}
			buttonPressed =0;
		 } 
	} 
	
	private void QuickMergeTileTouchUpdates(TouchEvent g) 
	{  
		
		for(int idxY=0;idxY<m_tiles.length;idxY++)
		{ 
			for(int idxX=0;idxX<m_tiles[0].length;idxX++)
			{
				if(m_tiles[idxY][idxX].isPressed(g))
				{  
					if(m_tiles[idxY][idxX].isMerged())
					{
						genSet.ShowToast("Tile already merge, pick a different tile.");
					}
					else 
					{
						QuickMerge(idxX,idxY);
						m_quickMergeAvailable = GameScreen.UseQuickMerge(); 	
					}
					break;
				}
			}
		}  
	}
	private Point m_quickMergeMain;

	/*
	 * QuickMerge and QuickMergeGroup are mandatory for the whole QUICK MERGE FUNCTIONALITY
	 * 
	 * */
	private void QuickMerge(int p_x,int p_y) 
	{ 
		m_tiles[p_y][p_x].QuickMerge();  
		m_quickMergeMain = new Point(p_x,p_y);
		SetState(state.QUICK_MERGE_ANIM);  
	}

	private void QuickMergeAnimateGroup(int p_x,int p_y) 
	{ 
		int MOVE_SPEED = 10;
//		int MOVE_SPEED = 100;
		Vector<Point> l_tiles = GetGroup(p_y,p_x);  

		for(int idxX=0;idxX<l_tiles.size();idxX++)
		{
			if((p_x==l_tiles.get(idxX).x&&p_y==l_tiles.get(idxX).y)==false)
			{ 
				m_tiles[l_tiles.get(idxX).y][l_tiles.get(idxX).x].AnimateQuickMerger(m_tiles[p_y][p_x].getScreenPos(),MOVE_SPEED);
			}
		}
	}
	
	private void QuickMergeGroup(int p_x,int p_y) 
	{  
		Vector<Point> l_tiles = GetGroup(p_y,p_x);  
//		genSet.ShowToast(l_tiles.size()+"");
		for(int idxX=0;idxX<l_tiles.size();idxX++)
		{
			if(((p_x==l_tiles.get(idxX).x)&&(p_y==l_tiles.get(idxX).y))==false)
			{
				m_tiles[p_y][p_x].addValue(m_tiles[l_tiles.get(idxX).y][l_tiles.get(idxX).x].getValue());
//				m_tiles[l_tiles.get(idxX).y][l_tiles.get(idxX).x].SetState(Tile.state.NONE);
				m_tiles[l_tiles.get(idxX).y][l_tiles.get(idxX).x].MarkAsMerged();
				m_tiles[l_tiles.get(idxX).y][l_tiles.get(idxX).x].SetValue(0); 
			}
		}
	}
	 
	private void QuickMerge() 
	{   
		if(CanQuickMerge())
		{ 
			 SetState(state.QUICK_MERGE_SELECT);
		}
		else if(m_quickMergeAvailable==0)
		{  
			AssetManager.btn_1.play(); 
			 GoToPurchQuickMerge();
		} 	
	}
	private void Undo() 
	{  

			if(m_undoAvailable>0&&CanUndo())
			{ 
				AssetManager.undo.play(); 
				m_undoAvailable = GameScreen.UseUndo();
				
				m_tileHistCtr--; 
				for(int idx=0;idx<m_tiles.length;idx++)
				{ 
					for(int idx2=0;idx2<m_tiles[0].length;idx2++)
					{
						m_tiles[idx][idx2].SetValue(m_tilesValueHistory.get(m_tileHistCtr-1)[idx][idx2]);
						if(m_tiles[idx][idx2].isMerged()&&m_tileMergeStatus.get(m_tileHistCtr-1)[idx][idx2]==false) 
						{
							m_tiles[idx][idx2].DemarkAsMerged();
						}
					}
				}  
				m_tilesValueHistory.remove(m_tilesValueHistory.size()-1);  
				m_tileMergeStatus.remove(m_tileMergeStatus.size()-1);  
				UpdateLineAdjacent();   
			}
			else if(m_undoAvailable==0)
			{ 

				AssetManager.btn_1.play();
				GoToPurchUndo();
			} 	
	}

	private boolean CanQuickMerge() 
	{
		return m_quickMergeAvailable>0;
	}
	private boolean CanUndo() 
	{
		return m_tileHistCtr>1;
	}
	
	private void SaveHistory() 
	{
		int [][] l_tilevalues = new int[m_tiles.length][m_tiles[0].length];
		boolean [][] l_tileMergedStatus = new boolean[m_tiles.length][m_tiles[0].length];
		
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{
				l_tilevalues[idx][idx2]       = m_tiles[idx][idx2].getValue();
				l_tileMergedStatus[idx][idx2] = m_tiles[idx][idx2].isMerged();
			}
		}
		m_tilesValueHistory.add(l_tilevalues);
		m_tileMergeStatus.add(l_tileMergedStatus);
		m_tileHistCtr++; 
		
		m_moveCtr++;
	}
 
	private int[] GetTileAndGroupCount()
	{  
		/*
		 * Get number of tiles and group of tiles present
		 * 
		 * Return array detail:
		 *  0 - Tiles
		 *  1 - Groups
		 *  
		 * */

		int l_nTiles = 0;
		int l_nGroup = 0;

		m_leftBehindTiles = new Vector<Point>();
		boolean l_isGrouped[][] = new boolean[m_tiles.length][m_tiles[0].length];
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{
				if(m_tiles[idx][idx2].isActive())
				{
					l_nTiles++;
					if(l_isGrouped[idx][idx2]==false)
					{
						
						Vector<Point> l_group = GetGroup(idx,idx2); 
						boolean group_counted = true;
						for(int idx3=0;idx3<l_group.size();idx3++)
						{
							if(l_isGrouped[l_group.get(idx3).y][l_group.get(idx3).x]==false)
							{
								l_isGrouped[l_group.get(idx3).y][l_group.get(idx3).x] = true;
								group_counted=false; 
							}
						} 
						
						if(group_counted==false || l_group.size()==0)
						{ 
							l_nGroup++;
							
							if(l_group.size()==0)
							{
								l_isGrouped[idx][idx2]=true; 
								m_leftBehindTiles.add(new Point(idx,idx2));
							}
						}
					}
				} 
			}
		}
		
		return new int[]{l_nTiles, l_nGroup};
	}
	
	private void TileTouchUpdates(TouchEvent g) {  
		
		for(int idxY=0;idxY<m_tiles.length;idxY++)
		{ 
			for(int idxX=0;idxX<m_tiles[0].length;idxX++)
			{
				if(m_tiles[idxY][idxX].isPressed(g))
				{
					MergeSelected(idxX,idxY);
//					m_tiles[idxY][idxX].animateGotCoin(1);
					break;
				}
			}
		}  
	}
	 
	
	private void MergeSelected(int idxX, int idxY) {
		
		int l_sameTypeCtr = countSameTiles(m_tiles[idxY][idxX].getTileType(), false); 
		int l_adjacents   = countAdjacent(idxY,idxX); 	
		if(l_sameTypeCtr-1==l_adjacents)
		{
//			genSet.ShowToast("LAST BB1");
		}
		
//		if(l_adjacents==1&&l_sameTypeCtr==1)
//		{ 
//			PassAdjacentSameColor(idxY,idxX); 
//			SetState(state.PASSING);
//			AssetManager.tile_merging.play(0.8f);
//			m_continuesPass=true;  
//		}
//		else
		if(l_adjacents==1) 
		{
			// Kapag isa lang adjacent, aalamin kung dalawa na lang sila, para sa m_continuesPass else normal na passing lang
			Point l_oneAdjacent = getAdjacent(idxY,idxX).get(0);
			if(countAdjacent(l_oneAdjacent.y,l_oneAdjacent.x)==1) 
			{
				PassAdjacentSameColor(idxY,idxX); 
				SetState(state.PASSING);
				AssetManager.tile_merging.play(0.8f);
				m_continuesPass=true;  
			}
			else 
			{ 
				PassAdjacentSameColor(idxY,idxX); 
				SetState(state.PASSING);
				AssetManager.tile_merging.play(0.8f); 
			}
		}
		else if (l_adjacents!=0)
		{
			PassAdjacentSameColor(idxY,idxX); 
			SetState(state.PASSING);
			AssetManager.tile_merging.play(0.8f); 
		}
//		else 
//		{
//			m_tiles[idxY][idxY].Shine();
//		}

	}

	private boolean[] GetAdjacent(int p_y, int p_x)
	{
		/*
		 *  A1-A2-A3
		 *  B1-B2-B3
		 *  C1-C2-C3
		 * */
		/*
		 *  X0-X1-X2
		 *  X3-  -X4
		 *  X5-X6-X7
		 * */
		boolean l_adjacents[] = new boolean[9]; 
		int l_targetColor = m_tiles[p_y][p_x].getTileType(); 
		
		if(p_y-1>=0)
		{ 
			if(p_x-1>=0) 
			{
				if(l_targetColor == m_tiles[p_y-1][p_x-1].getTileType())
				{
					//A1 - X0  
					l_adjacents[0] = true;
				}
			}

			if(l_targetColor == m_tiles[p_y-1][p_x].getTileType())
			{
				//A2 - X1 
				l_adjacents[1] = true;
			}
			
			if(p_x+1<m_tiles[0].length)
			{
				if(l_targetColor == m_tiles[p_y-1][p_x+1].getTileType())
				{
					//A3 - X2 
					l_adjacents[2] = true;
				}
			}
		}

		if(p_x-1>=0)
		{
			if(l_targetColor == m_tiles[p_y][p_x-1].getTileType())
			{
				//B1 - X3 
				l_adjacents[3] = true;
			}
		}
		 
				
		if(p_x+1<m_tiles[0].length)
		{
			if(l_targetColor == m_tiles[p_y][p_x+1].getTileType())
			{
				//B3 - X4 
				l_adjacents[4] = true;
			}
		}
		
		if(p_y+1<m_tiles.length)
		{ 
			if(p_x-1>=0)
			{
				if(l_targetColor == m_tiles[p_y+1][p_x-1].getTileType())
				{
					//C1 - X5 
					l_adjacents[5] = true;
				}
			}

			if(l_targetColor == m_tiles[p_y+1][p_x].getTileType())
			{
				//C2 - X6 
				l_adjacents[6] = true;
			}
				
			if(p_x+1<m_tiles[0].length)
			{
				if(l_targetColor == m_tiles[p_y+1][p_x+1].getTileType())
				{
					//C3 - X7 
				    l_adjacents[7] = true;
				 }
			}
			
		}
		return l_adjacents;
	} 
	
	private Vector<Point> getAdjacent(int p_y, int p_x)
	{
		Vector<Point> l_adjacents = new Vector<Point>();
		/*
		 *  A1-A2-A3
		 *  B1-B2-B3
		 *  C1-C2-C3
		 * */
		/*
		 *  X0-X1-X2
		 *  X3-  -X4
		 *  X5-X6-X7
		 * */  
		int l_targetColor = m_tiles[p_y][p_x].getTileType(); 
		
		if(p_y-1>=0)
		{ 
			if(p_x-1>=0) 
			{
				if(l_targetColor == m_tiles[p_y-1][p_x-1].getTileType())
				{
					//A1 - X0 
					l_adjacents.add(new Point(p_x-1,p_y-1));
				}
			}

			if(l_targetColor == m_tiles[p_y-1][p_x].getTileType())
			{
				//A2 - X1 
				l_adjacents.add(new Point(p_x,p_y-1));
			}
			
			if(p_x+1<m_tiles[0].length)
			{
				if(l_targetColor == m_tiles[p_y-1][p_x+1].getTileType())
				{
					//A3 - X2
					l_adjacents.add(new Point(p_x+1,p_y-1));
				}
			}
		}

		if(p_x-1>=0)
		{
			if(l_targetColor == m_tiles[p_y][p_x-1].getTileType())
			{
				//B1 - X3
				l_adjacents.add(new Point(p_x-1,p_y));
			}
		}
		 
				
		if(p_x+1<m_tiles[0].length)
		{
			if(l_targetColor == m_tiles[p_y][p_x+1].getTileType())
			{
				//B3 - X4
				l_adjacents.add(new Point(p_x+1,p_y));
			}
		}
		
		if(p_y+1<m_tiles.length)
		{ 
			if(p_x-1>=0)
			{
				if(l_targetColor == m_tiles[p_y+1][p_x-1].getTileType())
				{
					//C1 - X5
					l_adjacents.add(new Point(p_x-1,p_y+1)); 
				}
			}

			if(l_targetColor == m_tiles[p_y+1][p_x].getTileType())
			{
				//C2 - X6
				l_adjacents.add(new Point(p_x,p_y+1));
			}
				
			if(p_x+1<m_tiles[0].length)
			{
				if(l_targetColor == m_tiles[p_y+1][p_x+1].getTileType())
				{
					//C3 - X7
					l_adjacents.add(new Point(p_x+1,p_y+1));
				 }
			}
			
		}
		return l_adjacents;
	} 
	
	private int countAdjacent(int p_y, int p_x)
	{
		/*
		 *  A1-A2-A3
		 *  B1-B2-B3
		 *  C1-C2-C3
		 * */
		/*
		 *  X0-X1-X2
		 *  X3-  -X4
		 *  X5-X6-X7
		 * */ 
		int l_adjacent =0;   
		int l_targetColor = m_tiles[p_y][p_x].getTileType(); 
		
		if(p_y-1>=0)
		{ 
			if(p_x-1>=0) 
			{
				if(l_targetColor == m_tiles[p_y-1][p_x-1].getTileType())
				{
					//A1 - X0 
					l_adjacent++; 
				}
			}

			if(l_targetColor == m_tiles[p_y-1][p_x].getTileType())
			{
				//A2 - X1
				l_adjacent++;  
			}
			
			if(p_x+1<m_tiles[0].length)
			{
				if(l_targetColor == m_tiles[p_y-1][p_x+1].getTileType())
				{
					//A3 - X2
					l_adjacent++; 
				}
			}
		}

		if(p_x-1>=0)
		{
			if(l_targetColor == m_tiles[p_y][p_x-1].getTileType())
			{
				//B1 - X3
				l_adjacent++;    
			}
		}
		 
				
		if(p_x+1<m_tiles[0].length)
		{
			if(l_targetColor == m_tiles[p_y][p_x+1].getTileType())
			{
				//B3 - X4
				l_adjacent++;   
			}
		}
		
		if(p_y+1<m_tiles.length)
		{ 
			if(p_x-1>=0)
			{
				if(l_targetColor == m_tiles[p_y+1][p_x-1].getTileType())
				{
					//C1 - X5
					l_adjacent++;  
				}
			}

			if(l_targetColor == m_tiles[p_y+1][p_x].getTileType())
			{
				//C2 - X6
				l_adjacent++;  
			}
				
			if(p_x+1<m_tiles[0].length)
			{
				if(l_targetColor == m_tiles[p_y+1][p_x+1].getTileType())
				{
					//C3 - X7
					l_adjacent++;  
				 }
			}
			
		}
		return l_adjacent;
	} 
	
	private void PassAdjacentSameColor(int p_y, int p_x)
	{
		/*
		 *  A1-A2-A3
		 *  B1-B2-B3
		 *  C1-C2-C3
		 * */
		m_passTargetPt = new Point(p_x, p_y);
		m_passValOnTarget= 0;
		m_tiles[p_y][p_x].expandIn(0.05f,0.1f);   //EXPAND ANIM Start - A

		m_tiles[p_y][p_x].SetState(Tile.state.MERGING); // STATE CHANGE INSTANCE 1 - Open
		int l_targetColor = m_tiles[p_y][p_x].getTileType();
		Point l_passPoint = m_tiles[p_y][p_x].getScreenPos(); 
		if(p_y-1>=0)
		{ 
			if(p_x-1>=0) 
				PassValueIfSameColor(new Point(p_x-1,p_y-1),l_targetColor, l_passPoint); //A1 
			
			PassValueIfSameColor(new Point(p_x,p_y-1),l_targetColor, l_passPoint); //A2 
			
			if(p_x+1<m_tiles[0].length)
				PassValueIfSameColor(new Point(p_x+1,p_y-1),l_targetColor, l_passPoint); //A3 
		}

		if(p_x-1>=0)
			PassValueIfSameColor(new Point(p_x-1,p_y),l_targetColor, l_passPoint); //B1    
		
//		m_tiles[p_y][p_x].PassValueIfSameColor(l_passPoint, l_targetColor); //B2 
				
		if(p_x+1<m_tiles[0].length)
			PassValueIfSameColor(new Point(p_x+1,p_y),l_targetColor, l_passPoint); //B3    
		 
		if(p_y+1<m_tiles.length)
		{ 
			if(p_x-1>=0)
				PassValueIfSameColor(new Point(p_x-1,p_y+1),l_targetColor, l_passPoint); //C1 
			 
			PassValueIfSameColor(new Point(p_x,p_y+1),l_targetColor, l_passPoint); //C2
				
			if(p_x+1<m_tiles[0].length)
				PassValueIfSameColor(new Point(p_x+1,p_y+1),l_targetColor, l_passPoint); //C3
		}
	} 
	
	private void PassValueIfSameColor(Point p_targetPt, int p_targetColor, Point p_passPoint)
	{  
		if(p_targetColor == m_tiles[p_targetPt.y][p_targetPt.x].getTileType())
		{
			m_tiles[p_targetPt.y][p_targetPt.x].PassValue(p_passPoint,Constants.PASS_SPEED);
			m_passValOnTarget++;
		}
	} 
 	
	private Vector<Point> GetGroup(int p_y, int p_x)
	{  
		Vector<Point> l_group = GetAdjacentPoints(p_x,p_y);
		
		for(int idx=0;idx<l_group.size();idx++)
		{ 
			Vector<Point> l_adjacent = GetAdjacentPoints(l_group.get(idx).x,l_group.get(idx).y);
		 	
			for(int idx2=0;idx2<l_adjacent.size();idx2++)
			{ 
				boolean l_exist = false;
				for(int idx3=0;idx3<l_group.size();idx3++)
				{ 
					if(l_group.get(idx3).x==l_adjacent.get(idx2).x
				     &&l_group.get(idx3).y==l_adjacent.get(idx2).y)
					{
						l_exist=true;
						break;
					}
				}

				if(l_exist==false)
					l_group.add(l_adjacent.get(idx2));
			}
		}
		
		return l_group; 
	} 
	
	private Vector<Point> GetAdjacentScreenPoints(int p_x, int p_y)
	{
		Vector<Point> l_adjacent = new Vector<Point>();
		/*
		 *  A1-A2-A3
		 *  B1-B2-B3
		 *  C1-C2-C3
		 * */
		int l_tiletype = m_tiles[p_y][p_x].getTileType();
		if(l_tiletype==ColorManager.EMPTY_TILE_COLOR)return l_adjacent;
		if(p_y-1>=0)
		{ 
			if(p_x-1>=0)
				if(m_tiles[p_y-1][p_x-1].getTileType() == l_tiletype)
					l_adjacent.add(new Point(m_tiles[p_y-1][p_x-1].getScreenPosMid()));//A1 

			if(m_tiles[p_y-1][p_x].getTileType() == l_tiletype)
				l_adjacent.add(new Point(m_tiles[p_y-1][p_x].getScreenPosMid()));//A2  
 		
 			
			if(p_x+1<m_tiles[0].length) 
				if(m_tiles[p_y-1][p_x+1].getTileType() == l_tiletype) 
					l_adjacent.add(new Point(m_tiles[p_y-1][p_x+1].getScreenPosMid()));//A3 
		}

		if(p_x-1>=0)
		{
			if(m_tiles[p_y][p_x-1].getTileType() == l_tiletype)
			{
				l_adjacent.add(new Point(m_tiles[p_y][p_x-1].getScreenPosMid()));//B1 
			}
		}
 
		if(p_x+1<m_tiles[0].length)
		{
			if(m_tiles[p_y][p_x+1].getTileType() == l_tiletype) 
			{
				l_adjacent.add(new Point(m_tiles[p_y][p_x+1].getScreenPosMid()));//B3
			}
		}
		
		
		if(p_y+1<m_tiles.length)
		{  
			if(p_x-1>=0)
			{
				if(m_tiles[p_y+1][p_x-1].getTileType() == l_tiletype)
					l_adjacent.add(new Point(m_tiles[p_y+1][p_x-1].getScreenPosMid()));//C1 
			}

			if(m_tiles[p_y+1][p_x].getTileType() == l_tiletype)
			{
				l_adjacent.add(new Point(m_tiles[p_y+1][p_x].getScreenPosMid()));//C1 
			}
//				
			if(p_x+1<m_tiles[0].length)
			{
				if(m_tiles[p_y+1][p_x+1].getTileType() == l_tiletype)
					l_adjacent.add(new Point(m_tiles[p_y+1][p_x+1].getScreenPosMid()));//C1 
			}
		}
////		 
		return l_adjacent;
	} 
	
	private Vector<Point> GetAdjacentPoints(int p_x, int p_y)
	{
		Vector<Point> l_adjacent = new Vector<Point>();
		/*
		 *  A1-A2-A3
		 *  B1-B2-B3
		 *  C1-C2-C3
		 * */
		int l_tiletype = m_tiles[p_y][p_x].getTileType();
		if(p_y-1>=0)
		{ 
			if(p_x-1>=0)
				if(m_tiles[p_y-1][p_x-1].getTileType() == l_tiletype)
					l_adjacent.add(new Point(p_x-1,p_y-1));//A1 

			if(m_tiles[p_y-1][p_x].getTileType() == l_tiletype)
				l_adjacent.add(new Point(p_x,p_y-1));//A2
 		
 			
			if(p_x+1<m_tiles[0].length) 
				if(m_tiles[p_y-1][p_x+1].getTileType() == l_tiletype) 
					l_adjacent.add(new Point(p_x+1,p_y-1));//A3  
		}

		if(p_x-1>=0)
			if(m_tiles[p_y][p_x-1].getTileType() == l_tiletype)
				l_adjacent.add(new Point(p_x-1,p_y));//B1
 
		if(p_x+1<m_tiles[0].length)
			if(m_tiles[p_y][p_x+1].getTileType() == l_tiletype) 
				l_adjacent.add(new Point(p_x+1,p_y));//B3 
		
		
		if(p_y+1<m_tiles.length)
		{  
			if(p_x-1>=0)
				if(m_tiles[p_y+1][p_x-1].getTileType() == l_tiletype)
					l_adjacent.add(new Point(p_x-1,p_y+1));//C1 

			if(m_tiles[p_y+1][p_x].getTileType() == l_tiletype)
				l_adjacent.add(new Point(p_x,p_y+1));//C2 
//				
			if(p_x+1<m_tiles[0].length)
				if(m_tiles[p_y+1][p_x+1].getTileType() == l_tiletype)
					l_adjacent.add(new Point(p_x+1,p_y+1));//C3  
		}
		return l_adjacent;
	} 
	
	public void UpdateLineAdjacent()
	{  
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{ 
				m_tiles[idx][idx2].SetAdjacentPoints(GetAdjacentScreenPoints(idx2, idx));
			}
		}
	}
		 
	public void AnimateEntry(boolean p_hasFade)
	{ 
		SetState(state.ENTRY_ANIM);
		AssetManager.restart.play();
		for(int idx=0;idx<m_tiles.length;idx++)
		{ 
			for(int idx2=0;idx2<m_tiles[0].length;idx2++)
			{ 
				m_tiles[idx][idx2].animateEntry_1();
			}
		}  

		m_anim[1] = p_hasFade?0:1; 
	}
	
	private void AskToExit()
	{
		SetState(state.EXIT_PLAY);
		m_askWindow = new AskUserWindow(QUESTION.EXIT_PLAY);
		AssetManager.ask_exit_popup.play();
	}
  
	void GoToPurchaseSuccessNotif(VideoAdType p_vidAdType)
	{
		SetState(state.PURCH_SUCCESS);
		switch(p_vidAdType) 
		{
			case RewardToAddUndo:  	m_askWindow = new AskUserWindow(QUESTION.PURCH_SUCCESS_UNDO); break;
			case RewardToAddQMerge: m_askWindow = new AskUserWindow(QUESTION.PURCH_SUCCESS_QMERGE);  break;
		}
		m_undoAvailable = GameScreen.GetUndoAvailable();
		m_quickMergeAvailable = GameScreen.GetQuickMergeAvailable();
		UpdateCoins();
	}
	
	private void AskToRate()
	{
		SetState(state.ASK_TO_RATE);
		m_askWindow = new AskUserWindow(QUESTION.RATE_US);
	}
	 
	@Override
	protected void backButton() {
		switch(m_state)
		{
		    case PASSING: break;
			case PLAY:   AskToExit();  break;
			case EXIT_PLAY:      SetState(state.PLAY);break;
			case GAMEOVER:       m_gameOverWindow.backButton(); break;
			case LEVEL_COMPLETE:  m_levelCompleteWindow.backButton(); break;  
			case ASK_TO_RATE:     SetState(state.PLAY);break;
			case TO_COMPLETE:     break;
			case NO_TILE:  break; 
			case PLACEHOLDER:  break;
			case LEVEL_BAR_SHINE:  break;
			case EXIT_FADE:  break;
			case PURCH_UNDO:  m_undoPurchScreen.backButton(); break;
			case PURCH_QMERGE: m_quickMergePurchScreen.backButton();
			case PURCH_SUCCESS:  SetState(state.PLAY);break; 
			case QUICK_MERGE_SELECT: SetState(state.PLAY); break;

			default: 	break;
		} 
	}

	public Rect getTileBounds(int i, int j) { 
		return m_tiles[i][j].getBounds();
	}
 
}
