package com.eugexstudios.mergemaster;
 
 
public class Level {
	
	private Tile m_tiles[][]; 
	private int STAR_MIN_3;
	private int STAR_MIN_2; 
	private int TILE_SIZE;  
	public Level(Tile[][] p_tiles, int p_tileSize, int p_star_min_3, int p_star_min_2)
	{
		m_tiles  = p_tiles.clone(); 
		STAR_MIN_3 = p_star_min_3;
		STAR_MIN_2 = p_star_min_2; 
		TILE_SIZE = p_tileSize;
	}
	
	public Level(Tile[][] p_tiles)
	{
		m_tiles  = p_tiles.clone(); 
		STAR_MIN_3 = 0;
		STAR_MIN_2 = 0; 
	}
	
	public Tile[][] getTiles()
	{
		return m_tiles.clone();
	}
 
	public int getTileSize() 
	{
		return TILE_SIZE;
	}
	
	public int determineStar(int p_move) 
	{
		if(p_move<=STAR_MIN_3+2) 
		{
			return 3;
		}
		if(p_move<=STAR_MIN_2+5
				) 
		{
			return 2;
		}
		else return 1;
		
	}

	public int getStarMin_3() { return STAR_MIN_3;};
	public int getStarMin_2() { return STAR_MIN_2;}; 
	
	
}
