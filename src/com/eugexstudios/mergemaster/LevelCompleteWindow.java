package com.eugexstudios.mergemaster;

import android.graphics.Color;
import android.graphics.Rect;

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.ImageData;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.mergemaster.AskUserWindow.QUESTION;
import com.eugexstudios.utils.AssetManager;
import com.eugexstudios.utils.CONFIG; 
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Screen;
import com.eugexstudios.utils.genSet;

public class LevelCompleteWindow extends Screen{
	private enum state {
		IDLE
		,ASK_TO_EXIT
		,ENTRY_ANIM
		,PLACEHOLDER_3
		,PLACEHOLDER_4
		,PLACEHOLDER_5
		,PLACEHOLDER_6
		}; 

	private static final byte NEXT_LEVEL_BTN = 1; 
	private static final byte HOME_BTN = 2; 
	private static final byte RESTART_BTN = 3; 
	private static final byte SHARE_BTN = 4; 

	private final static int WINDOW_POS_X = Constants.ScaleX(30);
	private final static int WINDOW_POS_Y = Constants.ScaleY(330);
	private final static int WINDOW_HEIGHT = 6;
	private final static int STAR_RESULT_POS_X = CONFIG.SCREEN_MID-Constants.STAR_RESULT_3.width/2;
	private final static int STAR_RESULT_POS_Y = WINDOW_POS_Y+Constants.ScaleY(160);
	
	private static final float NEXT_LVL_BTN_X = CONFIG.SCREEN_MID-(Constants.NEXT_LEVEL_IMG.width/2)*CONFIG.X_SCALE;;
	private static final float NEXT_LVL_BTN_Y = WINDOW_POS_Y+ Constants.ScaleY(340);
	 
	private static final float RESTART_BTN_X = CONFIG.SCREEN_MID-(Constants.RESTART_BTN_IMG.width/2)*CONFIG.X_SCALE;
	private static final float BTNS_POS_Y = WINDOW_POS_Y+ Constants.ScaleY(560); 
	
	private static final float BTN_GAP    = Constants.ScaleX(0.02084f);
	private static final float HOME_BTN_X = RESTART_BTN_X-(Constants.HOME_BTN_IMG.width*CONFIG.X_SCALE)-BTN_GAP+Constants.ScaleX(10); 
	
	private static final float SHARE_BTN_X = RESTART_BTN_X+ (Constants.SHARE_BTN_IMG.width*CONFIG.X_SCALE)+BTN_GAP; 

	final int WINDOW_COLOR = Color.rgb(255, 245, 245);
	final int SHADOW = Color.argb(30, 0, 0, 0);
	
	final Rect WINDOW_RECT = new Rect(
			  Constants.ScaleX(0.1f)
			, Constants.ScaleY(0.28f)
			, Constants.ScaleX(0.1f)+ Constants.ScaleX(0.8f)
			, Constants.ScaleY(0.28f)+ Constants.ScaleY(0.54f)
			);
	final int DIM_COLOR = Color.argb(180, 0, 0, 0);

	private state m_state;
	private AskUserWindow m_askWindow; 
	private ImageData m_starResultImage;

	private int m_star;
	private int m_starShowAnimCtr;
	private float m_starShowAnimBuff;
	
	public LevelCompleteWindow(int p_star, boolean p_hasAnim)
	{ 
		m_star = p_star;
		m_starShowAnimBuff = 0;
		m_starShowAnimCtr  = 0;
		if(p_hasAnim) 
		{ 
			m_starShowAnimBuff = -10;
			m_state = state.ENTRY_ANIM; 
			m_starResultImage =  getStarImage(0);
		}
		else 
		{
			m_state = state.IDLE;
			m_starResultImage = getStarImage(p_star);
		} 
	}
	
	private ImageData getStarImage(int p_count) 
	{
		switch(p_count)
		{
			case 1: return Constants.STAR_RESULT_1;  
			case 2: return Constants.STAR_RESULT_2;  
			case 3: return Constants.STAR_RESULT_3;  
			default: return new ImageData(0, 0, 1, 1); // Technically none
		}
	}
	
	@Override
	protected void Paint(Graphics g) {
		switch(m_state)
		{
			case IDLE: 		  PaintIdle(g);    break;
			case ASK_TO_EXIT: PaintAskExit(g); break;
			case ENTRY_ANIM:  PaintIdle(g);  break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		} 
	}
	 
	private void PaintAskExit(Graphics g)
	{
		PaintIdle(g); 
		m_askWindow.Paint(g);
	}

	
	private void PaintIdle(Graphics g)
	{
		g.drawRect(Constants.SCREEN_RECT,DIM_COLOR); 
//		g.drawRoundRect(WINDOW_RECT.left+10,WINDOW_RECT.top+10,WINDOW_RECT.width(),WINDOW_RECT.height(), 30, 30, SHADOW);
//		g.drawRoundRect(WINDOW_RECT, 30, 30, WINDOW_COLOR);
//	    g.drawImage(AssetManager.sprites, 0,0,Constants.LEVEL_COMP_HEADER_2,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		 
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, WINDOW_POS_Y, Constants.WINDOW_1,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, WINDOW_POS_Y, Constants.LEVEL_COMPLETE_HEADER, CONFIG.X_SCALE, CONFIG.Y_SCALE);
		 

		g.drawImage(AssetManager.sprites, STAR_RESULT_POS_X, STAR_RESULT_POS_Y, Constants.STAR_RESULT_BACK);
		g.drawImage(AssetManager.sprites, STAR_RESULT_POS_X, STAR_RESULT_POS_Y, m_starResultImage);
		 

//	    drawRoundRect(g, (int)(NEXT_LVL_BTN_X+(buttonPressed==NEXT_LEVEL_BTN?-4:0)), (int)(NEXT_LVL_BTN_Y+(buttonPressed==NEXT_LEVEL_BTN?6:0)), Constants.NEXT_LEVEL_IMG, Color.BLACK,Color.BLACK,NEXT_LEVEL_BTN);
	    g.drawImage(AssetManager.sprites, (int)(NEXT_LVL_BTN_X+(buttonPressed==NEXT_LEVEL_BTN?-4:0)), (int)(NEXT_LVL_BTN_Y+(buttonPressed==NEXT_LEVEL_BTN?6:0)),Constants.NEXT_LEVEL_IMG,CONFIG.X_SCALE,CONFIG.Y_SCALE);

//	    drawRoundRect(g, (int)(HOME_BTN_X+(buttonPressed==HOME_BTN?-4:0)), (int)(BTNS_POS_Y+(buttonPressed==HOME_BTN?6:0)), Constants.HOME_BTN_IMG, Color.BLACK,Color.BLACK,HOME_BTN);
	    g.drawImage(AssetManager.sprites, (int)(HOME_BTN_X+(buttonPressed==HOME_BTN?-4:0)), (int)(BTNS_POS_Y+(buttonPressed==HOME_BTN?6:0)),Constants.HOME_BTN_IMG,CONFIG.X_SCALE,CONFIG.Y_SCALE);

//	    drawRoundRect(g, (int)(RESTART_BTN_X+(buttonPressed==RESTART_BTN?-4:0)), (int)(BTNS_POS_Y+(buttonPressed==RESTART_BTN?6:0)), Constants.RESTART_BTN_IMG, Color.BLACK,Color.BLACK,RESTART_BTN);
	    g.drawImage(AssetManager.sprites, (int)(RESTART_BTN_X+(buttonPressed==RESTART_BTN?-4:0)), (int)(BTNS_POS_Y+(buttonPressed==RESTART_BTN?6:0)), Constants.RESTART_BTN_IMG,CONFIG.X_SCALE,CONFIG.Y_SCALE);

//	    drawRoundRect(g, (int)(SHARE_BTN_X+(buttonPressed==SHARE_BTN?-4:0)), (int)(BTNS_POS_Y+(buttonPressed==SHARE_BTN?6:0)), Constants.SHARE_BTN_IMG, Color.BLACK,Color.BLACK,SHARE_BTN);
	    g.drawImage(AssetManager.sprites, (int)(SHARE_BTN_X+(buttonPressed==SHARE_BTN?-4:0)), (int)(BTNS_POS_Y+(buttonPressed==SHARE_BTN?6:0)),Constants.SHARE_BTN_IMG,CONFIG.X_SCALE,CONFIG.Y_SCALE);

//g.drawString(m_starShowAnimBuff+"", 200, 300,genSet.paint);

	} 
	 
	@Override
	protected void Updates(float p_deltaTime)
	{ 
		switch(m_state)
		{
			case IDLE: break;
			case ASK_TO_EXIT: 
				if(m_askWindow.isClosed())
				{
					m_state = state.IDLE;
				}
				break;
			case ENTRY_ANIM:EntryAnimUpdates(p_deltaTime); break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		}
		
	}

	@Override
	protected void TouchUpdates(TouchEvent g) {
		switch(m_state)
		{
			case IDLE:IdleTouchUpdates(g); break;
			case ASK_TO_EXIT: m_askWindow.TouchUpdates(g);break;
			case ENTRY_ANIM: EntryAnimTouchUpdates(g);break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break; 
		}
	} 
	
	private void EntryAnimUpdates(float p_deltaTime)
	{  
		m_starShowAnimBuff += p_deltaTime*1f;
		
		if((((int)m_starShowAnimBuff)%25)==0) 
		{
			m_starShowAnimCtr++;  
			if(m_starShowAnimCtr<m_star)
			{ 
				m_starResultImage = getStarImage(m_starShowAnimCtr);
				AssetManager.star_1.play();
			} 
			else 
			{
				m_state = state.IDLE;
				m_starResultImage = getStarImage(m_star); 
				
				if(m_starShowAnimCtr==3)
				{
					AssetManager.star_3.play();
				}
				else
				{
					AssetManager.star_1.play();
				}
				
			} 
		}
		
	}
	private void EntryAnimTouchUpdates(TouchEvent g)
	{
		if(g.type == TouchEvent.TOUCH_DOWN)
		{  
			interuptAnimation();
		}
		else if(g.type == TouchEvent.TOUCH_UP)
		{ 
			buttonPressed = 0;
		} 
	} 
	private void interuptAnimation()
	{
		m_state = state.IDLE;
		m_starResultImage = getStarImage(m_star);
	}
	
	private void IdleTouchUpdates(TouchEvent g)
	{
		if(g.type == TouchEvent.TOUCH_DOWN)
		{  

		    drawRoundRect(g, (int)(NEXT_LVL_BTN_X+(buttonPressed==NEXT_LEVEL_BTN?-4:0)), (int)(NEXT_LVL_BTN_Y+(buttonPressed==NEXT_LEVEL_BTN?6:0)), Constants.NEXT_LEVEL_IMG, Color.BLACK,Color.BLACK,NEXT_LEVEL_BTN);
		    drawRoundRect(g, (int)(HOME_BTN_X+(buttonPressed==HOME_BTN?-4:0)), (int)(BTNS_POS_Y+(buttonPressed==HOME_BTN?6:0)), Constants.HOME_BTN_IMG, Color.BLACK,Color.BLACK,HOME_BTN);
		    drawRoundRect(g, (int)(RESTART_BTN_X+(buttonPressed==RESTART_BTN?-4:0)), (int)(BTNS_POS_Y+(buttonPressed==RESTART_BTN?6:0)), Constants.RESTART_BTN_IMG, Color.BLACK,Color.BLACK,RESTART_BTN);
		    drawRoundRect(g, (int)(SHARE_BTN_X+(buttonPressed==SHARE_BTN?-4:0)), (int)(BTNS_POS_Y+(buttonPressed==SHARE_BTN?6:0)), Constants.SHARE_BTN_IMG, Color.BLACK,Color.BLACK,SHARE_BTN);
		}
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
			case NEXT_LEVEL_BTN: GameScreen.GoToNextLevel();break;
			case RESTART_BTN:    GameScreen.Restart();     break;
			case SHARE_BTN:    genSet.game.ShareApp();break;
			case HOME_BTN:   GameScreen.GoToMenu(true);break;
			}
			
			if(buttonPressed>0) 
			{
				AssetManager.btn_1.play();
			}
			buttonPressed = 0;
		} 
	} 
	private void AskToExit()
	{
		m_state = state.ASK_TO_EXIT; 
		m_askWindow = new AskUserWindow(QUESTION.GO_TO_MENU);
		AssetManager.ask_menu_popup.play();
	}
	@Override
	protected void backButton() {
		switch(m_state)
		{
			case IDLE:         AskToExit();break;
			case ASK_TO_EXIT:  m_state = state.IDLE;break;
			case ENTRY_ANIM: break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		}
		
	}

}
