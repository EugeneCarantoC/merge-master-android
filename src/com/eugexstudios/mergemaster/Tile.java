package com.eugexstudios.mergemaster;

import java.util.Vector;

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.ImageData;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.AssetManager;
import com.eugexstudios.utils.CONFIG;
import com.eugexstudios.utils.ColorManager; 
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.genSet;

import android.graphics.Color;
import android.graphics.Paint.Align;
import android.graphics.Point;
import android.graphics.Rect; 

public class Tile {

	public enum state{NONE,ANIM_PASS,ANIM_ENTRY_1,MERGED,SHOW_ADJACENT_IN,MERGING, MOVING,QUICK_MERGE_MAIN};
	private enum atrib{NONE,HIGHLIGHT,EXPAND_IN,EXPAND_OUT, EXPAND_IN_HOLD, SHINING,QUICK_MERGER,GOT_COIN};
	private enum anim_dir{TOP_LEFT, TOP_MID, TOP_RIGHT, LEFT_DIR, RIGHT_DIR, BOTTOM_LEFT, BOTTOM_MID, BOTTOM_RIGHT};
	private state m_state; ;
	private state m_last_state;
	private atrib m_atrib; 
	private atrib m_last_atrib; 
	private boolean m_isPressed; 

	private Point m_starImgPos;  
	private Point m_screenPos;  
	private Point m_screenPosMidle;  
	private int   m_val; 
	private int   m_width;
	private int   m_height;  
	private boolean m_active; 
 
	private Point m_passPoint;
	private float m_animPointX;
	private float m_animPointY;
	private float m_passSpeedX;
	private float m_passSpeedY; 
	private float m_passLimitX;
	private float m_passLimitY;  
	private float m_passBuffX;
	private float m_passBuffY; 
	private float m_anim[]; 
	private Vector<Point> m_adjacentPoints;
	private anim_dir m_animDir;
	
	private int         m_lineColor;
	private int         m_origTileType;
	private int         m_tileType;
	private int         m_tileSize;
	private ImageData   m_tileImage; 
	private ImageData   m_mergedImage;
	private ImageData   m_labelImg[];  

	private boolean m_promo=false;
	
	public Tile(Tile p_tile)
	{  
		m_screenPos = new Point(p_tile.getScreenPos());  
		SetTileType(p_tile.getTileType(), p_tile.getTileSize());
		m_val	    = p_tile.getValue();
		m_width	    = p_tile.getWidth();
		m_height    = p_tile.getHeight();
		SetState(state.NONE); 
		SetAtrib(atrib.NONE); 
		m_anim = new float[5];
		m_screenPosMidle = new Point(p_tile.getScreenPos());  
		m_screenPosMidle.offset(m_width/2, m_height/2);
		m_active    = true;   
		  
		m_starImgPos = new Point(
				(int)(m_screenPos.x + ((m_width-m_mergedImage.width*CONFIG.X_SCALE)/2) + Constants.ScaleX(0.0041666666666667f)) 
			  , (int)(m_screenPos.y + ((m_height-m_mergedImage.height*CONFIG.X_SCALE)/2))
			);
 
		updateLabelImage(); 
		
		if(m_val==0)
		{ 
			goneOutOfExistence();
		}
	} 	
	
	public Tile(Point p_screenPos, int p_tileType, int p_val, int p_width, int p_height, int p_tileSize)
	{  
		m_screenPos = new Point(p_screenPos);  
		m_val	    = p_val; 
		SetTileType(p_tileType,p_tileSize);
		m_width	    = p_width;
		m_height    = p_height;
		SetState(state.NONE);  
		SetAtrib(atrib.NONE);   
		m_anim = new float[5]; 
		m_screenPosMidle = new Point(p_screenPos);  
		m_screenPosMidle.offset(m_width, m_height);
		m_active     = true;

		
		m_starImgPos = new Point(
				(int)(m_screenPos.x + ((m_width-m_mergedImage.width*CONFIG.X_SCALE)/2) + Constants.ScaleX(0.0041666666666667f)) 
			  , (int)(m_screenPos.y + ((m_height-m_mergedImage.height*CONFIG.X_SCALE)/2))
			);  
		updateLabelImage(); 
		
		if(m_val==0)
		{  
			goneOutOfExistence();
		}
		
	} 	
	 
	private void SetTileType(int p_type, int p_tileSize) 
	{
		m_tileType = p_type;
		m_origTileType = p_type;
		m_tileSize = p_tileSize;
		
		 
		switch(m_tileSize) 
		{
			case 3:	m_mergedImage = Constants.TILE_STAR_1; break;
			case 4:	m_mergedImage = Constants.TILE_STAR_3; break;
			case 5:	m_mergedImage = Constants.TILE_STAR_4; break; 
		}
		
		if(m_promo) 
		{
			switch(m_tileSize) 
			{
				case 3:	m_mergedImage = Constants.PROMO_3; break;
				case 4:	m_mergedImage = Constants.PROMO_4; break;
				case 5:	m_mergedImage = Constants.PROMO_5; break; 
			} 
		}
		
		switch(m_tileType) 
		{	
			case 0:
				m_tileImage = new ImageData(0,  (m_tileSize-3)*200, 200,200);
				m_lineColor = Color.LTGRAY;
			break;
			case 111:
				m_tileImage = new ImageData(200*1,  (m_tileSize-3)*200, 200,200);
				m_lineColor = Color.rgb(212, 56, 56);
				break;
			case 222:
				m_tileImage = new ImageData(200*2, (m_tileSize-3)*200, 200, 200);
				m_lineColor = Color.rgb(225, 106, 48);
				break; 
			case 333:
				m_tileImage = new ImageData(200*3, (m_tileSize-3)*200, 200, 200);
				m_lineColor = Color.rgb(255, 199, 47);
				break; 
			case 444:
				m_tileImage = new ImageData(200*4, (m_tileSize-3)*200, 200, 200);
				m_lineColor = Color.rgb(117, 2019, 71);
				break; 
			case 555:
				m_tileImage = new ImageData(200*5, (m_tileSize-3)*200, 200, 200);
				m_lineColor = Color.rgb(50, 174, 65);
				break; 
			default:
				m_lineColor=p_type;
				break;
		}
	}
	
	public int GetTileSize()
	{
		return m_tileSize;
	}

	private void SetAtrib(atrib p_atrib) { 
		m_last_atrib = m_atrib;
		m_atrib  =p_atrib;
	}

	public void SetState(state p_state)
	{
		m_last_state = m_state;
		m_state = p_state;
	}
	
	public void Disconnect()
	{
		SetTileType(0,m_tileSize);
	}
	public void Shine()
	{
		SetAtrib(atrib.SHINING); 
		m_anim = new float[3];
		m_anim[0] = 0.008f;// anim speed
//		m_anim[0] = 0.25f;// anim speed
		m_anim[1] = 0;// Anim buffer
		m_anim[2] = 0;// current frame 
	}
	
	public void animateGotCoin(int p_coinQty) { 
		SetAtrib(atrib.GOT_COIN);
		m_anim = new float[6]; 
		m_anim[0] = 0;// Anim buffer
		m_anim[1] = 0.005f;// speed
		m_anim[2] = 0;// current frame 
		m_anim[3] = p_coinQty;// coin qty 
		m_anim[4] = Constants.ScaleY(200);// up limit px 
		m_anim[5] = 100;// pause duration
		
	}
	
	private void GotCoinUpdates(float p_deltaTime) 
	{
		m_anim[0] += p_deltaTime*m_anim[1];
		
		if(m_anim[2]==0) 
		{
			if(m_anim[0]>=(m_anim[4]))
			{
				m_anim[2]++;
				m_anim[0] = 0;
			}
		}
		else if(m_anim[2]==1)
		{
			if(m_anim[0]>=(m_anim[5]))
			{
				SetAtrib(atrib.NONE);
				
			}
		}
		
		
	}
	private void ShineUpdate(float p_deltaTime) 
	{
//		m_anim[0] // anim speed
//		m_anim[1] // Anim buffer
//		m_anim[2] // current frame 
		
		m_anim[1] += p_deltaTime*m_anim[0];
		
		if((int)(m_anim[1])%10==0) 
		{
			m_anim[2]+=1;		
		}
		
		if(m_anim[2]==23)//Frame limit
		{ 
			SetAtrib(atrib.NONE); 
		}
	}
	private void updateLabelImage() 
	{ 
		m_labelImg  = GetButtonData(m_val, m_screenPos.x, m_screenPos.y);
	} 
	
	private ImageData[] GetButtonData(int p_val, int p_x, int p_y)
	{ 

		int l_labelImgWidth = 100;
		int l_labelImgHeight = 100;
		int l_addX= (int)(m_width -l_labelImgWidth)/2;
		int l_addY= (int)(m_height-l_labelImgHeight )/2; 
		int l_passVal = 1;
		if(p_val<10)
		{
			return new ImageData[]
					{ 
					         new ImageData(l_addX+p_x, l_addY+p_y, 0, l_passVal*l_labelImgHeight, l_labelImgWidth, l_labelImgHeight) // Pass number
							,new ImageData(l_addX+p_x,l_addY+p_y,0,p_val*l_labelImgHeight,l_labelImgWidth,l_labelImgHeight)    //VALUE NUMBER DIGIT	
					};
		}
		else
		{
			return new ImageData[]
					{ 
				          new ImageData(l_addX-17,l_addY,0,l_passVal*l_labelImgHeight,l_labelImgWidth,l_labelImgHeight) // Pass number
						 ,new ImageData(l_addX+p_x-17,l_addY+p_y,0,(p_val/10)*100,105,108,CONFIG.X_SCALE,CONFIG.Y_SCALE)        //VALUE NUMBER DIGIT 1
						 ,new ImageData(l_addX+p_x+17,l_addY+p_y,0,(p_val%10)*100,105,108,CONFIG.X_SCALE,CONFIG.Y_SCALE)        //VALUE NUMBER DIGIT 2
					};
		}
	}

	private void PaintLabelAnim(Graphics g,int p_addX, int p_addY) 
	{
		switch(m_animDir) 
		{
			case TOP_LEFT:		PaintLabel(g,-m_passBuffX+p_addX,-m_passBuffY+p_addY); break;
			case TOP_MID:		PaintLabel(g,+m_passBuffX+p_addX,-m_passBuffY+p_addY); break;
			case TOP_RIGHT:		PaintLabel(g,+m_passBuffX+p_addX,-m_passBuffY+p_addY); break;
			case LEFT_DIR:		PaintLabel(g,-m_passBuffX+p_addX,+m_passBuffY+p_addY); break;
			case RIGHT_DIR:		PaintLabel(g,+m_passBuffX+p_addX,+m_passBuffY+p_addY); break;
			case BOTTOM_LEFT:	PaintLabel(g,-m_passBuffX+p_addX,+m_passBuffY+p_addY); break;
			case BOTTOM_MID:	PaintLabel(g,+m_passBuffX+p_addX,+m_passBuffY+p_addY); break;
			case BOTTOM_RIGHT:	PaintLabel(g,+m_passBuffX+p_addX,+m_passBuffY+p_addY); break; 
		} 
	}
	 
	
	private void PaintLabelAnimPass(Graphics g) 
	{
		switch(m_animDir) 
		{
			case TOP_LEFT:		PaintLabelPass(g,-m_passBuffX,-m_passBuffY); break;
			case TOP_MID:		PaintLabelPass(g,+m_passBuffX,-m_passBuffY); break;
			case TOP_RIGHT:		PaintLabelPass(g,+m_passBuffX,-m_passBuffY); break;
			case LEFT_DIR:		PaintLabelPass(g,-m_passBuffX,+m_passBuffY); break;
			case RIGHT_DIR:		PaintLabelPass(g,+m_passBuffX,+m_passBuffY); break;
			case BOTTOM_LEFT:	PaintLabelPass(g,-m_passBuffX,+m_passBuffY); break;
			case BOTTOM_MID:	PaintLabelPass(g,+m_passBuffX,+m_passBuffY); break;
			case BOTTOM_RIGHT:	PaintLabelPass(g,+m_passBuffX,+m_passBuffY); break; 
		} 
	}
	private void PaintLabelMoving(Graphics g) 
	{
		switch(m_animDir) 
		{
			case TOP_LEFT:		PaintLabelMoving(g,-m_passBuffX,-m_passBuffY); break;
			case TOP_MID:		PaintLabelMoving(g,+m_passBuffX,-m_passBuffY); break;
			case TOP_RIGHT:		PaintLabelMoving(g,+m_passBuffX,-m_passBuffY); break;
			case LEFT_DIR:		PaintLabelMoving(g,-m_passBuffX,+m_passBuffY); break;
			case RIGHT_DIR:		PaintLabelMoving(g,+m_passBuffX,+m_passBuffY); break;
			case BOTTOM_LEFT:	PaintLabelMoving(g,-m_passBuffX,+m_passBuffY); break;
			case BOTTOM_MID:	PaintLabelMoving(g,+m_passBuffX,+m_passBuffY); break;
			case BOTTOM_RIGHT:	PaintLabelMoving(g,+m_passBuffX,+m_passBuffY); break; 
		} 
	}
	
	private void PaintLabelPass(Graphics g, float p_addX, float p_addY) 
	{  
		g.drawImage(AssetManager.nums, (int)(p_addX),(int)(p_addY), m_labelImg[0], CONFIG.X_SCALE,CONFIG.Y_SCALE);
	} 
	
	private void PaintLabelMoving(Graphics g, float p_addX, float p_addY) 
	{    
		if(m_labelImg.length==2)
		{ 
			g.drawImage(AssetManager.nums,(int)(p_addX),(int)(p_addY), m_labelImg[1], CONFIG.X_SCALE,CONFIG.Y_SCALE);
		}
		else if(m_labelImg.length==3)
		{
			g.drawImage(AssetManager.nums, (int)(p_addX),(int)(p_addY), m_labelImg[1], CONFIG.X_SCALE,CONFIG.Y_SCALE);
			g.drawImage(AssetManager.nums, (int)(p_addX),(int)(p_addY), m_labelImg[2], CONFIG.X_SCALE,CONFIG.Y_SCALE);
		} 

	} 
	
	

	private void PaintLabel(Graphics g) 
	{ 
		PaintLabel(g,0,0);  
	}
	
	private void PaintLabel(Graphics g, float p_addX, float p_addY) 
	{ 
		PaintLabel(g, m_val, (int)p_addX, (int)p_addY);
	}
	
	
	public void paint_anim(Graphics g)
	{  

		if(m_active==false) return;
		switch(m_state)
		{ 
			
			case ANIM_ENTRY_1: 
				drawTileImage(g, m_animPointX, m_animPointY);   
				PaintLabelAnim(g, -m_screenPos.x + CONFIG.SCREEN_MID - m_width/2, CONFIG.SCREEN_HEIGHT/2+(int)((-m_screenPos.y)*CONFIG.Y_SCALE) -m_height);
				break;  
			case MOVING: 
				drawTileImage(g, m_animPointX, m_animPointY);   
				PaintLabelMoving(g);   
				break;
			case ANIM_PASS:    
				drawTileImage(g, m_animPointX, m_animPointY);     
				PaintLabelAnimPass(g);  
			break;
		default: 	break; 
		}   
		
		switch(m_atrib)
		{  
			case HIGHLIGHT: 
				if(m_anim[1]==1)
				{  
//					drawRoundRect(g, m_screenPos.x -8, m_screenPos.y -8, m_width+16, m_height+16, m_color, m_color);
					PaintLabel(g);
				}
			break;
			case EXPAND_IN_HOLD: 
			case EXPAND_IN: 
			case EXPAND_OUT:  
				/* 
				 * 0- expand scale buffer
				 * 1- expand speed
				 */

				drawTileImage(
							g 
						   ,(int)(m_screenPos.x - (((m_tileImage.width*(CONFIG.X_SCALE+m_anim[2]*m_anim[0]))-(m_tileImage.width*CONFIG.X_SCALE)))/4)
						   ,(int)(m_screenPos.y - (((m_tileImage.height*(CONFIG.Y_SCALE+m_anim[2]*m_anim[0]))-(m_tileImage.height*CONFIG.Y_SCALE)))/4)
						   , m_anim[2]*m_anim[0] 
						   , m_anim[2]*m_anim[0] 
				           );

				if(m_state==state.MERGED)
				{ 
					PaintMergeImage(g);
				} 
				else
				{ 
					PaintLabel(g);
				}
				
				break;
		default:
			break; 
		}   
	} 
	public void PaintMergeImage(Graphics g)
	{  
		g.drawImage(m_promo?AssetManager.promo:AssetManager.sprites
				, m_starImgPos.x
				, m_starImgPos.y
				, m_mergedImage
				, CONFIG.X_SCALE, CONFIG.Y_SCALE); 
	}
	public void paintAdjacents(Graphics g)
	{ 

		if(m_active==false) return;
			switch(m_state)
			{ 
			case SHOW_ADJACENT_IN:   
				for(int idx=0;idx<m_adjacentPoints.size();idx++)
				{
					g.drawLine(m_screenPosMidle, m_adjacentPoints.get(idx),m_lineColor,(int)m_anim[4]);    
				} 
				break;
			case ANIM_PASS:
				g.drawLine(
						 new Point((int)(m_passPoint.x+m_width/2),(int)(m_passPoint.y+m_height/2))
						,m_val==1?
								 new Point((int)(m_animPointX+m_width/2),(int)(m_animPointY+m_height/2))
								:new Point((int)(m_screenPos.x+m_width/2),(int)(m_screenPos.y+m_height/2))
						,m_lineColor
						,Constants.ADJACENT_LINE_SIZE);   
				break;
			case NONE: 
				for(int idx=0;idx<m_adjacentPoints.size();idx++)
				{
					g.drawLine(m_screenPosMidle, m_adjacentPoints.get(idx), m_lineColor,Constants.ADJACENT_LINE_SIZE);   
				}
				break;
			default:
				break;
			} 
		
	}
	
	public void paint(Graphics g)
	{ 
		
		if(m_active==false) return;
		switch(m_state)
		{  
		case MERGING:  
		case NONE:   
			drawTileImage(g, m_screenPos.x,   m_screenPos.y);

			if(m_val!=0)
			{ 
				PaintLabel(g);
			} 
			break;
		case ANIM_PASS:  
			if(m_val!=1)
			{
 				drawTileImage(g, m_screenPos.x,   m_screenPos.y); 
				PaintLabel(g);
			}
		break;  
			 
		case MERGED:     
			drawTileImage(g, m_screenPos.x, m_screenPos.y);   

			if(m_atrib==atrib.SHINING)
			{ 
				switch(m_tileSize) 
				{	
				case 3:g.drawImage(AssetManager.tile_shine, m_screenPos.x,   m_screenPos.y,   (int)(m_anim[2])*162,0,162,186);break;
				case 4:g.drawImage(AssetManager.tile_shine_4x4, m_screenPos.x,   m_screenPos.y,   (int)(m_anim[2])*129,0,129,152);break;
//				case 5:g.drawImage(AssetManager.tile_shine_5x5, m_screenPos.x,   m_screenPos.y,    (int)(m_anim[2])*109-22+(int)m_anim[2]/2,0,107,127);break;
				}
			}  
			PaintMergeImage(g);
			break;
		default:
			break;  
		}    
		
		switch(m_atrib) 
		{
		case GOT_COIN:
			if(m_anim[2]==0) 
			{

				genSet.setTextProperties( 25, Color.RED, Align.CENTER);
				g.drawString("+" + ((int)(m_anim[3])), m_screenPos.x,  (int)( m_screenPos.y+m_anim[4]-m_anim[0]), genSet.paint); 
			}
			else 
			{

				genSet.setTextProperties( 25, Color.RED, Align.CENTER);
				g.drawString("+" + ((int)(m_anim[3])), m_screenPos.x,  m_screenPos.y,   genSet.paint); 
			}
			break;
		}
		
		
//		g.drawRoundRect(m_screenPos.x,   m_screenPos.y, m_width, m_height,20,20,Color.argb(120, 0, 0,0));
//		genSet.setTextProperties( 25, Color.RED, Align.LEFT);
//		g.drawString(m_atrib + " ", m_screenPos.x,   m_screenPos.y, genSet.paint);   
//		g.drawString(m_last_atrib+"", m_screenPos.x,   m_screenPos.y+50, genSet.paint);  
		
	} 
	 
	
	private void PaintLabel(Graphics g, int p_val, int p_addX, int p_addY) 
	{  
		if(m_labelImg.length==2)
		{ 
			g.drawImage(AssetManager.nums, p_addX,p_addY, m_labelImg[1], CONFIG.X_SCALE,CONFIG.Y_SCALE);
		}
		else if(m_labelImg.length==3)
		{
			g.drawImage(AssetManager.nums, p_addX, p_addY, m_labelImg[1], CONFIG.X_SCALE,CONFIG.Y_SCALE);
			g.drawImage(AssetManager.nums, p_addX, p_addY, m_labelImg[2], CONFIG.X_SCALE,CONFIG.Y_SCALE);
		} 
	}
	
	public void SetValue(int p_val)
	{
		m_val = p_val;
		m_isPressed=false;
		updateTile();
	}
	public void addValue(int p_val)
	{
		m_val+=p_val;
		m_isPressed=false;
		updateTile();
	}
	
	private void updateTile() 
	{

		updateLabelImage() ;
		if(m_val==0)
		{ 
			goneOutOfExistence();
		}
		else 
		{
			m_tileType = m_origTileType;
			m_active = true;
		}
	}
	public boolean isOnAnim()
	{
		return m_state == state.ANIM_PASS || m_state == state.ANIM_ENTRY_1 || m_state == state.MOVING;
	}
	
	int MAGIC_VARIABLE = 10;
	public void animateEntry_1()
	{ 
		SetState(state.ANIM_ENTRY_1);

		float l_startPosX = CONFIG.SCREEN_MID-m_width/2;
		float l_startPosY =  CONFIG.SCREEN_MID_Y-m_height-MAGIC_VARIABLE;
		float l_destPosX = m_screenPos.x;
		float l_destPosY = m_screenPos.y;
		
		m_animPointX    = l_startPosX;
		m_animPointY    = l_startPosY;    
		m_passLimitX    = (float)(l_destPosX-l_startPosX);
		m_passLimitY    = (float)(l_destPosY-l_startPosY); 
		m_passBuffX     = 0; 
		m_passBuffY     = 0; 
		int p_speed     = 20; 
//		int p_speed     = 500; 
		m_passSpeedX    = (float)((float)m_passLimitX/(float)p_speed);
		m_passSpeedY    = (float)((float)m_passLimitY/(float)p_speed);
		m_passLimitX    =  Math.abs(m_passLimitX);
		m_passLimitY    =  Math.abs(m_passLimitY);  
		updateAnimDir();

	}
	
	public void AnimateQuickMerger(Point p_targetLoc, float p_speed)
	{ 
		animateMove(p_targetLoc, p_speed);
		SetAtrib(atrib.QUICK_MERGER);
	}
	public void animateMove(Point p_targetLoc, float p_speed)
	{ 
		SetState(state.MOVING);
		float l_startPosX = m_screenPos.x;
		float l_startPosY = m_screenPos.y;
		float l_destPosX  = p_targetLoc.x;
		float l_destPosY = p_targetLoc.y;	
		
		m_animPointX    =  l_startPosX;
		m_animPointY    =  l_startPosY; 
		m_passLimitX    = (float)(l_destPosX-l_startPosX);
		m_passLimitY    = (float)(l_destPosY-l_startPosY); 
		m_passBuffX     = 0; 
		m_passBuffY     = 0;  
		m_passSpeedX    = (float)((float)m_passLimitX/ p_speed);
		m_passSpeedY    = (float)((float)m_passLimitY/p_speed);
		m_passLimitX    =  Math.abs(m_passLimitX);
		m_passLimitY    =  Math.abs(m_passLimitY);  
		updateAnimDir();
		
	}
	
	 private void updateAnimDir()
	 {		   
		if(m_passSpeedX<0&&m_passSpeedY<0) 
		{
			m_animDir=anim_dir.TOP_LEFT;
		}
		else if(m_passSpeedX==0&&m_passSpeedY<0) 
		{
			m_animDir=anim_dir.TOP_MID;
		}
		else if(m_passSpeedX>0&&m_passSpeedY<0) 
		{
			m_animDir=anim_dir.TOP_RIGHT;
		}
		else if(m_passSpeedX<0&&m_passSpeedY==0) 
		{
			m_animDir=anim_dir.LEFT_DIR;
		}
		else if(m_passSpeedX>0&&m_passSpeedY==0) 
		{
			m_animDir=anim_dir.RIGHT_DIR;
		}
		else if(m_passSpeedX<0&&m_passSpeedY>0) 
		{
			m_animDir=anim_dir.BOTTOM_LEFT;
		}
		else if(m_passSpeedX==0&&m_passSpeedY>0) 
		{
			m_animDir=anim_dir.BOTTOM_MID;
		}
		else if(m_passSpeedX>0&&m_passSpeedY>0) 
		{
			m_animDir=anim_dir.BOTTOM_RIGHT;
		}
 
	 }
	
	 public void Updates(float p_deltaTime)
	{
		switch(m_state)
		{
			case NONE: break;
			case SHOW_ADJACENT_IN:
				/*
				 * 3- buffer
				 * 4- LINE size
				 */
				if(m_anim[3]<1.f)
				{
					m_anim[3] += p_deltaTime * 0.000003f; 
					m_anim[4] = m_anim[3] * Constants.ADJACENT_LINE_SIZE; 
				}
				else 
				{
					SetState(state.NONE);
				}
				
				break;
			case ANIM_ENTRY_1: 
				m_animPointX += p_deltaTime * m_passSpeedX; 
				m_animPointY += p_deltaTime * m_passSpeedY;  
				
				if(m_passBuffX<m_passLimitX)
					m_passBuffX += Math.abs(p_deltaTime * m_passSpeedX); 

				if(m_passBuffY<m_passLimitY)
					m_passBuffY += Math.abs(p_deltaTime * m_passSpeedY);
				
				if((m_passBuffX>=m_passLimitX)&&(m_passBuffY>=m_passLimitY))	 
				{
					SetState(state.NONE);
					m_passBuffX=0;
					m_passBuffY=0;
					expandOut(0.05f,m_anim[2]); // START Expand MARK 2
//					 animateEntry_1();
				}
				break;
			case MOVING: 
				m_animPointX += p_deltaTime * m_passSpeedX; 
				m_animPointY += p_deltaTime * m_passSpeedY;  
				
				if(m_passBuffX<m_passLimitX)
					m_passBuffX += Math.abs(p_deltaTime * m_passSpeedX); 

				if(m_passBuffY<m_passLimitY)
					m_passBuffY += Math.abs(p_deltaTime * m_passSpeedY);
				
				if((m_passBuffX>=m_passLimitX)&&(m_passBuffY>=m_passLimitY))	 
				{
					SetState(state.NONE);
					m_passBuffX=0;
					m_passBuffY=0;  
					switch(m_atrib)
					{
						case QUICK_MERGER: 	goneOutOfExistence();  break;
					}
					SetAtrib(atrib.NONE);
				}
				break;
			case ANIM_PASS: 
				m_animPointX += p_deltaTime * m_passSpeedX; 
				m_animPointY += p_deltaTime * m_passSpeedY;  
				m_passBuffX  += Math.abs(p_deltaTime * m_passSpeedX); 
				m_passBuffY  += Math.abs(p_deltaTime * m_passSpeedY);
				
				if((m_passBuffX>=m_passLimitX) &&(m_passBuffY>=m_passLimitY))
				{
					SetState(state.NONE); 
					addValue(-1);
					updateLabelImage() ;
					
				}
				break;
		default:
			break;
		}
		
		switch(m_atrib)
		{	
			case GOT_COIN: GotCoinUpdates(p_deltaTime); break;
			case SHINING: ShineUpdate(p_deltaTime); break;
			case HIGHLIGHT:
				/*
				 * 0- buffer
				 * 1- is blinking
				 * 
				 * */
				
				if(m_anim[0]<2.5f)
				{
					m_anim[0] += p_deltaTime * 0.1f; 
				}
				else 
				{
					m_anim[0] = 0;
					m_anim[1] = m_anim[1]==1?0:1;
				}
				break;
			case EXPAND_IN_HOLD:
			case EXPAND_IN: 
				/*
				 * 0- expand scale buffer
				 * 1- expand speed
				*/
				if(m_anim[0]<1)
				{
					m_anim[0] += p_deltaTime * m_anim[1]; 
				}
				else 
				{  
					if(m_atrib == atrib.EXPAND_IN)
					{
						m_atrib = atrib.NONE; 
					}
					if(m_last_state == state.ANIM_ENTRY_1)
					{
						expandOut(0.05f,m_anim[2]);// END Expand MARK 2
					}
					if(m_state==state.QUICK_MERGE_MAIN) 
					{ 
						expandOut(0.12f, m_anim[2]);  //EXPAND MK 001 END
					}
				}
				
				break;
			case EXPAND_OUT: 
				/*
				 * 0- expand scale buffer
				 * 1- expand speed
				*/
				if(m_anim[0]>0)
				{
					m_anim[0] -= p_deltaTime * m_anim[1]; 
				}
				else 
				{   
					switch(m_state) 
					{
						case QUICK_MERGE_MAIN:break;
						case MERGED:	Shine();  break;
						case ANIM_ENTRY_1:	SetState(state.NONE);  break;
					}

					SetAtrib(atrib.NONE); 
					
					 
				}
				
				break;
		default:
			break;
		}
	} 
	private void goneOutOfExistence() 
	{ 
		m_tileType = ColorManager.GONE_OUT_OF_EXISTENCE;
		m_active = false;
	}
	public boolean isOnPrepareQuickMerge() 
	{
		// if nabwelo para mag quick merge, (Expand in)
		return m_state == state.QUICK_MERGE_MAIN && m_atrib == atrib.EXPAND_IN;
	}
	
	public boolean isOnQuickMergeMagnet() 
	{
		// Nahigop na ng ibang tiles, (expand out)
		return m_state == state.QUICK_MERGE_MAIN && m_atrib == atrib.EXPAND_OUT;
	}
	
	public boolean isPressed(TouchEvent g)
	{ 
		if(g.type == TouchEvent.TOUCH_DOWN && m_active)
		{
//			genSet.ShowToast(Color.red(m_color) + "," + Color.green(m_color) + "," + Color.blue(m_color));
		  return drawRoundRect(
				   g
				   , m_screenPos.x 
				   , m_screenPos.y 
				   , m_width
				   , m_height 
		        );
		}
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			m_isPressed = false;
		}
		return false;
	} 
	 
	public void PassValue(Point p_passPoint,int p_speed)
	{  
		m_state         = state.ANIM_PASS;  
		m_passPoint     = new Point(p_passPoint);
		m_animPointX    = m_screenPos.x;
		m_animPointY    = m_screenPos.y;
		m_passLimitX    = m_passPoint.x - m_screenPos.x;
		m_passLimitY    = m_passPoint.y - m_screenPos.y;
		m_passSpeedX    = (m_passLimitX/p_speed);
		m_passSpeedY    = (m_passLimitY/p_speed);
		m_passLimitX    = Math.abs(m_passLimitX);
		m_passLimitY    = Math.abs(m_passLimitY); 
		m_passBuffX     = 0; 
		m_passBuffY     = 0; 
		
		updateAnimDir();
 	}
	
	public void drawTileImage(Graphics g, float p_x, float p_y)
	{   
		drawTileImage(g,(int)p_x, (int) p_y);
 	 }  
	
	public void drawTileImage(Graphics g, int p_x, int p_y)
	{     
		g.drawImage(AssetManager.tiles, p_x ,p_y, m_tileImage,CONFIG.X_SCALE,CONFIG.Y_SCALE);
    }  
	public void drawTileImage(Graphics g, int p_x, int p_y, float p_scaleX, float p_scaleY)
	{     
		g.drawImage(AssetManager.tiles, p_x ,p_y, m_tileImage,CONFIG.X_SCALE+p_scaleX,CONFIG.Y_SCALE+p_scaleY);
    }  
	
	public boolean drawRoundRect(TouchEvent g, int p_x, int p_y, int p_len, int p_wid)
	{    
		if(inBounds(g, p_x ,p_y, p_len, p_wid))
			m_isPressed=true;
		else
			m_isPressed=false;
		
		return m_isPressed;
	} 
	
	public boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
		 
		if (event.x > x && event.x < x + width - 1 && event.y > y && event.y < y + height - 1)
	 
			return true; 
		else
			return false;
	} 
	
	public int getTileType()
	{
		return m_tileType;
	}
	
	public int getTileSize()
	{
		return m_tileSize;
	}
	
	
	public Point getScreenPos()
	{
		return m_screenPos;
	} 
	
	public Point getScreenPosMid()
	{
		
		return m_screenPosMidle;
	} 
	
	public int getValue()
	{
		return m_val;
	}
	
	public int getWidth()
	{
		return m_width;
	}
	
	public int getHeight()
	{
		return m_height;
	}
	
	public boolean isActive()
	{
		return m_active;
	}
	
	public void SetAdjacentPoints(Vector<Point> p_adjacentPoints) { 
		m_adjacentPoints = p_adjacentPoints;
	}

	public boolean isMerged() 
	{   
		return m_state == state.MERGED;
	} 
	
	public void highlight()
	{ 
		SetAtrib(atrib.HIGHLIGHT);  
		m_anim = new float[5];
	}
	 
	public void expandInHold(float p_speed, float p_expandSize)
	{
		expandIn(p_speed,p_expandSize); 
		SetAtrib(atrib.EXPAND_IN_HOLD);  
	}
	
	public void ShowAdjacent()
	{
		SetState(state.SHOW_ADJACENT_IN);
		m_anim = new float[5]; 
		m_anim[0] = 0f;   
		m_anim[1] = 0f;  
	}
	public void QuickMerge() 
	{
		expandIn(0.03f, 0.2f);  //EXPAND MK 001 START 
		SetState(state.QUICK_MERGE_MAIN);   
	}
	public void expandIn(float p_speed, float p_expandSize)
	{ 
		SetAtrib(atrib.EXPAND_IN);  
		m_anim = new float[5]; 
		m_anim[0] = 0f; 
		m_anim[1] = p_speed;
		m_anim[2] = p_expandSize;   
	}

	public void MarkAsMerged()
	{
		SetState(state.MERGED);  
	}

	public void DemarkAsMerged()
	{
		SetState(state.NONE);  
	}
	
	 
	public void expandOut(float p_speed, float p_expandSize)
	{  
		SetAtrib(atrib.EXPAND_OUT);   
		m_anim = new float[5]; 
		m_anim[0] = 1f; 
		m_anim[1] = p_speed; 
		m_anim[2] = p_expandSize; 
	}

	public Rect getBounds() { 
		return new Rect(m_screenPos.x 
				   , m_screenPos.y 
				   , m_screenPos.x +m_width
				   , m_screenPos.y +m_height );
		
	}

	public boolean isAtribNone() {
		return m_atrib == atrib.NONE;
	}

	

}
