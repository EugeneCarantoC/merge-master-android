package com.eugexstudios.mergemaster;
 
import android.graphics.Color;
import android.graphics.Rect; 

import com.eugexstudios.adprovider.AdProvider;
import com.eugexstudios.adprovider.StartApp.VideoAdType; 
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.mergemaster.AskUserWindow.QUESTION;
import com.eugexstudios.utils.AssetManager;
import com.eugexstudios.utils.CONFIG;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Screen;
import com.eugexstudios.utils.genSet;

public class GameOverWindow extends Screen{
	private enum state {
		IDLE
		,ASK_TO_EXIT
		,NO_CONNECT
		,PLACEHOLDER_3
		,PLACEHOLDER_4
		,PLACEHOLDER_5
		,PLACEHOLDER_6
		}; 
	private static final byte RESTART_BTN = 1; 
	private static final int HOME_BTN = 2; 
	private static final int SHARE_BTN = 3; 
	private static final int WATCH_BTN = 4; 

	final int BTN_POS_Y      = Constants.ScaleY(665);
	final int HOME_BTN_X     = Constants.ScaleX(0.35f);
	final int SHARE_BTN_X    = Constants.ScaleX(0.507f);
	final int DIM_COLOR      = Color.argb(180, 0, 0, 0);
	final int WINDOW_COLOR   = Color.rgb(255, 245, 245);
	final int SHADOW_COLOR   = Color.argb(30, 0, 0, 0);

	final Rect WINDOW_RECT = new Rect(
			  Constants.ScaleX(0.14f)
			, Constants.ScaleY(0.25f)
			, Constants.ScaleX(0.14f)+ Constants.ScaleX(0.72f)
			, Constants.ScaleY(0.25f)+ Constants.ScaleY(0.52f)
			);

	private state m_state; 
	private AskUserWindow m_askWindow;
	private static AdProvider 		   m_adProvider; 
 
	public GameOverWindow()
	{ 
		m_state = state.IDLE;
	    m_adProvider = new AdProvider(); 
		m_adProvider.RequestVideoAd(Constants.REWARD_VIDEO_AD_SKIP_LVL,VideoAdType.RewardToSkipLevel);
 
		
	}
	@Override
	protected void Paint(Graphics g) {
		switch(m_state)
		{
			case IDLE: PaintIdle(g); break;
			case ASK_TO_EXIT:PaintAskExit(g); break;
			case NO_CONNECT: PaintNoConnect(g);break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		}
	 
	}

	private void PaintAskExit(Graphics g)
	{
		PaintIdle(g); 
		m_askWindow.Paint(g);
	}
	private void PaintNoConnect(Graphics g)
	{
		PaintIdle(g); 
		m_askWindow.Paint(g);
	}
	private final static int WINDOW_POS_X = Constants.ScaleX(30);
	private final static int WINDOW_POS_Y = Constants.ScaleY(300); 
	private final static int WATCH_BTN_X = WINDOW_POS_X+Constants.ScaleX(50);
	private final static int WATCH_BTN_Y = Constants.ScaleX(822);
	private void PaintIdle(Graphics g)
	{
		g.drawRect(Constants.SCREEN_RECT, DIM_COLOR); 

		g.drawImage(AssetManager.sprites, WINDOW_POS_X, WINDOW_POS_Y, Constants.WINDOW_1,CONFIG.X_SCALE,CONFIG.Y_SCALE);
		g.drawImage(AssetManager.sprites, WINDOW_POS_X, WINDOW_POS_Y, Constants.GAME_OVER_HEADER, CONFIG.X_SCALE, CONFIG.Y_SCALE);
		
//	    drawRoundRect(g, Constants.RESTART_BTN_3, Color.BLACK, Color.BLACK,RESTART_BTN, CONFIG.X_SCALE, CONFIG.Y_SCALE);
	    g.drawImage(AssetManager.sprites, (buttonPressed==RESTART_BTN?-6:0),(buttonPressed==RESTART_BTN?8:0), Constants.RESTART_BTN_3, CONFIG.X_SCALE, CONFIG.Y_SCALE);

//	    drawRoundRect(g, HOME_BTN_X, BTN_POS_Y, Constants.HOME_BTN_IMG, Color.BLACK, Color.BLACK,HOME_BTN, CONFIG.X_SCALE, CONFIG.Y_SCALE);
	    g.drawImage(AssetManager.sprites, HOME_BTN_X+(buttonPressed==HOME_BTN?-4:0),BTN_POS_Y+(buttonPressed==HOME_BTN?6:0),Constants.HOME_BTN_IMG, CONFIG.X_SCALE, CONFIG.Y_SCALE);

//	    drawRoundRect(g,SHARE_BTN_X, BTN_POS_Y, Constants.SHARE_BTN_IMG, Color.BLACK, Color.BLACK,SHARE_BTN, CONFIG.X_SCALE, CONFIG.Y_SCALE);
	    g.drawImage(AssetManager.sprites, SHARE_BTN_X+(buttonPressed==SHARE_BTN?-4:0), BTN_POS_Y+(buttonPressed==SHARE_BTN?6:0),Constants.SHARE_BTN_IMG, CONFIG.X_SCALE, CONFIG.Y_SCALE);
 
	    
//	    drawRoundRect(g,WATCH_BTN_X,WATCH_BTN_Y, Constants.WATCH_VID_BTN, Color.BLACK, Color.BLACK,WATCH_BTN, CONFIG.X_SCALE, CONFIG.Y_SCALE);
	    g.drawImage(AssetManager.sprites,WATCH_BTN_X+(buttonPressed==WATCH_BTN?-3:0),WATCH_BTN_Y+(buttonPressed==WATCH_BTN?4:0), Constants.WATCH_VID_BTN);
	} 
	 
	@Override
	protected void Updates(float p_deltaTime) { 
		switch(m_state)
		{
			case IDLE: break;
			case ASK_TO_EXIT: 
			case NO_CONNECT: 
				if(m_askWindow.isClosed())
				{
					m_state = state.IDLE;
				}
				break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		}
		
	}

	@Override
	protected void TouchUpdates(TouchEvent g) {
		
		switch(m_state)
		{
			case NO_CONNECT: 
			case ASK_TO_EXIT:  m_askWindow.TouchUpdates(g);break;
			case IDLE: GameOverTouchUpdates(g); break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		}    	 
				
	}
	private void AskToExit()
	{
		m_state = state.ASK_TO_EXIT; 
		m_askWindow = new AskUserWindow(QUESTION.GO_TO_MENU);
	} 

	private void GoToNoConnectNotif()
	{
		m_state = state.NO_CONNECT; 
		m_askWindow = new AskUserWindow(QUESTION.NO_CONNECT);
	}
	
	private void GameOverTouchUpdates(TouchEvent g)
	{
		if(g.type == TouchEvent.TOUCH_DOWN)
		{     
		    drawRoundRect(g, Constants.RESTART_BTN_3, Color.BLACK, Color.BLACK,RESTART_BTN, CONFIG.X_SCALE, CONFIG.Y_SCALE);
		    drawRoundRect(g, HOME_BTN_X, BTN_POS_Y, Constants.HOME_BTN_IMG, Color.BLACK, Color.BLACK,HOME_BTN, CONFIG.X_SCALE, CONFIG.Y_SCALE);
		    drawRoundRect(g, SHARE_BTN_X, BTN_POS_Y, Constants.SHARE_BTN_IMG, Color.BLACK, Color.BLACK, SHARE_BTN, CONFIG.X_SCALE, CONFIG.Y_SCALE);
		    drawRoundRect(g,WATCH_BTN_X,WATCH_BTN_Y, Constants.WATCH_VID_BTN, Color.BLACK, Color.BLACK, WATCH_BTN, CONFIG.X_SCALE, CONFIG.Y_SCALE);
 
		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
			case RESTART_BTN: GameScreen.Restart(); break;
			case HOME_BTN:  AskToExit(); break;
			case SHARE_BTN: genSet.game.ShareApp(); break; 
			case WATCH_BTN:  WatchVideo(); break;
			}

			if(buttonPressed>0) 
			{
				AssetManager.btn_1.play();
			}
			buttonPressed = 0;
		} 
	} 
	
	private void WatchVideo()
	{ 
		if(m_adProvider.isVideoAdAvailable())
		{ 
			m_adProvider.ShowVideoAds(); 
		} 
		else
		{
			GoToNoConnectNotif();
		}
		
	}
	@Override
	protected void backButton() {
		switch(m_state)
		{
			case IDLE:AskToExit(); break;
			case ASK_TO_EXIT: m_state = state.IDLE; break;
			case NO_CONNECT: m_state = state.IDLE;break;
			case PLACEHOLDER_3: break;
			case PLACEHOLDER_4: break;
			case PLACEHOLDER_5: break;
			case PLACEHOLDER_6: break;
		}
		
	}

}
