# Merge Master

Merge Master is a game created using JAVA programming language and is targeted for Android. 

## About the game

Merge Master is an exciting and joyful brain training puzzle, where you have to fuse all same colored tile blocks to one.

A fun, simple to learn, colorful block puzzle and a great IQ exercise suitable for all ages!

Develop a strategy, anticipate separation of a tile from its group, enjoy the tile merges and become the Merge Master!
 
## Author

* **Eugene C. Caranto** 

## Acknowledgments

*  [Kilobolt](http://www.kilobolt.com/game-development-tutorial.html) - Base framework used
